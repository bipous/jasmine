/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class ReadFastaFile {
    Logger logger = LogManager.getLogger();
    private ArrayList<String> seqTextArrayList;
    private ArrayList<String> seqStrArrayList ;
    private HashMap<String, String> seqStrNameHash ;
    public ReadFastaFile(){
    
    }
    
    public ReadFastaFile(String fastaFilePath) throws IOException{
        readFasta(fastaFilePath);
    }
    
    public void readFasta(String fastaFilePath) {
        seqStrArrayList = new ArrayList<>();
        seqStrNameHash = new HashMap<>();
        seqTextArrayList = new ArrayList<>();
        try {
            BufferedReader fastaBR = new BufferedReader(new FileReader(new File(fastaFilePath)));
            String fastaLine = null;
            StringBuilder seqStr = new StringBuilder();
            String seqID = "";
            while ((fastaLine = fastaBR.readLine()) != null) {
    //            String seqID = fastaLine.trim();
    //            seqID = seqID.split(" ")[0];
    //            seqID = seqID.substring(1,(seqID.length()));
    //            String seqStr = fastaBR.readLine();
    ////            logger.info(seqID +"\t"+ seqStr);
    //            
    //            seqStrArrayList.add(seqStr);
    //            seqStrNameHash.put(seqID, seqStr);

                if (fastaLine.startsWith(">") == true) {
                    if(!seqID.equals("")){
                        seqStrArrayList.add(seqStr.toString());
                        seqStrNameHash.put(seqID, seqStr.toString());
                        seqTextArrayList.add(seqID+"\t"+ seqStr.toString());
                    }
                    seqStr = new StringBuilder();
                    seqID = fastaLine.trim();
                    seqID = seqID.split(" ")[0];
                    seqID = seqID.substring(1,(seqID.length()));                            
                } else {
                    fastaLine = fastaLine.trim().replaceAll("\\s+", "");
                    seqStr.append(fastaLine);
                }

            }
            fastaBR.close();
        } catch (Exception e) {
            logger.error("Error in reading fasta file: "+fastaFilePath);
            logger.error("error meassage: " + e.getMessage());
        }
        
    }
    
    /*
    readin fasta file
    and selecting sequence according defined host
    */
    public void readFasta(String fastaFilePath, String selectHost) throws FileNotFoundException, IOException{
        seqStrArrayList = new ArrayList<>();
        seqStrNameHash = new HashMap<>();
        seqTextArrayList = new ArrayList<>();
        BufferedReader fastaBR = new BufferedReader(new FileReader(new File(fastaFilePath)));
        String fastaLine = null;
        
        StringBuilder seqStr = new StringBuilder();
        String seqID = "";
        while ((fastaLine = fastaBR.readLine()) != null) {
            if (fastaLine.startsWith(">") == true) {
                if(seqID.toLowerCase().contains(selectHost)){
                    seqStrArrayList.add(seqStr.toString());
                    seqStrNameHash.put(seqID, seqStr.toString());
                    seqTextArrayList.add(seqID+"\t"+ seqStr.toString());
                }
                seqStr = new StringBuilder();
                seqID = fastaLine.trim();
                seqID = seqID.split(" ")[0];
                seqID = seqID.substring(1,(seqID.length()));                            
            } else {
                fastaLine = fastaLine.trim().replaceAll("\\s+", "");
                seqStr.append(fastaLine);
            }
        }
        fastaBR.close();
    }
    
    /*
    readin fasta file
    and selecting sequence according defined host
    addition filtering, by accession number from MirGeneDB
    */
    public void readFasta(String fastaFilePath, String selectHost, boolean useMirGeneDB, ArrayList accInMiRGDB ) throws FileNotFoundException, IOException{
        logger.info("Query fasta file: "+fastaFilePath);
        seqStrArrayList = new ArrayList<>();
        seqStrNameHash = new HashMap<>();
        seqTextArrayList = new ArrayList<>();
        try {
            BufferedReader fastaBR = new BufferedReader(new FileReader(new File(fastaFilePath)));
            String fastaLine = null;

            StringBuilder seqStr = new StringBuilder();
            String seqID = "";
            String seqAcc = "";
            while ((fastaLine = fastaBR.readLine()) != null) {
                if (fastaLine.startsWith(">") == true) {
                    if(seqID.toLowerCase().contains(selectHost)){
                        if (useMirGeneDB) {
                            // use MirGeneDB
                            if(accInMiRGDB.contains(seqAcc)){
                                seqStrArrayList.add(seqStr.toString());
                                seqStrNameHash.put(seqID, seqStr.toString());
                                seqTextArrayList.add(seqID+"\t"+ seqStr.toString());
                            }
                        }
                        else{
                            // not use MirGeneDB
                            seqStrArrayList.add(seqStr.toString());
                            seqStrNameHash.put(seqID, seqStr.toString());
                            seqTextArrayList.add(seqID+"\t"+ seqStr.toString());
                        }
                        
                    }
                    seqStr = new StringBuilder();
                    seqID = fastaLine.trim();
                    seqAcc = seqID.split(" ")[1];
                    seqID = seqID.split(" ")[0];
                    seqID = seqID.substring(1,(seqID.length()));                            
                } else {
                    fastaLine = fastaLine.trim().replaceAll("\\s+", "");
                    seqStr.append(fastaLine);
                }
            }
            fastaBR.close();
        } catch (Exception e) {
            logger.error("Error in reading fasta file: "+fastaFilePath);
            logger.error("error meassage: " + e.getMessage());
        }
        
    }

    /*
    specific for reading in chromosome fasta sequence  
    single chromosome sequence in each fasta file
    */
    public void readChromoFa(String fastaFilePath) throws FileNotFoundException, IOException{
        String[] chromoAray = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","X","Y"};
        seqStrArrayList = new ArrayList<>();
        seqStrNameHash = new HashMap<>();
        seqTextArrayList = new ArrayList<>();
        BufferedReader fastaBR = new BufferedReader(new FileReader(new File(fastaFilePath)));
        String fastaLine = null;
        
        StringBuilder seqStr = new StringBuilder();
        String seqID = "none";
        while ((fastaLine = fastaBR.readLine()) != null) {
            if (fastaLine.startsWith(">") == true) {
                seqStr = new StringBuilder();
                seqID = fastaLine.trim();
                seqID = seqID.split(" ")[0];
                seqID = seqID.substring(1, (seqID.length()));
            } else {
                fastaLine = fastaLine.trim().replaceAll("\\s+", "");
                seqStr.append(fastaLine);
            }
        }
        fastaBR.close();
        seqStrNameHash.put(seqID, seqStr.toString());
    }

    
    public ArrayList<String> getSeqArrayList() {
        return seqStrArrayList;
    }
    
    public ArrayList<String> getSequenceText(){
        return seqTextArrayList;
    }
    
    public HashMap<String, String> getSeqStrNameHash() {
        return seqStrNameHash;
    }
    
    
}
