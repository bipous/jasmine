/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
//// */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class Plotting extends JASMINEstep implements JASMINEbase{
    Logger logger = LogManager.getRootLogger();
    private String TASK_FOLDER = "PlottedFigures";
    private String R_PATH;
    private String OUTPUTFOLDER;
    private String projectID;
    private String inputFolder ;
    private String datasetlstFile;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder ;
    private HashMap<String, String> CONFIG_HASH;

    
    Plotting(HashMap<String, String> CONFIGHASH) throws IOException {
        CONFIG_HASH = CONFIGHASH;
        setConfigurations(CONFIG_HASH);
        executeThisStep();
    }
    
    public void setConfigurations(HashMap<String, String> CONFIG_HASH)  throws IOException{
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.R_PATH = CONFIG_HASH.get("RSCRIPT");
        this.OUTPUTFOLDER = CONFIG_HASH.get("PROJECT_FOLDER")+TASK_FOLDER+"/";
        
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
            this.inputFolder = CONFIG_HASH.get("PROJECT_FOLDER")+"PlottingData"+FILESEPARATOR;
            this.OUTPUTFOLDER = CONFIG_HASH.get("PROJECT_FOLDER")+TASK_FOLDER+FILESEPARATOR;
        }
        else{
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER")+FILESEPARATOR;
            this.inputFolder = CONFIG_HASH.get("PROJECT_FOLDER")+FILESEPARATOR+"PlottingData"+FILESEPARATOR;
            this.OUTPUTFOLDER = CONFIG_HASH.get("PROJECT_FOLDER")+FILESEPARATOR+TASK_FOLDER+FILESEPARATOR;
        } 
        
        if(CONFIG_HASH.containsKey("DATASETLIST")){
            this.datasetlstFile = CONFIG_HASH.get("DATASETLIST");
        }
//        else{
//            this.datasetlstFile = projectFolder +"configs"+FILESEPARATOR+"miRNASeqData.tsv";
//        }
        JASMINEutility misoUtility = new JASMINEutility();
        this.datasetArray = misoUtility.getDatasetList(CONFIG_HASH.get("DATASETLIST"));



    }


    @Override
    public void verifyInputData() throws IOException {
        logger.info("Not supported yet.");
    }

    @Override
    public void verifyOutputData() throws IOException {
        logger.info("Not supported yet.");
    } 

    @Override
    public void executeThisStep() throws IOException {
        Boolean fA = new File(OUTPUTFOLDER).mkdir();       
        if (fA) logger.info("created output folder <" + OUTPUTFOLDER+ "> for results" );
//        plotMappingSummary();
//        plotHierarchy();
//        plotDistribution();
//        plotSampleIsoDistance();
//        plotTemplatedProfile();
//        plotCorrSingleDouble();
        logger.info(datasetlstFile);
        plotingALLlevels();

    }

    private void plotMappingSummary() throws IOException {
        String plotDataFilePath = inputFolder+projectID +".mappingSummary.tsv";
        String mappingSummaryRaw = OUTPUTFOLDER+projectID+".mappingsummary.raw.png";
        String mappingSummaryRatio = OUTPUTFOLDER+projectID+".mappingsummary.ratio.png";
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("#   Rscript generated for project " + projectID);
        scriptArray.add("#   created: " + new Date());
        scriptArray.add("library(plyr)" );
        scriptArray.add("library(reshape2)");
        scriptArray.add("library(ggplot2)");
        scriptArray.add("library(grid)");
        scriptArray.add("library(gridExtra)");
        scriptArray.add("#");
        scriptArray.add("x <- read.delim(\""+plotDataFilePath+"\", sep='\\t', header = TRUE )");
        scriptArray.add("x.melt <- melt(x[,c(\"dataset\",\"unmap\",\"reliable\",\"ambiguous\",\"others\") ], id.vars = c('dataset'))");
        scriptArray.add("colnames(x.melt)<- c(\"dataset\",\"type\",\"readCount\")");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("png(file=\""+mappingSummaryRaw+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("ggplot(x.melt, aes(x = dataset, y = readCount, fill = type)) + geom_bar(stat = 'identity')+ theme(axis.text.x = element_text(angle=270, hjust=1))+ theme(legend.key.size = unit(0.5, \"in\"),legend.text = element_text(size = 15),legend.position=\"bottom\")+ylab(\"Raw Read Count\")+xlab(\" \")");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("x.percentage <-data.frame(x$dataset, x$unmap/x$total, x$reliable/x$total, x$ambiguous/x$total, x$others/x$total)");
        scriptArray.add("colnames(x.percentage) <- c(\"dataset\",\"unmap\",\"reliable\",\"ambiguous\",\"others\")");
        scriptArray.add("x.percentage.melt <- melt(x.percentage, id.vars = c('dataset'))");
        scriptArray.add(" colnames(x.percentage.melt)<- c(\"dataset\",\"type\",\"readCount\")");
        scriptArray.add("png(file=\""+mappingSummaryRatio+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("ggplot(x.percentage.melt, aes(x = dataset, y = readCount, fill = type)) + geom_bar(stat = 'identity')+ theme(axis.text.x = element_text(angle=270, hjust=1))+ theme(legend.key.size = unit(0.5, \"in\"),legend.text = element_text(size = 15),legend.position=\"bottom\")+ylab(\"Proportion\")+xlab(\" \")");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("#");
        
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "plotMappingSummary-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

                String line = null;
                logger.info("<OUTPUT>");
                while ((line = brAStdin.readLine()) != null)
                    logger.info(line);
                logger.info("</OUTPUT>");

                logger.info("<ERROR>");
                while ( (line = brAStdErr.readLine()) != null){
                    logger.info(line);
                }
                // need to parse the output from Bowtie to get the mapping summary
                logger.info("</ERROR>");

                int exitVal = proc.waitFor();            
                logger.info("Process exitValue: " + exitVal);   

            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
            
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        
        

    }
    
    private void plotHierarchy() throws IOException {
        String plotDataFilePath = inputFolder+projectID +".isomiR.hierarchy.tsv";
        String plotHierarchyPNG = OUTPUTFOLDER+projectID+".isomiR.hierarchy.png";
        
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("#   Rscript generated for project " + projectID);
        scriptArray.add("#   created: " + new Date());
        scriptArray.add("library(plyr)" );
        scriptArray.add("library(reshape2)");
        scriptArray.add("library(ggplot2)");
        scriptArray.add("library(grid)");
        scriptArray.add("library(gridExtra)");
        scriptArray.add("#");
        scriptArray.add("x <- read.delim(\""+plotDataFilePath+"\", sep='\\t', header = FALSE )");
        scriptArray.add("colnames(x) <- c(\"dataset\",\"study\",\"type\",\"isoType\",\"readPercentage\")");
        scriptArray.add("#");
        scriptArray.add("x.plot.A <- ggplot(x[which(x$type == \"A\"),], aes(x=dataset, y=readPercentage,fill=isoType)) + geom_bar( stat = 'identity') +theme(axis.text.x = element_blank(), axis.text.y = element_text(size=25),legend.position=\"none\") +scale_fill_manual(values=c(\"red\",\"blue\"))");
        scriptArray.add("x.plot.B <- ggplot(x[which(x$type == \"B\"),], aes(x=dataset, y=readPercentage,fill=isoType)) + geom_bar( stat = 'identity')+theme(axis.text.x = element_blank(), axis.text.y = element_text(size=25),legend.position=\"none\") +scale_fill_manual(values=c(\"hotpink\",\"lightskyblue\",\"palegreen\",\"springgreen4\",\"lightseagreen\"))");
        scriptArray.add("x.plot.C <- ggplot(x[which(x$type == \"C\"),], aes(x=dataset, y=readPercentage,fill=isoType)) + geom_bar( stat = 'identity')+theme(axis.text.x = element_blank(), axis.text.y = element_text(size=25),legend.position=\"none\")");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("png(file=\""+plotHierarchyPNG+"\",res = 300,units = 'in',width = 30, height = 10)");
        scriptArray.add("grid.arrange( arrangeGrob(x.plot.A, top=textGrob(\"Hierarchy 1\", gp=gpar(fontsize=30)) ), arrangeGrob(x.plot.B, top=textGrob(\"Hierarchy 2\", gp=gpar(fontsize=30)) ),  arrangeGrob(x.plot.C,top=textGrob(\"Hierarchy 3\", gp=gpar(fontsize=30)) ), ncol=3)  ");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("#");
      
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "plotIsomiRHierarchy-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brAStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brAStdErr.readLine()) != null){
                logger.info(line);
            }
            logger.info("</ERROR>");
            int exitVal = proc.waitFor();            
            logger.info("Process exitValue: " + exitVal);   
            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
    }
    
    private void plotDistribution() throws IOException {
        String plotDataFilePath1 = inputFolder+projectID +".isomiR.distribution.tsv";
        String plotDataFilePath2 = inputFolder+projectID +".isomiR.composition.tsv";
        String distributionPNGradar = OUTPUTFOLDER+projectID+".isomiR.distribution.radar.png";
        String distributionPNGheatmap = OUTPUTFOLDER+projectID+".isomiR.distribution.heatmap.png";
        
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("#   Rscript generated for project " + projectID);
        scriptArray.add("#   created: " + new Date());
        scriptArray.add("library(plyr)" );
        scriptArray.add("library(reshape2)");
        scriptArray.add("library(ggplot2)");
        scriptArray.add("library(grid)");
        scriptArray.add("library(gridExtra)");
        scriptArray.add("#");
        scriptArray.add("x <- read.delim(\""+plotDataFilePath1+"\", sep='\\t', header = FALSE )");
        scriptArray.add("colnames(x) <- c(\"dataset\", \"group\",\"isoform\",\"readCount\")");
        scriptArray.add("#");
        scriptArray.add("x.sum <- aggregate(x$readCount, by=list(x$dataset, x$isoform), FUN=sum)");
        scriptArray.add("#");
        scriptArray.add("colnames(x.sum) <- c(\"dataset\",\"isoform\",\"ReadCount\")");
        scriptArray.add("#");
        scriptArray.add("png(file=\""+distributionPNGradar+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("ggplot(x.sum,aes(x = isoform , y= log(cpm(ReadCount)) ,color=dataset, group=dataset)) + "
                + "geom_line( size=1.2,stat = 'identity')+"
                + "theme(panel.grid.major = element_line(colour= \"gray84\"), "
                + "panel.background = element_rect(fill = \"white\", colour = \"white\"), "
                + "legend.position=\"none\", "
                + "legend.key.size = unit(0.2, \"in\"),"
                + "legend.text = element_text(size = 20))+"
                + "theme(axis.text.x = element_blank(),"
                + "axis.text.y = element_text(size=20),"
                + "strip.text.x = element_text(size = 20, colour = \"black\", angle = 0)) +"
                + "coord_polar() +"
                + "guides(colour = guide_legend(override.aes = list(size=3))) + "
                + "xlab(\"\")");
        scriptArray.add("dev.off()");

        scriptArray.add("#");
        scriptArray.add("#");
        
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        
        scriptArray.add("#");
        scriptArray.add("#");
      
        scriptArray.add("#");
        scriptArray.add("#");
        
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "plotIsomiRDistribution-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brAStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brAStdErr.readLine()) != null){
                logger.info(line);
            }
            logger.info("</ERROR>");
            int exitVal = proc.waitFor();            
            logger.info("Process exitValue: " + exitVal);   
            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
    }
    
    private void plotSampleIsoDistance() throws IOException {
        String plotDataFilePath1 = inputFolder+projectID +".isomiR.countTable.tsv";
        String plotPNG = OUTPUTFOLDER+projectID+".sampleDistance-isomiRprofile.png";
        String plotPNG2 = OUTPUTFOLDER+projectID+".isomiR.profile.heatmap.png";
        String plotPNG3 = OUTPUTFOLDER+projectID+".isomiR.profile.heatmap.clean.png";
        
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("#   Rscript generated for project " + projectID);
        scriptArray.add("#   created: " + new Date());
        scriptArray.add("library(plyr)" );
        scriptArray.add("library(reshape2)");
        scriptArray.add("library(ggplot2)");
        scriptArray.add("library(grid)");
        scriptArray.add("library(gridExtra)");
        scriptArray.add("library(gplots)");
        scriptArray.add("x <- read.delim(\""+plotDataFilePath1+"\", sep='\\t', row.names=\"name\" )");
        scriptArray.add("#");
        scriptArray.add("mat.dist <- log2(x+1)");
        scriptArray.add("colnames(mat.dist) = paste(colnames(mat.dist))");
        scriptArray.add("mat.dist = as.matrix(dist(t(mat.dist)))");
        scriptArray.add("mat.dist = mat.dist/max(mat.dist)");
        scriptArray.add("library(RColorBrewer)");
        scriptArray.add("hmcol = colorRampPalette(brewer.pal(9, \"GnBu\"))(16)");
        scriptArray.add("#");
        scriptArray.add("png(file=\""+plotPNG+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("heatmap.2(mat.dist, col = rev(hmcol), symkey = FALSE, trace=\"none\","
                + "margins = c(12, 12),density.info=c(\"density\"),cexRow =1,cexCol = 1)");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("png(file=\""+plotPNG2+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("heatmap.2(as.matrix(log2(x +1)), symkey = FALSE, trace=\"none\", "
                + "margins = c(12, 6),density.info=c(\"density\"),labRow = NA)");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("png(file=\""+plotPNG3+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("heatmap.2(as.matrix(log2(x[ rowSums(x)!=0, ] +1)), symkey = FALSE, "
                + "trace=\"none\", margins = c(12, 6),density.info=c(\"density\"),labRow = NA)");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("#");

        
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "plotSampleDistance.isomiRprofile-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brAStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brAStdErr.readLine()) != null){
                logger.info(line);
            }
            logger.info("</ERROR>");
            int exitVal = proc.waitFor();            
            logger.info("Process exitValue: " + exitVal);   
            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
    }
    
    private void plotTemplatedProfile() throws IOException {
        String plotDataFilePath1 = inputFolder+projectID +".isomiR.templated.tsv";
        String plotPNG = OUTPUTFOLDER+projectID+".templated.profile.png";
        String speciesCommon = "~/hsammuComm/hsa-mmu.miRNA.common.tsv";
        
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("#   Rscript generated for project " + projectID);
        scriptArray.add("#   created: " + new Date());
        scriptArray.add("library(plyr)" );
        scriptArray.add("library(reshape2)");
        scriptArray.add("library(ggplot2)");
        scriptArray.add("library(grid)");
        scriptArray.add("library(gridExtra)");
        scriptArray.add("library(gplots)");
        scriptArray.add("x <- read.delim(\""+plotDataFilePath1+"\", sep='\\t', header=FALSE )");
        scriptArray.add("library(edgeR)");
        scriptArray.add("colnames(x) <- c(\"dataset\", \"group\", \"miRNA\", \"isoform\", \"eventCount\",\"event\",\"readCount\")");
        scriptArray.add("#");
        scriptArray.add("x$isoform <- factor(x$isoform, levels = c(\"lr-5e\",\"lr-3e\"))");
        scriptArray.add("x.single <- x[which(x$eventCount ==1),]");
        scriptArray.add("#");
        scriptArray.add("hsammuCommon <- as.data.frame(read.delim(\"" +speciesCommon +"\",sep=\"\\t\",header = FALSE))");
        scriptArray.add("colnames(hsammuCommon) <- c(\"seqStr\", \"hsaID\",\"mmuID\",\"memrgedID\")");
       
        scriptArray.add("x.single <- x.single[which(x.single$miRNA %in% hsammuCommon$mmuID),]\n" +
"if(\"hsa\" %in% x.single$miRNA[1]){\n" +
"  x.single$miRNA <-  hsammuCommon$memrgedID[match(x.single$miRNA, hsammuCommon$hsaID )]\n" +
"} else{\n" +
"  x.single$miRNA <- hsammuCommon$memrgedID[match(x.single$miRNA, hsammuCommon$mmuID)] \n" +
"}");
        scriptArray.add("png(file=\""+plotPNG+"\",res = 300,units = 'in',width = 20, height = 30)");
        scriptArray.add("#");
        scriptArray.add("ggplot(x.single, aes(fill=event, y=log(cpm(readCount)), x=miRNA)) + "
                + "geom_bar( stat=\"identity\") +"
                + "facet_grid(dataset ~ isoform) + "
                + "theme(legend.position=\"left\", "
                + "legend.key.size = unit(0.6, \"in\"),"
                + "legend.title =element_text(size = 20),  "
                + "legend.text = element_text(size = 20))+"
                + "theme(axis.text.x = element_text(angle=90, hjust=1, vjust=0,size=10),"
                + "axis.text.y = element_text(size=15)) +theme(axis.title.x = element_text(size = 20),"
                + "axis.title.y = element_text(size = 20))+ "
                + "scale_fill_manual(values=c(\"blue\",\"steelblue1\",\"turquoise1\",\"slateblue1\", "
                + "       \"lightgreen\",\"green4\",\"seagreen2\",\"yellowgreen\",    "
                + "       \"palevioletred1\",\"plum\",\"red\",\"magenta\",  "
                + "    \"yellow\",\"palegoldenrod\",\"navajowhite\",\"tan1\")) + "
                + "theme(strip.text.y = element_text(size = 25, "
                + "colour = \"black\", angle = 0,face=\"bold\"),"
                + "strip.text.x = element_text(size = 25, "
                + "colour = \"black\", angle = 0,face=\"bold\")) +"
                + "xlab(\"miRNAs\") +ylab(\"log(cpm(mean read count))\")");
        scriptArray.add("#");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");

        
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "plotTemplateProfile-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brAStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brAStdErr.readLine()) != null){
                logger.info(line);
            }
            logger.info("</ERROR>");
            int exitVal = proc.waitFor();            
            logger.info("Process exitValue: " + exitVal);   
            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
    }
    
    private void plotCorrSingleDouble() throws IOException {
        String plotDataFilePath = inputFolder+projectID +".isomiR.templated.mixing.tsv";
        String plotPNG = OUTPUTFOLDER+projectID+".TemplatedCorrelation.profile.png";
        
        
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("#   Rscript generated for project " + projectID);
        scriptArray.add("#   created: " + new Date());
        scriptArray.add("library(plyr)" );
        scriptArray.add("library(reshape2)");
        scriptArray.add("library(ggplot2)");
        scriptArray.add("library(grid)");
        scriptArray.add("library(gridExtra)");
        scriptArray.add("library(gplots)");
        scriptArray.add("library(edgeR)");
        scriptArray.add("x.mix <- read.delim(\""+plotDataFilePath+"\", sep='\\t', header=FALSE )");
        
        scriptArray.add("colnames(x.mix) <- c(\"dataset\", \"group\", \"miRNA\", \"isoform\",\"type\",\"readCount\")");
        scriptArray.add("#");
        scriptArray.add("x.mix.sum <- aggregate(x.mix$readCount, by=list(x.mix$dataset, x.mix$miRNA, x.mix$isoform, x.mix$type), FUN=sum)");
        scriptArray.add("colnames(x.mix.sum) <- c(\"dataset\",\"miRNA\",\"isoform\",\"type\",\"sumCount\")");
        scriptArray.add("panel.cor <- function(x, y, digits = 2, prefix = \"\", cex.cor, ...)\n" +
"{\n" +
"    usr <- par(\"usr\"); on.exit(par(usr))\n" +
"    par(usr = c(0, 1, 0, 1))\n" +
"    r <- abs(cor(x, y))\n" +
"    txt <- format(c(r, 0.123456789), digits = digits)[1]\n" +
"    txt <- paste0(prefix, txt)\n" +
"    if(missing(cex.cor)) cex.cor <- 0.8/strwidth(txt)\n" +
"    text(0.5, 0.5, txt, cex = cex.cor * r)\n" +
"}\n" +
"\n" +
"panel.hist <- function(x,...){\n" +
"  usr <- par(\"usr\")\n" +
"   on.exit(par(usr))\n" +
"   par(usr=c(usr[1:2], 0,1.5))\n" +
"   h <- hist(x,plot=FALSE)\n" +
"   breaks <- h$breaks\n" +
"   nB <- length(breaks)\n" +
"   y <- h$counts\n" +
"   y <- y/max(y)\n" +
"   rect(breaks[-nB],0,breaks[-1], y, col=\"white\", ...)\n" +
"}");
        scriptArray.add("x.mix.matrix <- dcast(x.mix.sum,  dataset+miRNA ~ type, value.var=\"sumCount\")");
        scriptArray.add("x.mix.matrix[is.na(x.mix.matrix)] <- 0");
        scriptArray.add("rownames(x.mix.matrix) <- paste(x.mix.matrix$dataset, x.mix.matrix$group, x.mix.matrix$miRNA, sep = \"_\")");
        scriptArray.add("x.mix.matrix.ratio <- x.mix.matrix[,3:8]/rowSums(x.mix.matrix[,3:8])");
        scriptArray.add("x.mix.matrix.ratio <- cbind(x.mix.matrix.ratio, do.call(rbind,strsplit(as.character(rownames(x.mix.matrix.ratio)), \"_\")))");
        scriptArray.add("colnames(x.mix.matrix.ratio) <- c(\"N\", \"NN\", \"NT\",  \"T\",  \"TN\", \"TT\", \"dataset\", \"group\",\"miRNA\")");
        scriptArray.add("png(file=\""+plotPNG+"\",res = 300,units = 'in',width = 10, height = 10)");
        scriptArray.add("pairs(x.mix.matrix.ratio[,1:6], lower.panel =panel.cor, upper.panel = panel.smooth,diag.panel = panel.hist )");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");
        scriptArray.add("#");

        
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "plotTemplatedCorrelation-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brAStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brAStdErr.readLine()) != null){
                logger.info(line);
            }
            logger.info("</ERROR>");
            int exitVal = proc.waitFor();            
            logger.info("Process exitValue: " + exitVal);   
            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
    }
    
    private void plotingALLlevels() throws IOException {
//        String plotDataFilePath = inputFolder+projectID +".isomiR.templated.mixing.tsv";
//        String plotPNG = OUTPUTFOLDER+projectID+".TemplatedCorrelation.profile.png";
         
        
        ArrayList<String> scriptArray = new ArrayList<>();
        scriptArray.add("#");
        scriptArray.add("## Author: Xiangfu (Joey) ZHONG");
        scriptArray.add("#");
        scriptArray.add("outputFolder <- \""+OUTPUTFOLDER+"\"");
        scriptArray.add("workDir <- \""+projectFolder+"\"");
        scriptArray.add("#");
        scriptArray.add("## load packages ");
        scriptArray.add("library('plyr')");
        scriptArray.add("library('reshape2')");
        scriptArray.add("library('ggplot2')");
        scriptArray.add("library('grid')");
        scriptArray.add("library('gridExtra')");
        scriptArray.add("library('psych')");
        scriptArray.add("library('scales')");
        scriptArray.add("library('gplots')");
        scriptArray.add("library('GGally')");
        scriptArray.add("library('reshape')");
        scriptArray.add("library('stringr')");
        scriptArray.add("#");
        scriptArray.add("# source(\"https://bioconductor.org/biocLite.R\")");
        scriptArray.add("# biocLite(\"edgeR\")");
        scriptArray.add("#");
        scriptArray.add("################################ ");
        scriptArray.add("## read in data information table");
        scriptArray.add("SAMPLE_INFO_dataTable <- read.delim(\""+datasetlstFile+"\", sep='\\t', header = TRUE )");
        scriptArray.add("SAMPLE_INFO_dataTable$File <- gsub(\".fastq.gz\", \"\", SAMPLE_INFO_dataTable$File)");
        scriptArray.add("##change column name");
        scriptArray.add("colnames(SAMPLE_INFO_dataTable) <- c(\"sample\", \"species\", \"strain\", \"timepoint\",\"rep\")");
        scriptArray.add("#");
        scriptArray.add("################################ ");
        scriptArray.add("## read in level 0 count table");
        scriptArray.add("lev_0_DF <- read.delim(paste(workDir,\"PlottingData/unique/"+projectID+".isomiR.Level-0.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## merge with sample information");
        scriptArray.add("lev_0_DF <- merge(lev_0_DF, SAMPLE_INFO_dataTable[c(1,3,4,5)], by=\"sample\")");
        scriptArray.add("## ploting and output figure");
        scriptArray.add("png(paste(outputFolder, \"lev_0-onlyMature.png\", sep=\"\"), res = 500,units = 'in',width = 9, height = 12)");
        scriptArray.add("ggplot(lev_0_DF, aes(x=paste(strain, timepoint, rep), y=miRNA, fill=log2(count))) +geom_tile() +scale_fill_distiller(palette =\"Spectral\", direction=-1, space = \"Lab\", na.value = \"black\",name=\"log2(count)\") +theme(axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0.5), axis.text.y = element_text( hjust = 1, vjust = 0.5), axis.title=element_text(size=20), legend.position = \"right\") +xlab(\"sample(strain,timepoint,rep)\")");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("################################ ");
        scriptArray.add("## read in level 1 count table");
        scriptArray.add("lev_1_DF <- read.delim(paste(workDir,\"PlottingData/unique/"+projectID+".isomiR.Level-1.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## merge with sample information");
        scriptArray.add("lev_1_DF <- merge(lev_1_DF, SAMPLE_INFO_dataTable[c(1,3,4,5)], by=\"sample\")");
        scriptArray.add("## ploting and output figure");
        scriptArray.add("png(paste(outputFolder, \"lev_1-maturePercentage.png\", sep=\"\"), res = 500,units = 'in',width = 9, height = 12)");
        scriptArray.add("ggplot(lev_1_DF, aes(x=paste(strain, timepoint, rep), y=miRNA, fill=matureCount/(isomiRcount+matureCount))) +geom_tile() +scale_fill_distiller(palette =  \"Spectral\", direction=-1, space = \"Lab\", na.value = \"black\", name=\"miRNA/(miRNA+isomiR)\") +theme(axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0.5), axis.text.y = element_text( hjust = 1, vjust = 0.5), axis.title=element_text(size=20), legend.position = \"bottom\") +xlab(\"sample(strain,timepoint,rep)\")");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("################################");
        scriptArray.add("## read in level 2 count table");
        scriptArray.add("lev_2_DF <- read.delim(paste(workDir,\"PlottingData/unique/"+projectID+".isomiR.Level-2.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## merge with sample information");
        scriptArray.add("lev_2_DF <- merge(lev_2_DF, SAMPLE_INFO_dataTable[c(1,3,4,5)], by=\"sample\")");
        scriptArray.add("## melt data frame");
        scriptArray.add("lev_2_DF_melt <-  melt(lev_2_DF[-c(7)], id.vars = c('sample','miRNA','strain','timepoint','rep'))");
        scriptArray.add("levels(lev_2_DF_melt$variable )");
        scriptArray.add("levels(lev_2_DF_melt$variable ) <- c(\"mature miRNA\", \"SameLength isomiRs\", \"shorter isomiRs\", \"longer isomiRs\")");
        scriptArray.add("## ploting and output figure");
        scriptArray.add("png(paste(outputFolder, \"lev_2-isomiR-in-length.png\", sep=\"\"), res = 500,units = 'in',width = 10, height = 20)");
        scriptArray.add("ggplot(lev_2_DF_melt, aes(x=paste(strain, timepoint, rep), y=miRNA, fill=log2(value))) +geom_tile() +facet_wrap(~variable) +scale_fill_distiller(palette =  \"Spectral\", direction=-1, space = \"Lab\", na.value = \"black\", name=\"log2(count)\") +theme(axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0.5), axis.text.y = element_text( hjust = 1, vjust = 0.5), axis.title=element_text(size=20), legend.position = \"bottom\", strip.text.x = element_text(size = 22)) +xlab(\"sample(strain,timepoint,rep)\")");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("################################");
        scriptArray.add("## read in level 3 count table");
        scriptArray.add("lev_3_DF <- read.delim(paste(workDir, \"PlottingData/unique/"+projectID+".isomiR.Level-3.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## merge with sample information");
        scriptArray.add("lev_3_DF <- merge(lev_3_DF, SAMPLE_INFO_dataTable[c(1,3,4,5)], by=\"sample\")");
        scriptArray.add("## melt data frame");
        scriptArray.add("lev_3_DF_melt <-  melt(lev_3_DF[-c(29)], id.vars = c('sample','miRNA','strain','timepoint','rep'))");
        scriptArray.add("nrow(lev_3_DF_melt)");
        scriptArray.add("lev_3_DF_melt <- lev_3_DF_melt[which(lev_3_DF_melt$value > 0),]");
        scriptArray.add("nrow(lev_3_DF_melt)");
        scriptArray.add("lev_3_DF_melt$variable <- gsub(\"\\\\.\", \"-\", lev_3_DF_melt$variable)");
        scriptArray.add("lev_3_DF_melt$variable <- factor(lev_3_DF_melt$variable, levels = c(\"mature\", \"poly\", \"sl-5d-3e\", \"sl-5d-3e-p\", \"sl-5e-3d\", \"sl-5e-3d-p\", \"sr-5d\",  \"sr-5d-p\", \"sr-3d\", \"sr-3d-p\",\"sr-5d-3d\",\"sr-5d-3d-p\", \"sr-5d-3e\",  \"sr-5d-3e-p\", \"sr-5e-3d\",\"sr-5e-3d-p\", \"lr-5e\", \"lr-5e-p\",  \"lr-3e\",  \"lr-3e-p\",  \"lr-5e-3e\", \"lr-5e-3e-p\", \"lr-5d-3e\",   \"lr-5d-3e-p\", \"lr-5e-3d\", \"lr-5e-3d-p\")) ");
        scriptArray.add("## ploting and output figure");
        scriptArray.add("png(paste(outputFolder, \"lev_3-isomiR-in-length.png\", sep=\"\"), res = 500,units = 'in',width = 21, height = 30)");
        scriptArray.add("ggplot(lev_3_DF_melt, aes(x=paste(strain, timepoint, rep), y=miRNA, fill=log2(value))) +geom_tile() +facet_wrap(~variable, ncol = 4) +scale_fill_distiller(palette =  \"Spectral\", direction=-1, space = \"Lab\", na.value = \"black\", name=\"log2(count)\") +theme(axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0.5), axis.text.y = element_text(size=4, hjust = 1, vjust = 0.5), axis.title=element_text(size=20), legend.position = \"bottom\", strip.text.x = element_text(size = 25)) +xlab(\"sample(strain,timepoint,rep)\") ");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("################################ ");
        scriptArray.add("## read in level 4 count table");
        scriptArray.add("lev_4_DF <- read.delim(paste(workDir, \"PlottingData/unique/"+projectID+".isomiR.Level-4.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## merge with sample information");
        scriptArray.add("lev_4_DF <- merge(lev_4_DF, SAMPLE_INFO_dataTable[c(1,3,4,5)], by=\"sample\")");
        scriptArray.add("## ploting and output figure");
        scriptArray.add("png(paste(outputFolder, \"lev_4-isomiR-single.png\", sep=\"\"), res = 500,units = 'in',width = 10, height = 20)");
        scriptArray.add("ggplot(lev_4_DF, aes(x=paste(strain, timepoint, rep), y=paste(miRNA,isomiR.group,isomiR.info), fill=log2(count))) +geom_tile()  +scale_fill_distiller(palette =  \"Spectral\", direction=-1, space = \"Lab\", na.value = \"black\", name=\"log2(count)\") +theme(axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0.5), axis.text.y = element_blank(), axis.ticks.y = element_blank(), axis.title=element_text(size=20), legend.position = \"bottom\") +xlab(\"sample(strain,timepoint,rep)\")");
        scriptArray.add("dev.off()");
        scriptArray.add("#");
        scriptArray.add("################################ ");
        scriptArray.add("## read in count data on arm usage");
        scriptArray.add("armUsage <- read.delim(paste(workDir, \"PlottingData/unique/"+projectID+".precursor_armUsage.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## read in arm profile");
        scriptArray.add("armProfile <- read.delim(paste(workDir, \"PlottingData/unique/"+projectID+".armProfile.tsv\", sep=\"\"), sep='\\t', header = TRUE )");
        scriptArray.add("## merge sample information");
        scriptArray.add("armUsage <- merge(armUsage, SAMPLE_INFO_dataTable[c(1,3,4,5)], by=\"sample\")");
        scriptArray.add("## melt data frame");
        scriptArray.add("armUsage_melt <- melt(armUsage[-c(7,8,9,10)], id.vars = c('sample','precursor','strain','timepoint','rep'))");
        scriptArray.add("## set levels");
        scriptArray.add("levels(armUsage_melt$variable) <- c(\"miRNA_5p\", \"miRNA+isomiR_5p\",    \"miRNA_3p\", \"miRNA+isomiR_3p\" )");
        scriptArray.add("armUsage_melt$variable <- factor(armUsage_melt$variable, levels = c(\"miRNA_5p\", \"miRNA+isomiR_5p\",    \"miRNA_3p\", \"miRNA+isomiR_3p\" ))");
        scriptArray.add("## cbind variables");
        scriptArray.add("armUsage_melt <- cbind(armUsage_melt, str_split_fixed(armUsage_melt$variable, \"_\", 2))");
        scriptArray.add("## set column name");
        scriptArray.add("colnames(armUsage_melt) <- c(\"sample\", \"precursor\", \"strain\", \"timepoint\", \"rep\", \"variable\", \"value\", \"var1\", \"var2\")");
        scriptArray.add("## set factors");
        scriptArray.add("armUsage_melt$var2 <- factor(armUsage_melt$var2, levels = c(\"5p\", \"3p\"))");
        scriptArray.add("## ploting and output figure");
        scriptArray.add("png(paste(outputFolder, \"arm-usage.png\", sep=\"\"), res = 500,units = 'in',width = 18, height = 24)");
        scriptArray.add("ggplot(armUsage_melt, aes(x=paste(strain, timepoint, rep), y=precursor, fill=log2(value))) +geom_tile() +facet_grid(var1 ~ var2) +scale_fill_distiller(palette =  \"Spectral\", direction=-1, space = \"Lab\", na.value = \"black\", name=\"log2(count)\") +theme(axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0.5), axis.text.y = element_text(size=15, hjust = 1, vjust = 0.5), axis.title=element_text(size=20), legend.position = \"bottom\",strip.text = element_text(size = 20)) +xlab(\"sample(strain,timepoint,rep)\") ");
        scriptArray.add("dev.off()");
        scriptArray.add("#");


        
        BigInteger big = new BigInteger(16, new Random());
        String randomName = new BigInteger(16, new SecureRandom()).toString(32);
        String rScriptFilename = OUTPUTFOLDER  + "ploting_ALL_levels_countdata-"+randomName + ".R";
        try{
            BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rScriptFilename)));
                for(String cmd: scriptArray){
                    bwRC.write(cmd + "\n");
                }
            bwRC.close();
        }
        catch(IOException exIO){
            logger.error("error writing generated RScript to file <" + rScriptFilename + ">");
            logger.error(exIO);
            throw new IOException(TASK_FOLDER + ": error writing generated RScript to file <" + rScriptFilename + ">");
        }
        
        ArrayList<String> executeCmd = new ArrayList<>();
        executeCmd.add(R_PATH);
        executeCmd.add(rScriptFilename);
        String cmdRunRScript = StringUtils.join(executeCmd, " ");
        cmdRunRScript = cmdRunRScript.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
        logger.info("Rscript command:\t" + cmdRunRScript);

        try{
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            BufferedReader brAStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader brAStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brAStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brAStdErr.readLine()) != null){
                logger.info(line);
            }
            logger.info("</ERROR>");
            int exitVal = proc.waitFor();            
            logger.info("Process exitValue: " + exitVal);   
            brAStdin.close();
            brAStdErr.close();        
        }
        catch(IOException exIO){
            logger.info("error executing RScript command\n" + cmdRunRScript);
            logger.info(exIO);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
        catch(InterruptedException exIE){
            logger.info("error executing RScript command\n" + exIE);            
            logger.info(exIE);
            throw new IOException(TASK_FOLDER + ": error executing RScript command\n" + cmdRunRScript);
        }
    }
    
    public HashMap<String, String> updateConfig() {
        /*
        updating configuration hash, passing it to next step
        */
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        CONFIG_HASH.replace("DATASET_ARRAY_STR", StringUtils.join(datasetArray, "#"));
        return CONFIG_HASH;
    }
}
