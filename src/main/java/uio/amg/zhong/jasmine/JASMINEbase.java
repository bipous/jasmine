/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author xiangfuz
 */
public interface JASMINEbase {
    void                verifyInputData() throws IOException;
    void                verifyOutputData() throws  IOException;
    void                setConfigurations(HashMap<String, String> CONFIG_HASH) throws IOException;
//    HashMap             generateExampleConfigurationData();
    void                executeThisStep() throws IOException;
}
