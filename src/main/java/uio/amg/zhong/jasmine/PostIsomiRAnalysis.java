/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class PostIsomiRAnalysis {

    Logger logger = LogManager.getRootLogger();
    private String TASK_FOLDER = "PlottingData";
    private String HOST;
    private String OUTPUTFOLDER;
    private String projectID;
    private String mirBaseVersion;
    private int mismatch;
    private String inputFolder;
    private String datasetlstFile;
    private int threads;
    protected final String FILESEPARATOR = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder;
    private static final String RAW_INPUT_EXTENSION = ".fastq.gz";
    private int thisDatasetThreshold;
    private ArrayList<String> iLengthVarArrayList;
    private ArrayList<String> groupVariabilty;
    private HashMap<String, Integer[]> iGroupVariabiltyHash;
    private HashMap<String, HashMap> isomirCompHashMap;
    private ArrayList<String> readCountArrayList;
    private HashMap<String, Integer> iPooledCountHashMap;
    private HashMap<String, Integer> iIsomiRDistHashMap;
    private ArrayList<String> isomirDistbutionArrayList;
    private ArrayList<String> isomirCompArrayList;
    private ArrayList<String> eventSummaryArray;
    private HashMap<String, HashMap> ntShiftingHash;
    private HashMap<String, HashMap> iTemplatedHashMap;
    private HashMap<String, String> mirnaLenHash;
    private HashMap<String, HashMap> iLengthVarHashMap;
    private ArrayList<String> polyEventArrayList;
    private ArrayList<String> DATASETARRAYLIST;
    private ArrayList<String> TEMPLATEDStrArray;
    private boolean DOFILTER;
    private ArrayList<String> extension1NTArray; // single fent extension
    private ArrayList<String> extension2NTArray; // up to 2 nt extension
    private ArrayList<String> extensionALLArray; // up to 2 nt extension
    private HashMap<String, Integer> countSingleInMir;
    private ArrayList<String> countSingleTable;
    private ArrayList<String> singleModIsoArrayList;
    private HashMap<String, HashMap> miRNA2isomirHash;
    private ArrayList<String> isomirProporationArray;
    private HashMap<String, Integer> countingIsomiRHashMap;
    private String FILTER_THRESHOLD;
    private HashMap<String, String> CONFIG_HASH;
    private String OUTPUT_FOLDER_unique;
    private String OUTPUT_FOLDER_amgiguous;
    private String[] ISOFORMTYPE = {"mature", "poly", "sl-5d-3e", "sl-5d-3e-p", "sl-5e-3d",
        "sl-5e-3d-p", "sr-5d", "sr-5d-p", "sr-3d", "sr-3d-p", "sr-5d-3d",
        "sr-5d-3d-p", "sr-5d-3e", "sr-5d-3e-p", "sr-5e-3d", "sr-5e-3d-p",
        "lr-5e", "lr-5e-p", "lr-3e", "lr-3e-p", "lr-5e-3e", "lr-5e-3e-p",
        "lr-5d-3e", "lr-5d-3e-p", "lr-5e-3d", "lr-5e-3d-p"};

    private JASMINEutility misoUtility;
    private HashMap<String, Integer> isomiR_lev_0_unique_hash;
    private HashMap<String, Integer> isomiR_lev_0_amgiguous_hash;

    private HashMap<String, int[]> isomiR_lev_1_unique_hash;
    private HashMap<String, int[]> isomiR_lev_1_amgiguous_hash;

    private HashMap<String, int[]> isomiR_lev_2_unique_hash;
    private HashMap<String, int[]> isomiR_lev_2_amgiguous_hash;

    private HashMap<String, int[]> isomiR_lev_3_unique_hash;
    private HashMap<String, int[]> isomiR_lev_3_amgiguous_hash;

    private HashMap<String, Integer> isomiR_lev_4_unique_hash;
    private HashMap<String, Integer> isomiR_lev_4_amgiguous_hash;

    private HashMap<String, int[]> isomiR_lev_5_unique_hash;
    private HashMap<String, int[]> isomiR_lev_5_amgiguous_hash;

    private HashMap<String, String> miR_armProfile_Hash;
    private HashMap<String, int[]> isomiR_armCounting_hash;
    private ArrayList<String> isomiR_polyEvent_Array;
    private ArrayList<String> isomiR_extension_Array ;

    PostIsomiRAnalysis(HashMap<String, String> CONFIGHASH) throws IOException {
        CONFIG_HASH = CONFIGHASH;
        setConfigurations(CONFIG_HASH);
//        startPostAnalysis();
        startPostAnalysis_V2();
    }

    private void setConfigurations(HashMap<String, String> CONFIG_HASH) {
        this.HOST = CONFIG_HASH.get("HOST");
        this.mirBaseVersion = CONFIG_HASH.get("MIRBASE_VERSION");
        this.mismatch = Integer.valueOf(CONFIG_HASH.get("MISMATCH"));
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.threads = Integer.valueOf(CONFIG_HASH.get("THREADS"));

        this.FILTER_THRESHOLD = CONFIG_HASH.get("READ_FILTERING_COUNT");

        if (CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)) {
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        } else {
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER") + FILESEPARATOR;
        }
        if (new File(CONFIG_HASH.get("INPUTFOLDER")).isDirectory()) {
            this.inputFolder = CONFIG_HASH.get("INPUTFOLDER");
        } else {
            this.inputFolder = projectFolder + CONFIG_HASH.get("INPUTFOLDER") + FILESEPARATOR;
        }
        this.OUTPUTFOLDER = projectFolder + TASK_FOLDER + FILESEPARATOR;
        this.OUTPUT_FOLDER_unique = OUTPUTFOLDER + "unique" + FILESEPARATOR;
        this.OUTPUT_FOLDER_amgiguous = OUTPUTFOLDER + "amgiguous" + FILESEPARATOR;

        if (CONFIG_HASH.containsKey("DATASETLIST")) {
            this.datasetlstFile = CONFIG_HASH.get("DATASETLIST");
        } else {
            this.datasetlstFile = projectFolder + "configs" + FILESEPARATOR + "miRNASeqData.tsv";
        }
    }

    private void startPostAnalysis() throws IOException {
        JASMINEutility misoUtility = new JASMINEutility();

        mirnaLenHash = new HashMap<>();
        misoUtility.readFasta(projectFolder + "Reference" + FILESEPARATOR + "miRBase." + HOST + ".unique.mature.fasta");
        mirnaLenHash = misoUtility.getSeqLengthHash();

        Boolean fA = new File(OUTPUTFOLDER).mkdir(); // output folder
        if (fA) {
            logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
        }
        Boolean fA_unique = new File(OUTPUT_FOLDER_unique).mkdir(); // sub output folder for unique isomiRs
        if (fA_unique) {
            logger.info("created output folder <" + OUTPUT_FOLDER_unique + "> for results");
        }
        Boolean fA_amgiguous = new File(OUTPUT_FOLDER_amgiguous).mkdir(); // sub output folder for amgiguous isomiRs
        if (fA_amgiguous) {
            logger.info("created output folder <" + OUTPUT_FOLDER_amgiguous + "> for results");
        }

        this.DATASETARRAYLIST = getDatasetList(datasetlstFile);

        groupVariabilty = new ArrayList<>();
        iGroupVariabiltyHash = new HashMap<>();

        readCountArrayList = new ArrayList<>();
        iPooledCountHashMap = new HashMap<>();

        ArrayList<String> matureDrievnInfo = readOnlyMature(projectFolder + "Reference" + FILESEPARATOR + "miRBase." + HOST + ".matureInHairpin.tsv");

        ArrayList<String> underThresholdArray = new ArrayList<>();  // count how many reads under threshold in each dataset

        String[] ISOFORMTYPE = {"mature", "poly", "sl-5d-3e", "sl-5d-3e-p", "sl-5e-3d",
            "sl-5e-3d-p", "sr-5d", "sr-5d-p", "sr-3d", "sr-3d-p", "sr-5d-3d",
            "sr-5d-3d-p", "sr-5d-3e", "sr-5d-3e-p", "sr-5e-3d", "sr-5e-3d-p",
            "lr-5e", "lr-5e-p", "lr-3e", "lr-3e-p", "lr-5e-3e", "lr-5e-3e-p",
            "lr-5d-3e", "lr-5d-3e-p", "lr-5e-3d", "lr-5e-3d-p"};

        HashMap<String, Integer> tmpisoHashMap = new HashMap<>();
        for (String itype : ISOFORMTYPE) {
            tmpisoHashMap.put(itype, 0);
        }

        iTemplatedHashMap = new HashMap<>();
        iPooledCountHashMap = new HashMap<>();

        ArrayList<String> isoHierarchy = new ArrayList<>();
        ArrayList<String> targetisomiArrayList = new ArrayList<>();
        for (Map.Entry<String, String> entry : mirnaLenHash.entrySet()) {
            String key = entry.getKey();
            for (String itype : ISOFORMTYPE) {
                targetisomiArrayList.add(key + ":" + itype);
            }
        }
        String headline = "name";

        ArrayList<String> isomirCountArray = new ArrayList<>();
        isomirCountArray.add(headline);
        HashMap<String, HashMap<String, Integer>> isomiRdatasetHashMap = new HashMap<String, HashMap<String, Integer>>();

        String mappingSummaryFile = projectFolder + "MappedReads/"  + projectID + ".mappingSummary.tsv";
        HashMap<String, String> mappingCountSummary = readMappingSummary(mappingSummaryFile);

        ArrayList<String> updatedMappingSummary = new ArrayList<>();
        updatedMappingSummary.add("dataset\ttotal\tmapped\tunmap\treliable\tambiguous\tothers");

        /*
        to avoid ## java.lang.OutOfMemoryError: GC overhead limit exceeded## 
        we append parsed text to files
        so here create empty files first, after parsed every file, append text to files
         */
        String[] outputFileSuffix = {".isomiR.templated.tsv",
            ".mature.countTable.tsv", ".isomiR.polyPosDistribution.tsv", ".isomiR.polyEventDistribution.tsv",
            ".mappingSummary.tsv", ".isomiR.distribution.tsv", ".isomiR.composition.tsv", ".isomiR.lengthVariation.tsv",
            ".isomiR.eventSummary.tsv", ".isomiR.templated.mixing.tsv", ".isomiR.singleForm.tsv",
            ".countSingleForm.tsv", ".isomiR.proportion.tsv"};
        for (String isuffix : outputFileSuffix) {
            try {
                File ifile = new File(OUTPUTFOLDER + projectID + isuffix);

                if (ifile.exists() && !ifile.isDirectory()) {
                    if (ifile.delete()) {
                        logger.info("Existed file deleted successfully: " + ifile.toString());
                    } else {
                        logger.info("Failed to delete Existed file: " + ifile.toString());
                    }
                }
                ifile.getParentFile().mkdirs();
                ifile.createNewFile();
            } catch (Exception e) {
            }
        }

        for (String idataset : DATASETARRAYLIST) {
            /*
            ##################################################################
            START
            some data need append to file
             */
            iLengthVarHashMap = new HashMap<>();
            isomirCompHashMap = new HashMap<>();
            miRNA2isomirHash = new HashMap<>();
            TEMPLATEDStrArray = new ArrayList<>();
            polyEventArrayList = new ArrayList<>();
            isomirDistbutionArrayList = new ArrayList<>();
            isomirCompArrayList = new ArrayList<>();
            iLengthVarArrayList = new ArrayList<>();
            extensionALLArray = new ArrayList<>();
            singleModIsoArrayList = new ArrayList<>();
            countSingleTable = new ArrayList<>();
            isomirProporationArray = new ArrayList<>();

            countingIsomiRHashMap = new HashMap<>();
            for (String imature : matureDrievnInfo) {
                for (String itype : ISOFORMTYPE) {
                    countingIsomiRHashMap.put((imature + ":" + itype), 0);
                }
            }

            iIsomiRDistHashMap = new HashMap<>();
            for (String itype : ISOFORMTYPE) {
                iIsomiRDistHashMap.put(itype, 0);
//                countingIsomiRHashMap.put(itype, 0);
            }

            /*
            END
            ##################################################################
             */
            countSingleInMir = new HashMap<>();
            String datasetName = idataset.replace(RAW_INPUT_EXTENSION, "");

            /*
            decide the threshold for counting on each read
             */
            if (FILTER_THRESHOLD == null) {
                thisDatasetThreshold = 0;
            } else {
                try {
                    thisDatasetThreshold = Integer.valueOf(FILTER_THRESHOLD);
                } catch (Exception e) {
                    logger.error("could not convert READ_FILTERING_COUNT into Integer: " + FILTER_THRESHOLD.toString());
                }
            }


            int reliableTotal = 0;
            int ambiguousTotal = 0;
            int unmappedTotal = 0;
            int totalSum = 0;
            int mappedSum = 0;

            int totalCount = 0;
            int matureCount = 0;
            int isomiRcount = 0;
            int polyCount = 0;
            int shorterCount = 0;
            int longerCount = 0;
            int samelenCount = 0;

            int topReliableCount = 0;
            int topAmbiguousCount = 0;
            int nonTopCount = 0;

            int sumUnderThreshold = 0;

            HashMap<String, Integer> isomirCounting = new HashMap<>();
            for (String isString : ISOFORMTYPE) {
                isomirCounting.put(isString, 0);
            }

            HashMap<String, Integer> idatasetCoitingIsomiR = new HashMap<>();
            for (String jmirnaString : targetisomiArrayList) {
                idatasetCoitingIsomiR.put(jmirnaString, 0);
            }

            String idatasetFilename = projectFolder + "IsomiRsReports" + FILESEPARATOR + idataset.replace(RAW_INPUT_EXTENSION, "") + ".isomiRs.report.tsv";
            try {
                logger.info("reading isomiR report file <" + idatasetFilename + ">");
                BufferedReader reportBR = new BufferedReader(new FileReader(new File(idatasetFilename)));
                String reportLine = null;
                while ((reportLine = reportBR.readLine()) != null) {
                    /*
                    get isomiR name; read count and parental miRNA name 
                     */
                    String lineString = reportLine.trim();
                    String imirna = lineString.split("\t")[2];
                    String isotype = lineString.split("\t")[5];
                    int isocopy = Integer.valueOf(lineString.split("\t")[4]);
                    String isomirName = lineString.split("\t")[3];

                    /*
                    summary ambiguous miRNA read count
                     */
                    totalSum = totalSum + isocopy;
                    if (imirna.contains("*")) {
                        ambiguousTotal = ambiguousTotal + isocopy;
                    } else {
                        reliableTotal = reliableTotal + isocopy;
                        /*
                        build isomiR count table
                         */
//                        if(countingIsomiRHashMap.containsKey(isomirName)){
//                            countingIsomiRHashMap.replace(isomirName, (countingIsomiRHashMap.get(isomirName) + isocopy));
//                        }
//                        else{
//                            countingIsomiRHashMap.put(isomirName, isocopy);
//                        }
                        if (countingIsomiRHashMap.containsKey(isomirName)) {
                            countingIsomiRHashMap.replace(isomirName, (countingIsomiRHashMap.get(isomirName) + isocopy));
                        } else {
                            logger.error("MISSING this key !!!\t\t" + isomirName + "\t\t" + lineString);
                        }
                    }

                    /*
                    start 
                     */
                    if (isocopy > thisDatasetThreshold) {
                        if (idatasetCoitingIsomiR.containsKey(isomirName)) {
                            idatasetCoitingIsomiR.replace(isomirName, (isocopy + idatasetCoitingIsomiR.get(isomirName)));
                        }

                        totalCount = totalCount + isocopy;
                        if (isotype.equals("mature")) {
                            matureCount = matureCount + isocopy;
                        } else {
                            isomiRcount = isomiRcount + isocopy;
                            if (isotype.equals("poly")) {
                                polyCount = polyCount + isocopy;
                            }
                            if (isotype.startsWith("sl")) {
                                samelenCount = samelenCount + isocopy;
                            }
                            if (isotype.startsWith("lr")) {
                                longerCount = longerCount + isocopy;
                            }
                            if (isotype.startsWith("sr")) {
                                shorterCount = shorterCount + isocopy;
                            }
                        }
                        isomirCounting.replace(isotype, (isocopy + isomirCounting.get(isotype)));

                        /*
                        parse features
                         */
                        checkingTemplated(lineString.split("\t"));
                        lengthVariation(lineString);
                        groupingVariability(lineString);
                        readCountMature(lineString);
                        checkPolyPosition(lineString);
                        isomiRdistribution(lineString); // from an overview point
                        isomirComposition(lineString); // from an individual view point
                        lengthVariation(lineString); // check length variation
                        //                    summaryEvent(lineString); // taking too much storage

                        extensionCheck(lineString);
                        singleModificationIsoform(lineString);

                        howManySingleInMir(lineString);
                    } else {
                        // some thing to do with reads tht under threshold
                        sumUnderThreshold += isocopy;
                    }
                }
                reportBR.close();
                logger.info("done");
                String tmpDataInfo = idataset + "\t" + idataset.split("\\.")[0] + "." + idataset.split("\\.")[1];
                isoHierarchy.add(tmpDataInfo + "\tA\t" + "mature" + "\t" + String.valueOf(matureCount / (double) totalCount));
                isoHierarchy.add(tmpDataInfo + "\tA\t" + "isomiRs" + "\t" + String.valueOf(isomiRcount / (double) totalCount));

                isoHierarchy.add(tmpDataInfo + "\tB\t" + "mature" + "\t" + String.valueOf(matureCount / (double) totalCount));
                isoHierarchy.add(tmpDataInfo + "\tB\t" + "poly" + "\t" + String.valueOf(polyCount / (double) totalCount));
                isoHierarchy.add(tmpDataInfo + "\tB\t" + "samelength" + "\t" + String.valueOf(samelenCount / (double) totalCount));
                isoHierarchy.add(tmpDataInfo + "\tB\t" + "longer" + "\t" + String.valueOf(longerCount / (double) totalCount));
                isoHierarchy.add(tmpDataInfo + "\tB\t" + "shorter" + "\t" + String.valueOf(shorterCount / (double) totalCount));

                for (Map.Entry<String, Integer> entry
                        : isomirCounting.entrySet()) {
                    String key = entry.getKey();
                    Integer value = entry.getValue();
                    isoHierarchy.add(tmpDataInfo + "\tC\t" + key + "\t" + String.valueOf(value / (double) totalCount));

                }

            } catch (IOException ex) {
                logger.error("error reading top expressed miRNAs list file <" + idatasetFilename + ">\n" + ex.toString());
                throw new IOException("error reading top expressed miRNAs list file <" + idatasetFilename + ">");
            }  // END parsing isomiR report file

            underThresholdArray.add(datasetName + "\t" + String.valueOf(sumUnderThreshold));

            /*
                append text to files
             */
            String sampleName = datasetName;
            // prepare length variation for output
            for (Map.Entry<String, HashMap> ientryHashMap
                    : iLengthVarHashMap.entrySet()) {
                String mirnaKey = ientryHashMap.getKey();
                HashMap<String, Integer> mirnaHash = ientryHashMap.getValue();
                double iReadCountSum = 0.0;
                for (Integer f : mirnaHash.values()) {
                    iReadCountSum += (double) f;
                }
                for (Map.Entry<String, Integer> jentry
                        : mirnaHash.entrySet()) {
                    String typeKey = jentry.getKey();
                    Integer jreadCount = jentry.getValue();
                    if (iReadCountSum != 0) {
                        String tmpStr = sampleName + "\t" + mirnaKey + "\t" + typeKey + "\t" + String.valueOf(jreadCount / iReadCountSum);
                        iLengthVarArrayList.add(tmpStr);
                    }
                }
            }

            for (Map.Entry<String, Integer> entry
                    : iIsomiRDistHashMap.entrySet()) {
                String isomirStr = entry.getKey();;
                String readcountsum = String.valueOf(entry.getValue());
                String tmpStr = sampleName + "\t" + isomirStr + "\t" + readcountsum;
                isomirDistbutionArrayList.add(tmpStr);
            }

            for (Map.Entry<String, HashMap> entry
                    : isomirCompHashMap.entrySet()) {
                String mirnaKey = entry.getKey();
                HashMap<String, Integer> tmphashMap = entry.getValue();
                for (Map.Entry<String, Integer> tmpentry
                        : tmphashMap.entrySet()) {
                    String isomirKey = tmpentry.getKey();
                    Integer isomirReadcount = tmpentry.getValue();
                    String tmpString = sampleName + "\t" + (sampleName + "." + mirnaKey) + "\t" + mirnaKey + "\t" + isomirKey + "\t" + String.valueOf(isomirReadcount);
                    isomirCompArrayList.add(tmpString);
                }
            }

            for (Map.Entry<String, Integer> entry
                    : countSingleInMir.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();
                countSingleTable.add(datasetName + "\t" + key + "\t" + String.valueOf(value));
            }

            int mappedsum = Integer.valueOf(mappingCountSummary.get(datasetName).split("\t")[2]);
            String tmpnewmap = String.valueOf(reliableTotal)
                    + "\t" + String.valueOf(ambiguousTotal)
                    + "\t" + String.valueOf(mappedsum - reliableTotal - ambiguousTotal);

            updatedMappingSummary.add(mappingCountSummary.get(datasetName)
                    + "\t" + tmpnewmap);

            String idataCounting = datasetName;
            for (Map.Entry<String, Integer> entry
                    : idatasetCoitingIsomiR.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();
                idataCounting = idataCounting + "\t" + String.valueOf(value);
            }
            isomirCountArray.add(idataCounting);

            isomiRdatasetHashMap.put(datasetName, idatasetCoitingIsomiR);

            int totalMappedCount = Integer.valueOf(mappingCountSummary.get(datasetName).split("\t")[1]);
            String tmpStringtopExpressedVariabilty = datasetName
                    + "\t" + String.valueOf(topReliableCount)
                    + "\t" + String.valueOf(topAmbiguousCount)
                    + "\t" + String.valueOf(nonTopCount)
                    + "\t" + String.valueOf(topReliableCount / (double) totalMappedCount)
                    + "\t" + String.valueOf(topAmbiguousCount / (double) totalMappedCount)
                    + "\t" + String.valueOf(nonTopCount / (double) totalMappedCount);

            for (Map.Entry<String, Integer> entry
                    : countingIsomiRHashMap.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();
                String tmpStringV2 = sampleName + "\t" + key + "\t" + String.valueOf(value);
                isomirProporationArray.add(tmpStringV2);
            }

            misoUtility.appendTextArray2File(TEMPLATEDStrArray, (OUTPUTFOLDER + projectID + ".isomiR.templated.tsv"));
            misoUtility.appendTextArray2File(polyEventArrayList, (OUTPUTFOLDER + projectID + ".isomiR.polyEventDistribution.tsv"));
            misoUtility.appendTextArray2File(isomirDistbutionArrayList, (OUTPUTFOLDER + projectID + ".isomiR.distribution.tsv"));
            misoUtility.appendTextArray2File(isomirCompArrayList, (OUTPUTFOLDER + projectID + ".isomiR.composition.tsv"));
            misoUtility.appendTextArray2File(iLengthVarArrayList, (OUTPUTFOLDER + projectID + ".isomiR.lengthVariation.tsv"));
            misoUtility.appendTextArray2File(extensionALLArray, (OUTPUTFOLDER + projectID + ".isomiR.templated.mixing.tsv"));
            misoUtility.appendTextArray2File(singleModIsoArrayList, (OUTPUTFOLDER + projectID + ".isomiR.singleForm.tsv"));
            misoUtility.appendTextArray2File(countSingleTable, (OUTPUTFOLDER + projectID + ".countSingleForm.tsv"));
            misoUtility.appendTextArray2File(isomirProporationArray, (OUTPUTFOLDER + projectID + ".isomiR.proportion.tsv"));

        }  // END iterate datasets

        ArrayList<String> isomiRdatasetStr = new ArrayList<>();
        String headlineStr = "name";
        for (int i = 0; i < DATASETARRAYLIST.size(); i++) {
            headlineStr = headlineStr + "\t" + DATASETARRAYLIST.get(i).replace(RAW_INPUT_EXTENSION, "");
        }

        isomiRdatasetStr.add(headlineStr);
        for (String iisomiR : targetisomiArrayList) {
            String tmpisomir = iisomiR;
            for (int j = 0; j < DATASETARRAYLIST.size(); j++) {
                String jdataset = DATASETARRAYLIST.get(j);
                HashMap<String, Integer> countIndataset = isomiRdatasetHashMap.get(jdataset.replace(RAW_INPUT_EXTENSION, ""));
                tmpisomir = tmpisomir + "\t" + String.valueOf(countIndataset.get(iisomiR));
            }
            isomiRdatasetStr.add(tmpisomir);
        }

        ArrayList<String> onlyMatureCountTable = new ArrayList<>();
        for (String jstString : isomiRdatasetStr) {
            if (jstString.contains("name")) {
                onlyMatureCountTable.add(jstString);
            }
            if (jstString.contains("mature")) {
                onlyMatureCountTable.add(jstString);
            }

        }
        misoUtility.writeStrArrayList(OUTPUTFOLDER, projectID, isoHierarchy, ".isomiR.hierarchy.tsv");
        misoUtility.writeStrArrayList(OUTPUTFOLDER, projectID, isomiRdatasetStr, ".isomiR.countTable.tsv");
        misoUtility.writeStrArrayList(OUTPUTFOLDER, projectID, updatedMappingSummary, ".mappingSummary.tsv");
//        misoUtility.appendTextArray2File(updatedMappingSummary, (OUTPUTFOLDER + projectID + ".mappingSummary.tsv"));
    }

    /**
     * checking templated modification on isomiRs only checking lr-3e and lr-5e,
     * not mixture with polymorphism
     *
     * @param splitedLineStr
     */
    private void checkingTemplated(String[] splitedLineStr) {
        String isoType = splitedLineStr[5];
        String mirnsStr = splitedLineStr[2];
        String idataset = splitedLineStr[0].replace(RAW_INPUT_EXTENSION, "");
        if (!mirnsStr.contains("*")) {
            if (isoType.equals("lr-3e") || isoType.equals("lr-5e")) {
                int eventCount = splitedLineStr[6].length() - splitedLineStr[6].replace(":", "").length();
                String[] isoEvents = splitedLineStr[6].split(",");
                for (String ievent : isoEvents) {
                    if (ievent != null) {
                        TEMPLATEDStrArray.add(idataset + "\t" + mirnsStr + "\t" + isoType + "\t" + String.valueOf(eventCount) + "\t" + ievent.split(":")[1] + "\t" + splitedLineStr[4]);
                    }
                }
            }
        }
    }

    private void lengthVariation(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isoformType = lineStrArray[5];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            // only look at reliable miRNA
//            if(TOP100EXPRESSED.contains(mirnaStr)){
            // only look top expressed miRNAs
            String tmpKey = "none";
            if (isoformType.equals("mature")) {
                tmpKey = "same";
            } else if (isoformType.equals("poly")) {
                tmpKey = "same";
            } else if (isoformType.startsWith("sl")) {
                tmpKey = "same";
            } else if (isoformType.startsWith("sr")) {
                tmpKey = "shorter";
            } else if (isoformType.startsWith("lr")) {
                tmpKey = "longer";
            }
            if (iLengthVarHashMap.containsKey(mirnaStr)) {
                HashMap<String, Integer> tmpLenHash = iLengthVarHashMap.get(mirnaStr);
                tmpLenHash.replace(tmpKey, (tmpLenHash.get(tmpKey) + Integer.valueOf(readcount)));
                iLengthVarHashMap.replace(mirnaStr, tmpLenHash);
            }
//            }
        }
    }

    public void setFilter(boolean bfilter) {
        this.DOFILTER = bfilter;
    }

    private void checkPolyPosition(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isoformType = lineStrArray[5];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            if (isoformType.equals("poly")) {
                String isoEventStr = (lineString.split("\t")[6]);
                int polyCount = isoEventStr.length() - isoEventStr.replace(":", "").length();
                for (String ievent : isoEventStr.split(";")) {
                    if (ievent != null) {
                        String polyPosStr = ievent.split("p")[0];
//                        String tmpPolyPosStr = sampleName +"\t"+  mirnaStr +"\t"+ mirnaLenHash.get(mirnaStr) +"\t"+ polyPosStr +"\t"+ polyCount +"\t"+ readcount;
//                        polyPosArrayList.add(tmpPolyPosStr);
                        String tmpPolyEventStr = sampleName + "\t" + mirnaStr + "\t" + mirnaLenHash.get(mirnaStr) + "\t" + polyPosStr + "\t" + polyCount + "\t" + readcount + "\t" + ievent.split(":")[1];
                        polyEventArrayList.add(tmpPolyEventStr);

                    }
                }
            }
        }
    }

    private ArrayList<String> getDatasetList(String dataLayoutFilename) throws IOException {
        ArrayList datasetArrayList = new ArrayList();
        try {
            logger.info("reading data layout file <" + dataLayoutFilename + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(dataLayoutFilename)));
            String lineFA = null;
            while ((lineFA = brFA.readLine()) != null) {
                if (lineFA.startsWith("File")) {
                    continue;
                }
                String datasetString = lineFA.trim().split("\t")[0];
                datasetArrayList.add(datasetString);
            }
            brFA.close();
            logger.info("read " + datasetArrayList.size() + " entries");
            logger.info("done");
        } catch (IOException ex) {
            logger.error("error reading data layout file <" + dataLayoutFilename + ">\n" + ex.toString());
            throw new IOException("error reading data layout file <" + dataLayoutFilename + ">");
        }
        return datasetArrayList;
    }

    private String cleanPath(String pathstring) {
        return pathstring.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
    }

    
    private HashMap<String, String> readMappingSummary(String mappingSummaryFile) throws IOException {
        HashMap<String, String> summaryHashMap = new HashMap();
        try {
            logger.info("reading mapping summary file <" + mappingSummaryFile + ">");
            String summamryLine = null;
            BufferedReader summaryBR = new BufferedReader(new FileReader(new File(mappingSummaryFile)));
            while ((summamryLine = summaryBR.readLine()) != null) {
                if (summamryLine.startsWith("dataset")) {
                    continue;
                }
                String datasetname = summamryLine.split("\t")[0];
//                String mappedReadCounts = summamryLine.split("\t")[2];
//                int thresholdMapping = Integer.valueOf(mappedReadCounts) /100000;
                summaryHashMap.put(datasetname, summamryLine.trim());
            }
            summaryBR.close();
            logger.info("read " + summaryHashMap.size() + " entries");
            logger.info("done");
        } catch (IOException ex) {
            logger.error("error reading mapping summary file <" + mappingSummaryFile + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" + mappingSummaryFile + ">");
        }
        return summaryHashMap;
    }

    /**
     * this is to check the grouping approach in our nomenclature is reliable
     *
     * @param lineString
     */
    private void groupingVariability(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isoformType = lineStrArray[5];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        String modificationEvent = lineStrArray[6];

        ArrayList<String> miRNAsStrs = new ArrayList<>(Arrays.asList("hsa-miR-22-5p",
                "hsa-miR-22-3p", "hsa-miR-21-5p", "hsa-miR-21-3p", "hsa-miR-99b-5p",
                "hsa-miR-99b-3p", "hsa-miR-433-5p", "hsa-miR-433-3p"));
        if (!mirnaStr.contains("*")) {
            if (isoformType.equals("sr-3d")) {
                if (iGroupVariabiltyHash.containsKey(mirnaStr)) {
                    int eventCountIndex = modificationEvent.length() - modificationEvent.replaceAll(":", "").length() - 1;
                    Integer[] tmpIntArr = iGroupVariabiltyHash.get(mirnaStr);
                    tmpIntArr[eventCountIndex] = tmpIntArr[eventCountIndex] + Integer.valueOf(readcount);
                    iGroupVariabiltyHash.replace(mirnaStr, tmpIntArr);
                }
            }
        }
    }

    /**
     * this is to get pooled and only mature read count of every reliable miRNA
     *
     * @param lineString
     */
    private void readCountMature(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isoformType = lineStrArray[5];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];

        if (!mirnaStr.contains("*")) {
            if (isoformType.equals("mature")) {
                String tmpStr = sampleName + "\t" + mirnaStr + "\t" + "mature" + "\t" + readcount;
                readCountArrayList.add(tmpStr);
            }
            if (iPooledCountHashMap.containsKey(mirnaStr)) {
                iPooledCountHashMap.replace(mirnaStr, (Integer.valueOf(readcount) + iPooledCountHashMap.get(mirnaStr)));
            } else {
                iPooledCountHashMap.put(mirnaStr, Integer.valueOf(readcount));
            }
        }
    }

    private void isomiRdistribution(String lineString) {

        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
//        String cohortCode = tmpCohort.substring(0, (tmpCohort.length()-2));
//        String cellType = sampleName.split("-")[2];
        String isoformType = lineStrArray[5];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            iIsomiRDistHashMap.replace(isoformType, ((int) iIsomiRDistHashMap.get(isoformType) + Integer.valueOf(readcount)));
        }
    }

    private void isomirComposition(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isoformType = lineStrArray[5];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            if (isomirCompHashMap.containsKey(mirnaStr)) {
                HashMap<String, Integer> tmphash = isomirCompHashMap.get(mirnaStr);
                tmphash.replace(isoformType, (tmphash.get(isoformType) + Integer.valueOf(readcount)));
                isomirCompHashMap.replace(mirnaStr, tmphash);
            }
//            else{
//                
//            
//            }
        }
    }

    private void summaryEvent(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isomirName = lineStrArray[3];
        String isoformType = lineStrArray[5];
        String isoformEvent = lineStrArray[6];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            eventSummaryArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + isoformEvent + "\t" + readcount);
        }
    }

    private void extensionCheck(String lineString) {

        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isomirName = lineStrArray[3];
        String isoformType = lineStrArray[5];
        String isoformEvent = lineStrArray[6];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            if (isoformType.equals("lr-3e")) {
                if (isoformEvent.contains(",")) {
                    String[] firstEventArray = ((isoformEvent.split(",")[0]).split(":")[1]).split(">");
                    String[] secondEventArray = ((isoformEvent.split(",")[1]).split(":")[1]).split(">");

                    if (firstEventArray[0].equals(firstEventArray[1])) {
                        if (secondEventArray[0].equals(secondEventArray[1])) {
                            extensionALLArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + "TT" + "\t" + readcount);
                        } else {
                            extensionALLArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + "TN" + "\t" + readcount);
                        }
                    } else {
                        if (secondEventArray[0].equals(secondEventArray[1])) {
                            extensionALLArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + "NT" + "\t" + readcount);
                        } else {
                            extensionALLArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + "NN" + "\t" + readcount);
                        }
                    }
                } else {
                    String ntevent = isoformEvent.split(":")[1];
                    String[] nteventArray = ntevent.split(">");
                    if (nteventArray[0].equals(nteventArray[1])) {
                        extensionALLArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + "T" + "\t" + readcount);
                    } else {
                        extensionALLArray.add(sampleName + "\t" + isomirName + "\t" + mirnaStr + "\t" + isoformType + "\t" + "N" + "\t" + readcount);
                    }
                }
            }
        }
    }

    private void singleModificationIsoform(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isomirName = lineStrArray[3];
        String isoformType = lineStrArray[5];
        String isoformEvent = lineStrArray[6];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];
        if (!mirnaStr.contains("*")) {
            singleModIsoArrayList.add(sampleName + "\t" + isomirName + "#" + isoformEvent + "\t" + readcount);
        }
    }

    private void howManySingleInMir(String lineString) {
        String[] lineStrArray = lineString.split("\t");
        String sampleName = lineStrArray[0];
        String isomirName = lineStrArray[3];
        String isoformType = lineStrArray[5];
        String isoformEvent = lineStrArray[6];
        String mirnaStr = lineStrArray[2];
        String readcount = lineStrArray[4];

        if (countSingleInMir.containsKey(mirnaStr)) {
            countSingleInMir.replace(mirnaStr, (countSingleInMir.get(mirnaStr) + 1));
        } else {
            countSingleInMir.put(mirnaStr, 1);
        }
    }

    private ArrayList<String> readOnlyMature(String filenameStr) throws IOException {
        ArrayList<String> matureNameArray = new ArrayList<>();
        try {
            logger.info("read reference mature miRNA name as Array  <" + filenameStr + ">");
            String profileLine = null;
            BufferedReader profileBR = new BufferedReader(new FileReader(new File(filenameStr)));
            while ((profileLine = profileBR.readLine()) != null) {
                /*
                example line:
                hsa-mir-6858	hsa-miR-6858-3p:22:41:63;hsa-miR-6858-5p:22:0:22
                hsa-mir-5091	hsa-miR-5091:23:10:33
                 */
                String matureClusters = profileLine.split("\t")[1].trim();
                if (matureClusters.contains(";")) {
                    for (String imature : matureClusters.split(";")) {
                        matureNameArray.add(imature.split(":")[0]);
                    }
                } else {
                    matureNameArray.add(matureClusters.split(":")[0]);
                }
            }
            profileBR.close();
            logger.info("read " + matureNameArray.size() + " entries");
            logger.info("done");
        } catch (IOException ex) {
            logger.error("error in reading reference mature miRNA name as Array <" + filenameStr + ">\n" + ex.toString());
            throw new IOException("error in reading reference mature miRNA name as Array <" + filenameStr + ">");
        }
        return matureNameArray;
    }

    public HashMap<String, String> updateConfig() {
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        CONFIG_HASH.replace("DATASET_ARRAY_STR", StringUtils.join(datasetArray, "#"));
        return CONFIG_HASH;
    }

    private void startPostAnalysis_V2() throws IOException {
        misoUtility = new JASMINEutility();

        Boolean fA = new File(OUTPUTFOLDER).mkdir(); // output folder
        if (fA) {
            logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
        }
        Boolean fA_unique = new File(OUTPUT_FOLDER_unique).mkdir(); // sub output folder for unique isomiRs
        if (fA_unique) {
            logger.info("created output folder <" + OUTPUT_FOLDER_unique + "> for results");
        }
        Boolean fA_amgiguous = new File(OUTPUT_FOLDER_amgiguous).mkdir(); // sub output folder for amgiguous isomiRs
        if (fA_amgiguous) {
            logger.info("created output folder <" + OUTPUT_FOLDER_amgiguous + "> for results");
        }

        this.DATASETARRAYLIST = getDatasetList(datasetlstFile);

        ArrayList<String> level_0_unique = new ArrayList<>();
        ArrayList<String> level_0_ambiguous = new ArrayList<>();
        String[] header0Strs = new String[]{"sample", "miRNA", "count"};
        level_0_ambiguous.add(StringUtils.join(header0Strs, "\t"));
        level_0_unique.add(StringUtils.join(header0Strs, "\t"));

        ArrayList<String> level_1_unique = new ArrayList<>();
        ArrayList<String> level_1_ambiguous = new ArrayList<>();
        String[] header1Strs = new String[]{"sample", "miRNA", "matureCount", "isomiRcount", "rowsum"};
        level_1_ambiguous.add(StringUtils.join(header1Strs, "\t"));
        level_1_unique.add(StringUtils.join(header1Strs, "\t"));

        ArrayList<String> level_2_unique = new ArrayList<>();
        ArrayList<String> level_2_ambiguous = new ArrayList<>();
        String[] header2Strs = new String[]{"sample", "miRNA", "matureCount", "sameLengthCount", "shorterCount", "longerCount", "rowsum"};
        level_2_ambiguous.add(StringUtils.join(header2Strs, "\t"));
        level_2_unique.add(StringUtils.join(header2Strs, "\t"));

        ArrayList<String> level_3_unique = new ArrayList<>();
        ArrayList<String> level_3_ambiguous = new ArrayList<>();
        String header3Strs = "sample\tmiRNA" + "\t" + StringUtils.join(ISOFORMTYPE, "\t") + "\t" + "rowsum";
        level_3_ambiguous.add(header3Strs);
        level_3_unique.add(header3Strs);

        ArrayList<String> level_4_unique = new ArrayList<>();
        ArrayList<String> level_4_ambiguous = new ArrayList<>();
        String[] header4Strs = new String[]{"sample", "miRNA", "isomiR.group", "isomiR.info", "change.SeedRegion", "matchedSNP", "count"};
        level_4_ambiguous.add(StringUtils.join(header4Strs, "\t"));
        level_4_unique.add(StringUtils.join(header4Strs, "\t"));

        ArrayList<String> level_5_unique = new ArrayList<>();
        ArrayList<String> level_5_ambiguous = new ArrayList<>();
        String[] header5Strs = new String[]{"sample", "miRNA", "mature", "5.isomiR", "3.isomiR", "sub.isomiR", "mix.isomiR", "rowsum"};
        level_5_ambiguous.add(StringUtils.join(header5Strs, "\t"));
        level_5_unique.add(StringUtils.join(header5Strs, "\t"));

        ArrayList<String> precursorArmUsageArray = new ArrayList<>();
        String[] headerArmStrs = new String[]{"sample", "precursor", "mature5pCount", "all5pCount", "mature3pCount", "all3pCount", "mature5p", "all5p", "mature3p", "all3p"};
        precursorArmUsageArray.add(StringUtils.join(headerArmStrs, "\t"));

        String mature2HairpinProfileFile = projectFolder + "Reference" + FILESEPARATOR + "miRBase" + "." + HOST + ".matureInHairpin.tsv";
        miR_armProfile_Hash = buildArmHashFromText(mature2HairpinProfileFile);

        ArrayList<String> armProfileArray = new ArrayList<>();
        armProfileArray.add("miRNA\tprecursor\tarm");

        ArrayList<String> polyProfileArray_unique = new ArrayList<>();
        polyProfileArray_unique.add("sample\tmiRNA\tcount\tsinglePoly\tposition\tpolyEvent");
        ArrayList<String> polyProfileArray_ambiguous = new ArrayList<>();
        polyProfileArray_ambiguous.add("sample\tmiRNA\tcount\tsinglePoly\tposition\tpolyEvent");
        
        ArrayList<String> extensionProfileArray_unique = new ArrayList<>();
//        extensionProfileArray_unique.add("sample\tmiRNA\tcount\tsinglePoly\tposition\tpolyEvent");
        ArrayList<String> extensionProfileArray_ambiguous = new ArrayList<>();
//        extensionProfileArray_ambiguous.add("sample\tmiRNA\tcount\tsinglePoly\tposition\tpolyEvent");
        
        
        /*
        wtite miR_armProfile_Hash into local
//        */
        for (Map.Entry<String, String> entry
                : miR_armProfile_Hash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            armProfileArray.add(key + "\t" + value.split("@")[1] + "\t" + value.split("@")[0]);
        }
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, armProfileArray, ".armProfile.tsv");

        for (String idataset : DATASETARRAYLIST) {
            idataset = idataset.replace(RAW_INPUT_EXTENSION, "");
            logger.info(idataset);
            isomiR_lev_0_unique_hash = new HashMap<>();
            isomiR_lev_0_amgiguous_hash = new HashMap<>();

            isomiR_lev_1_unique_hash = new HashMap<>();
            isomiR_lev_1_amgiguous_hash = new HashMap<>();

            isomiR_lev_2_unique_hash = new HashMap<>();
            isomiR_lev_2_amgiguous_hash = new HashMap<>();

            isomiR_lev_3_unique_hash = new HashMap<>();
            isomiR_lev_3_amgiguous_hash = new HashMap<>();

            isomiR_lev_4_unique_hash = new HashMap<>();
            isomiR_lev_4_amgiguous_hash = new HashMap<>();

            isomiR_lev_5_unique_hash = new HashMap<>();
            isomiR_lev_5_amgiguous_hash = new HashMap<>();

            isomiR_armCounting_hash = new HashMap<>();
            isomiR_polyEvent_Array = new ArrayList<>();
            isomiR_extension_Array = new ArrayList<>();

            String idatasetFilename = projectFolder + "IsomiRsReports" + FILESEPARATOR + idataset + ".isomiRs.report.tsv";
            try {
                logger.info("reading isomiR report file <" + idatasetFilename + ">");
                BufferedReader reportBR = new BufferedReader(new FileReader(new File(idatasetFilename)));
                String reportLine = null;
                while ((reportLine = reportBR.readLine()) != null) {
                    String[] lineSplitted = reportLine.split("\t");
                    boolean isMarked = false;
                    if (reportLine.contains("*")) {
                        isMarked = true;
                    }
                    build_isomiR_Level_0_table(isMarked, lineSplitted); // only perfect matched miRNAs
                    build_isomiR_Level_1_table(isMarked, lineSplitted); // build count table summarize mature and isoform
                    build_isomiR_Level_2_table(isMarked, lineSplitted); // build count table summarize mature and isoform in different length
                    build_isomiR_Level_3_table(isMarked, lineSplitted); // build count table all possibile isomiR groups
                    build_isomiR_Level_4_table(isMarked, lineSplitted); // build count table on single reads
                    build_isomiR_Level_5_table(isMarked, lineSplitted);  // additional and new-added, group isomiRs based termin
                    build_armUsage(isMarked, lineSplitted); // precursor arm usage
                    build_poly(isMarked, lineSplitted); // profiling polymorphis
                    build_additionProfile(isMarked, lineSplitted); // profiling addition at 3' end is templated ot not
                }
            } catch (IOException ex) {
                logger.error("error reading top expressed miRNAs list file <" + idatasetFilename + ">\n" + ex.toString());
                throw new IOException("error reading top expressed miRNAs list file <" + idatasetFilename + ">");
            }

            /*
            summarize level 0 into String Array
             */
            for (Map.Entry<String, Integer> entry
                    : isomiR_lev_0_unique_hash.entrySet()) {
                String key = entry.getKey();
                String valueStr = String.valueOf(entry.getValue());
                level_0_unique.add(idataset + "\t" + key + "\t" + valueStr);
            }
            for (Map.Entry<String, Integer> entry
                    : isomiR_lev_0_amgiguous_hash.entrySet()) {
                String key = entry.getKey();
                String valueStr = String.valueOf(entry.getValue());
                level_0_ambiguous.add(idataset + "\t" + key + "\t" + valueStr);
            }

            /*
            summarize level 1 into String Array
             */
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_1_unique_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_1_unique.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_1_amgiguous_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_1_ambiguous.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }

            /*
            summarize level 2 into String Array
             */
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_2_unique_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_2_unique.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_2_amgiguous_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_2_ambiguous.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }

            /*
            summarize level 3 into String Array
             */
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_3_unique_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_3_unique.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_3_amgiguous_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_3_ambiguous.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }

            /*
            summarize level 4 into String Array
             */
            for (Map.Entry<String, Integer> entry
                    : isomiR_lev_4_unique_hash.entrySet()) {
                String key = entry.getKey();
                String valueStr = String.valueOf(entry.getValue());
//                String[] valueTransfed = intArray2StrArray(value);
                level_4_unique.add(idataset + "\t" + key.replace("_", "\t") + "\t" + valueStr);
            }
            for (Map.Entry<String, Integer> entry
                    : isomiR_lev_4_amgiguous_hash.entrySet()) {
                String key = entry.getKey();
                String valueStr = String.valueOf(entry.getValue());
//                String[] valueTransfed = intArray2StrArray(value);
                level_4_ambiguous.add(idataset + "\t" + key.replace("_", "\t") + "\t" + valueStr);
            }

            /*
            summarize arm usage into String Array
             */
            for (Map.Entry<String, int[]> entry
                    : isomiR_armCounting_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                double[] armPercentage = armPercentaging(value);

                String[] valueTransfed = doubleArray2StrArray(armPercentage);
                precursorArmUsageArray.add(idataset + "\t" + key + "\t" + StringUtils.join(intArray2StrArray(value), "\t") + "\t" + StringUtils.join(valueTransfed, "\t"));
            }

            /*
            summarize level 5 into String Array
             */
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_5_unique_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_5_unique.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }
            for (Map.Entry<String, int[]> entry
                    : isomiR_lev_5_amgiguous_hash.entrySet()) {
                String key = entry.getKey();
                int[] value = entry.getValue();
                String[] valueTransfed = intArray2StrArray(value);
                level_5_ambiguous.add(idataset + "\t" + key + "\t" + StringUtils.join(valueTransfed, "\t") + "\t" + String.valueOf(IntStream.of(value).sum()));
            }

            /*
            summarize poly event into local file
             */
            for (String ipolyLine : isomiR_polyEvent_Array) {
                if (ipolyLine.contains("*")) {
                    polyProfileArray_ambiguous.add(idataset + "\t" + ipolyLine);
                } else {
                    polyProfileArray_unique.add(idataset + "\t" + ipolyLine);
                }
            }
            
            /*
            summary extension
            */
            for(String iExtensionStr : isomiR_extension_Array){
                if (iExtensionStr.contains("*")) {
                    extensionProfileArray_ambiguous.add(idataset + "\t" + iExtensionStr);
                }else{
                    extensionProfileArray_unique.add(idataset + "\t" + iExtensionStr);
                }
            }

        }
        /*
        OUTPUT to local files
         */
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, level_0_unique, ".isomiR.Level-0.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, level_0_ambiguous, ".isomiR.Level-0.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, level_1_unique, ".isomiR.Level-1.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, level_1_ambiguous, ".isomiR.Level-1.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, level_2_unique, ".isomiR.Level-2.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, level_2_ambiguous, ".isomiR.Level-2.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, level_3_unique, ".isomiR.Level-3.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, level_3_ambiguous, ".isomiR.Level-3.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, level_4_unique, ".isomiR.Level-4.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, level_4_ambiguous, ".isomiR.Level-4.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, level_5_unique, ".isomiR.Level-5.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, level_5_ambiguous, ".isomiR.Level-5.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, precursorArmUsageArray, ".precursor_armUsage.tsv");

        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, polyProfileArray_unique, ".polyEvent.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, polyProfileArray_ambiguous, ".polyEvent.tsv");
        
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_unique, projectID, extensionProfileArray_unique, ".extenEvent.tsv");
        misoUtility.writeStrArrayList(OUTPUT_FOLDER_amgiguous, projectID, extensionProfileArray_ambiguous, ".extenEvent.tsv");

    }

    private void build_isomiR_Level_0_table(boolean ismarked, String[] lineSplitted) {
        /*
        only count end-to-end perfect matched mature miRNAs
         */
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        if (typeStr.equals("mature")) {
            // mature 
            if (!ismarked) {
                // unique
                if (isomiR_lev_0_unique_hash.containsKey(mirNameStr)) {
                    int newCount = isomiR_lev_0_unique_hash.get(mirNameStr);
                    newCount += iReadcount;
                    isomiR_lev_0_unique_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_0_unique_hash.put(mirNameStr, iReadcount);
                }
            } else {
                // ambiguous
                if (isomiR_lev_0_amgiguous_hash.containsKey(mirNameStr)) {
                    int newCount = isomiR_lev_0_amgiguous_hash.get(mirNameStr);
                    newCount += iReadcount;
                    isomiR_lev_0_amgiguous_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_0_amgiguous_hash.put(mirNameStr, iReadcount);
                }
            }
        }

    }

    private void build_isomiR_Level_1_table(boolean ismarked, String[] lineSplitted) {
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        if (typeStr.equals("mature")) {
            // mature 
            if (!ismarked) {
                // unique
                if (isomiR_lev_1_unique_hash.containsKey(mirNameStr)) {
                    int[] newCount = isomiR_lev_1_unique_hash.get(mirNameStr);
                    newCount[0] = newCount[0] + iReadcount;
                    isomiR_lev_1_unique_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_1_unique_hash.put(mirNameStr, new int[]{iReadcount, 0});
                }
            } else {
                // ambiguous
                if (isomiR_lev_1_amgiguous_hash.containsKey(mirNameStr)) {
                    int[] newCount = isomiR_lev_1_amgiguous_hash.get(mirNameStr);
                    newCount[0] = newCount[0] + iReadcount;
                    isomiR_lev_1_amgiguous_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_1_amgiguous_hash.put(mirNameStr, new int[]{iReadcount, 0});
                }
            }
        } else {
            // isoform
            if (!ismarked) {
                // unique
                if (isomiR_lev_1_unique_hash.containsKey(mirNameStr)) {
                    int[] newCount = isomiR_lev_1_unique_hash.get(mirNameStr);
                    newCount[1] = newCount[1] + iReadcount;
                    isomiR_lev_1_unique_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_1_unique_hash.put(mirNameStr, new int[]{0, iReadcount});
                }
            } else {
                // ambiguous
                if (isomiR_lev_1_amgiguous_hash.containsKey(mirNameStr)) {
                    int[] newCount = isomiR_lev_1_amgiguous_hash.get(mirNameStr);
                    newCount[1] = newCount[1] + iReadcount;
                    isomiR_lev_1_amgiguous_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_1_amgiguous_hash.put(mirNameStr, new int[]{0, iReadcount});
                }
            }
        }

    }

    private void build_isomiR_Level_2_table(boolean ismarked, String[] lineSplitted) {
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);

        if (typeStr.equals("mature")) {
            // mature 
            if (!ismarked) {
                // unique
                if (isomiR_lev_2_unique_hash.containsKey(mirNameStr)) {
                    int[] newCount = isomiR_lev_2_unique_hash.get(mirNameStr);
                    newCount[0] = newCount[0] + iReadcount;
                    isomiR_lev_2_unique_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_2_unique_hash.put(mirNameStr, new int[]{iReadcount, 0, 0, 0}); // ordr: mature, same length isomiRs, shorter isomiRs, longer isomiRs
                }
            } else {
                // ambiguous
                if (isomiR_lev_2_amgiguous_hash.containsKey(mirNameStr)) {
                    int[] newCount = isomiR_lev_2_amgiguous_hash.get(mirNameStr);
                    newCount[0] = newCount[0] + iReadcount;
                    isomiR_lev_2_amgiguous_hash.replace(mirNameStr, newCount);
                } else {
                    isomiR_lev_2_amgiguous_hash.put(mirNameStr, new int[]{iReadcount, 0, 0, 0}); // ordr: mature, same length isomiRs, shorter isomiRs, longer isomiRs
                }
            }
        } else {
            // isoform
            if (!ismarked) {
                // unique
                if (typeStr.equals("poly") | typeStr.startsWith("sl")) {
                    if (isomiR_lev_2_unique_hash.containsKey(mirNameStr)) {
                        int[] newCount = isomiR_lev_2_unique_hash.get(mirNameStr);
                        newCount[1] = newCount[1] + iReadcount;
                        isomiR_lev_2_unique_hash.replace(mirNameStr, newCount);
                    } else {
                        isomiR_lev_2_unique_hash.put(mirNameStr, new int[]{0, iReadcount, 0, 0});
                    }
                } else if (typeStr.startsWith("sr")) {
                    if (isomiR_lev_2_unique_hash.containsKey(mirNameStr)) {
                        int[] newCount = isomiR_lev_2_unique_hash.get(mirNameStr);
                        newCount[2] = newCount[2] + iReadcount;
                        isomiR_lev_2_unique_hash.replace(mirNameStr, newCount);
                    } else {
                        isomiR_lev_2_unique_hash.put(mirNameStr, new int[]{0, 0, iReadcount, 0});
                    }
                } else if (typeStr.startsWith("lr")) {
                    if (isomiR_lev_2_unique_hash.containsKey(mirNameStr)) {
                        int[] newCount = isomiR_lev_2_unique_hash.get(mirNameStr);
                        newCount[3] = newCount[3] + iReadcount;
                        isomiR_lev_2_unique_hash.replace(mirNameStr, newCount);
                    } else {
                        isomiR_lev_2_unique_hash.put(mirNameStr, new int[]{0, 0, 0, iReadcount});
                    }
                }
            } else {
                // ambiguous
                if (typeStr.equals("poly") | typeStr.startsWith("sl")) {
                    if (isomiR_lev_2_amgiguous_hash.containsKey(mirNameStr)) {
                        int[] newCount = isomiR_lev_2_amgiguous_hash.get(mirNameStr);
                        newCount[1] = newCount[1] + iReadcount;
                        isomiR_lev_2_amgiguous_hash.replace(mirNameStr, newCount);
                    } else {
                        isomiR_lev_2_amgiguous_hash.put(mirNameStr, new int[]{0, iReadcount, 0, 0});
                    }
                } else if (typeStr.startsWith("sr")) {
                    if (isomiR_lev_2_amgiguous_hash.containsKey(mirNameStr)) {
                        int[] newCount = isomiR_lev_2_amgiguous_hash.get(mirNameStr);
                        newCount[2] = newCount[2] + iReadcount;
                        isomiR_lev_2_amgiguous_hash.replace(mirNameStr, newCount);
                    } else {
                        isomiR_lev_2_amgiguous_hash.put(mirNameStr, new int[]{0, 0, iReadcount, 0});
                    }
                } else if (typeStr.startsWith("lr")) {
                    if (isomiR_lev_2_amgiguous_hash.containsKey(mirNameStr)) {
                        int[] newCount = isomiR_lev_2_amgiguous_hash.get(mirNameStr);
                        newCount[3] = newCount[3] + iReadcount;
                        isomiR_lev_2_amgiguous_hash.replace(mirNameStr, newCount);
                    } else {
                        isomiR_lev_2_amgiguous_hash.put(mirNameStr, new int[]{0, 0, 0, iReadcount});
                    }
                }
            }
        }
    }

    private String[] intArray2StrArray(int[] intvalue) {
        String[] strvalue = new String[intvalue.length];
        for (int i = 0; i < intvalue.length; i++) {
            strvalue[i] = String.valueOf(intvalue[i]);
        }
        return strvalue;
    }

    private void build_isomiR_Level_3_table(boolean ismarked, String[] lineSplitted) {
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);

        int typeIndex = Arrays.asList(ISOFORMTYPE).indexOf(typeStr);
        // mature 
        if (!ismarked) {
            // unique
            if (isomiR_lev_3_unique_hash.containsKey(mirNameStr)) {
                int[] newCount = isomiR_lev_3_unique_hash.get(mirNameStr);
                newCount[typeIndex] = newCount[typeIndex] + iReadcount;
                isomiR_lev_3_unique_hash.replace(mirNameStr, newCount);
            } else {
                int[] newCount = new int[ISOFORMTYPE.length];
                Arrays.fill(newCount, 0);
                newCount[typeIndex] = iReadcount;
                isomiR_lev_3_unique_hash.put(mirNameStr, newCount);
            }
        } else {
            // ambiguous
            if (isomiR_lev_3_amgiguous_hash.containsKey(mirNameStr)) {
                int[] newCount = isomiR_lev_3_amgiguous_hash.get(mirNameStr);
                newCount[typeIndex] = newCount[typeIndex] + iReadcount;
                isomiR_lev_3_amgiguous_hash.replace(mirNameStr, newCount);
            } else {
                int[] newCount = new int[ISOFORMTYPE.length];
                Arrays.fill(newCount, 0);
                newCount[typeIndex] = iReadcount;
                isomiR_lev_3_amgiguous_hash.put(mirNameStr, newCount);
            }
        }

    }

    private void build_isomiR_Level_4_table(boolean ismarked, String[] lineSplitted) {
        String mirNameStr = lineSplitted[1] + "_" + lineSplitted[4] + "_" + lineSplitted[5]
                + "_" + lineSplitted[6] + "_" + lineSplitted[7];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        if (!ismarked) {
            // unique
            if (isomiR_lev_4_unique_hash.containsKey(mirNameStr)) {
                int newCount = isomiR_lev_4_unique_hash.get(mirNameStr);
                newCount = newCount + iReadcount;
                isomiR_lev_4_unique_hash.replace(mirNameStr, newCount);
            } else {
                isomiR_lev_4_unique_hash.put(mirNameStr, iReadcount);
            }
        } else {
            // ambiguous
            if (isomiR_lev_4_amgiguous_hash.containsKey(mirNameStr)) {
                int newCount = isomiR_lev_4_amgiguous_hash.get(mirNameStr);
                newCount = newCount + iReadcount;
                isomiR_lev_4_amgiguous_hash.replace(mirNameStr, newCount);
            } else {
                isomiR_lev_4_amgiguous_hash.put(mirNameStr, iReadcount);
            }
        }

    }

    private void build_armUsage(boolean ismarked, String[] lineSplitted) {
        String mirNameStr = lineSplitted[1] + "_" + lineSplitted[4] + "_" + lineSplitted[5];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        int[] tmpCountArray = new int[]{0, 0, 0, 0};

        if (miR_armProfile_Hash.containsKey(lineSplitted[1])) {
            /*
            miR_armProfile_Hash only contain unambiguous arm profile
             */
            String queryArm = miR_armProfile_Hash.get(lineSplitted[1]).split("@")[0];
            String queryPrecursor = miR_armProfile_Hash.get(lineSplitted[1]).split("@")[1];

            if (isomiR_armCounting_hash.containsKey(queryPrecursor)) {
                /*
                updating hash
                 */
                if (typeStr.equals("mature")) {
                    // only mature miRNAs
                    if (queryArm.equals("5p")) {
                        tmpCountArray = isomiR_armCounting_hash.get(queryPrecursor);
                        tmpCountArray[0] += iReadcount;
                        isomiR_armCounting_hash.replace(queryPrecursor, tmpCountArray);
                    } else {
                        tmpCountArray = isomiR_armCounting_hash.get(queryPrecursor);
                        tmpCountArray[2] += iReadcount;
                        isomiR_armCounting_hash.replace(queryPrecursor, tmpCountArray);
                    }
                }
                // sum up mature and isomiRs
                if (queryArm.equals("5p")) {
                    tmpCountArray = isomiR_armCounting_hash.get(queryPrecursor);
                    tmpCountArray[1] += iReadcount;
                    isomiR_armCounting_hash.replace(queryPrecursor, tmpCountArray);
                } else {
                    tmpCountArray = isomiR_armCounting_hash.get(queryPrecursor);
                    tmpCountArray[3] += iReadcount;
                    isomiR_armCounting_hash.replace(queryPrecursor, tmpCountArray);
                }
            } else {
                /*
                adding the new pair of key and value
                 */
                if (typeStr.equals("mature")) {
                    // only mature miRNAs
                    if (queryArm.equals("5p")) {
                        tmpCountArray[0] = iReadcount;
                        isomiR_armCounting_hash.put(queryPrecursor, tmpCountArray);
                    } else {
                        tmpCountArray[2] = iReadcount;
                        isomiR_armCounting_hash.put(queryPrecursor, tmpCountArray);
                    }
                }
                // sum up mature and isomiRs
                if (queryArm.equals("5p")) {
                    tmpCountArray[1] = iReadcount;
                    isomiR_armCounting_hash.put(queryPrecursor, tmpCountArray);
                } else {
                    tmpCountArray[3] = iReadcount;
                    isomiR_armCounting_hash.put(queryPrecursor, tmpCountArray);
                }
            }
        }
    }

    private HashMap<String, String> buildArmHashFromText(String mature2HairpinProfileFile) {
        HashMap<String, String> armHash = new HashMap<>();
        try {
            String profileLine = null;
            BufferedReader profileBR = new BufferedReader(new FileReader(new File(mature2HairpinProfileFile)));
            while ((profileLine = profileBR.readLine()) != null) {
                if (!profileLine.contains("|")) {
                    if (profileLine.contains("-5p") & profileLine.contains("-3p")) {
                        String premirnaStr = profileLine.split("\t")[1];
                        String[] matureSplitted = premirnaStr.split(";");
                        int profiledChildMiRs = (matureSplitted).length;
                        if (profiledChildMiRs == 2) {
                            String precursorStr = profileLine.split("\t")[0];
                            for (String imature : matureSplitted) {
                                if (imature.contains("-5p")) {
                                    armHash.put(imature.split(":")[0], "5p@" + precursorStr);
                                } else if (imature.contains("-3p")) {
                                    armHash.put(imature.split(":")[0], "3p@" + precursorStr);
                                }
                            }
                        }
                    }
                }
            }
            profileBR.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
        return armHash;
    }

    private String[] doubleArray2StrArray(double[] doubleArray) {
        String[] strvalue = new String[doubleArray.length];
        for (int i = 0; i < doubleArray.length; i++) {
            strvalue[i] = String.valueOf(doubleArray[i]);
        }
        return strvalue;
    }

    private double[] armPercentaging(int[] valueIntArr) {
        double[] armPercentage = new double[valueIntArr.length];

        armPercentage[0] = giveCorrectPercentage(valueIntArr[0], (valueIntArr[0] + valueIntArr[2]));
        armPercentage[2] = giveCorrectPercentage(valueIntArr[2], (valueIntArr[0] + valueIntArr[2]));
        armPercentage[1] = giveCorrectPercentage(valueIntArr[1], (valueIntArr[1] + valueIntArr[3]));
        armPercentage[3] = giveCorrectPercentage(valueIntArr[3], (valueIntArr[1] + valueIntArr[3]));
        return armPercentage;
    }

    private double giveCorrectPercentage(int queryVal, int querySum) {
        double queryedPer = (double) 0;
        if (queryVal != 0 & querySum != 0) {
            queryedPer = (double) queryVal / querySum;
        }
        return queryedPer;
    }

    private void build_isomiR_Level_5_table(boolean ismarked, String[] lineSplitted) {
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        int[] newCount = new int[]{0, 0, 0, 0, 0};
        int typeIndex = assignTypeIndex(typeStr);

        if (!ismarked) {
            // unique
            if (isomiR_lev_5_unique_hash.containsKey(mirNameStr)) {
                newCount = isomiR_lev_5_unique_hash.get(mirNameStr);
                newCount[typeIndex] = newCount[typeIndex] + iReadcount;
                isomiR_lev_5_unique_hash.replace(mirNameStr, newCount);
            } else {
                newCount[typeIndex] = iReadcount;
                isomiR_lev_5_unique_hash.put(mirNameStr, newCount); // ordr: mature, same length isomiRs, shorter isomiRs, longer isomiRs
            }
        } else {
            // ambiguous
            if (isomiR_lev_5_amgiguous_hash.containsKey(mirNameStr)) {
                newCount = isomiR_lev_5_amgiguous_hash.get(mirNameStr);
                newCount[typeIndex] = newCount[typeIndex] + iReadcount;
                isomiR_lev_5_amgiguous_hash.replace(mirNameStr, newCount);
            } else {
                newCount[typeIndex] = iReadcount;
                isomiR_lev_5_amgiguous_hash.put(mirNameStr, newCount); // ordr: mature, same length isomiRs, shorter isomiRs, longer isomiRs
            }
        }

    }

    private int assignTypeIndex(String typeStr) {
        /*
        given a isomiR type, such as mature, poly, sr-3d
        assign an Integer in rule
        and return this number
        integer assigning rule:
            0 -- mature
            1 -- isomiRs with any modifications at 5' end, shorter or longer, deletion and extension
            2 -- isomiRs with any modifications at 3' end, shorter or longer, deletion and extension
            3 -- isomiRs with polyorphism, the poly type
            4 -- all other mixed isomiRs
         */

        int typeindex = -1; // initiate the isomiR type index

        if (typeStr.equals("mature")) {
            typeindex = 0;
        } else if (typeStr.equals("poly")) {
            typeindex = 3;
        } else if (typeStr.split("-").length == 2 & typeStr.contains("3")) {
            typeindex = 2;
        } else if (typeStr.split("-").length == 2 & typeStr.contains("5")) {
            typeindex = 1;
        } else {
            typeindex = 4;
        }

        return typeindex;
    }

    private void build_poly(boolean ismarked, String[] lineSplitted) {
        /*
        profiling  isomiR poly with polymorphism
        miRNA:poly
         */
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        String polyInfoStr = lineSplitted[5];

        if (typeStr.equals("poly")) {
            String polyPosStr = null;
            String polyEventStr = null;
            String singlePoly = "0";

            if (polyInfoStr.contains(";")) {
                singlePoly = "1";
                for (String ipoly : polyInfoStr.split(";")) {
                    polyPosStr = ipoly.split(":")[0].replace("p", "");
                    polyEventStr = ipoly.split(":")[1];
                    isomiR_polyEvent_Array.add(StringUtils.join((new String[]{mirNameStr, lineSplitted[3], singlePoly, polyPosStr, polyEventStr}), "\t"));
                }
            } else {
                polyPosStr = polyInfoStr.split(":")[0].replace("p", "");
                polyEventStr = polyInfoStr.split(":")[1];
                isomiR_polyEvent_Array.add(StringUtils.join((new String[]{mirNameStr, lineSplitted[3], singlePoly, polyPosStr, polyEventStr}), "\t"));
            }
        }
    }

    private void build_additionProfile(boolean ismarked, String[] lineSplitted) {
        /*
        profiling nucleotide addition profile in isomiR 
        templated or non-templated
        */
        String mirNameStr = lineSplitted[1];
        String typeStr = lineSplitted[4];
        int iReadcount = Integer.valueOf(lineSplitted[3]);
        String eventInfoStr = lineSplitted[5];
        
        int typelength = typeStr.split("-").length; // how many type of modifications
        int eventNumber = eventInfoStr.split(";").length; // how many modification event occured
        if (typeStr.contains("3e")) {
            if(typelength == 2){
                // single type, only 3e, no other modifications such as 5d/5e/p
                if (eventNumber > 1) {
                    // multiple nucleotide addition
                    for (String ievent : eventInfoStr.split(";")) {
                        String eventinfo = ievent.split(":")[1];
                        String iExType = "m"+ievent.split(":")[0].split("\\+")[1];
                        if (eventinfo.charAt(0) == eventinfo.charAt(2)) {
                            isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr, lineSplitted[3], "3e", iExType, "T", String.valueOf(eventinfo.charAt(2))}), "\t"));

                        } else {
                            isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr, lineSplitted[3], "3e", iExType, "nT", String.valueOf(eventinfo.charAt(2))}), "\t"));
                        }
                    }
                }
                else{
                    // single addition
                    String eventinfo = eventInfoStr.split(":")[1];
                    if(eventinfo.charAt(0) == eventinfo.charAt(2) ){
                        isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr,typeStr, lineSplitted[3], "3e", "s", "T", String.valueOf(eventinfo.charAt(2))}), "\t"));
                    
                    }else{
                        isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr, lineSplitted[3], "3e", "s","nT", String.valueOf(eventinfo.charAt(2))}), "\t"));
                    }
                }
            }
            
        } else if(typeStr.contains("5e")){
            if(typelength == 2){
                // single type, only 3e, no other modifications such as 5d/5e/p
                if (eventNumber > 1) {
                    // multiple nucleotide addition
                    for (String ievent : eventInfoStr.split(";")) {
                        String eventinfo = ievent.split(":")[1];
                        String iExType = "m"+ievent.split(":")[0].split("\\+")[1];
                        if (eventinfo.charAt(0) == eventinfo.charAt(2)) {
                            isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr, lineSplitted[3], "5e", iExType, "T", String.valueOf(eventinfo.charAt(2))}), "\t"));

                        } else {
                            isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr, lineSplitted[3], "5e", iExType, "nT", String.valueOf(eventinfo.charAt(2))}), "\t"));
                        }
                    }
                }
                else{
                    // single addition
                    String eventinfo = eventInfoStr.split(":")[1];
                    if(eventinfo.charAt(0) == eventinfo.charAt(2) ){
                        isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr, lineSplitted[3], "5e", "s", "T", String.valueOf(eventinfo.charAt(2))}), "\t"));
                    
                    } else{
                        isomiR_extension_Array.add(StringUtils.join((new String[]{mirNameStr, typeStr,  lineSplitted[3], "5e", "s", "nT", String.valueOf(eventinfo.charAt(2))}), "\t"));
                    }
                }
            }
        }
        
    }
}
