/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
class JisomiRPipeline {
    Logger logger = LogManager.getRootLogger();
    private HashMap<String, String>  CONFIG_HASH;
    private ArrayList<String> RUNSTEPS;
    
    private String path2MappingReference ;
    private String path2DatasetFile;
    private String path2FastqFolder ;
    private String folder4Output ;
    
    /**
     * start execute the pipeline
     */
    public void execute() throws IOException, FileNotFoundException, InterruptedException {
        for (String istep : RUNSTEPS) {
            logger.info("\n\n>>>>found step ["+ istep + "] ");
            switch (istep){
                case "DownloadSRA":
                    DownloadSRA downloadSRA = new DownloadSRA(CONFIG_HASH);
                    CONFIG_HASH = downloadSRA.updateConfig(CONFIG_HASH);
                    break;
                    
                case "CheckingReference":
                    CheckReference checkRef = new CheckReference(CONFIG_HASH);
//                    CONFIG_HASH = checkRef.updateConfig(CONFIG_HASH);
                    break;
                case "BuildMappingReference":
                    BuildMappingReference buildRef = new BuildMappingReference(CONFIG_HASH);
//                    CONFIG_HASH = buildRef.updateConfig(CONFIG_HASH);
                    break;
                    
                case "AdapterTrimming":
                    AdapterTrimming trimAdapter = new AdapterTrimming(CONFIG_HASH);
                    CONFIG_HASH = trimAdapter.updateConfig();
                    logger.info("---<<<<< "+CONFIG_HASH.get("INPUTFOLDER"));
                    break;
                
                case "Collapsing":
                    Collapsing collaper = new Collapsing(CONFIG_HASH);
                    CONFIG_HASH = collaper.updateConfig();
                    break;
                    
                case "MiRBaseHairpinMapping":
                    MiRBaseHairpinMapping hairpinMapping = new MiRBaseHairpinMapping(CONFIG_HASH);
                    CONFIG_HASH = hairpinMapping.updateConfig();
                    break;
                case "SAMParser":
                    SAMParser parseSAM = new SAMParser(CONFIG_HASH);
                    CONFIG_HASH = parseSAM.updateConfig();
                    break;
                case "PostIsomiRAnalysis" :
                    PostIsomiRAnalysis isomiRanalysis = new PostIsomiRAnalysis(CONFIG_HASH);
                    CONFIG_HASH = isomiRanalysis.updateConfig();
                    break;
                case "Plotting" :
                    Plotting plotFigures = new Plotting(CONFIG_HASH);
                    CONFIG_HASH = plotFigures.updateConfig();
                    break;
            
            }
            
        }
    }

    /**
     * set path folder for emapping reference
     * @param mappingReferecePath 
     */
    public void setMappingReference(String mappingReferecePath) {
        this.path2MappingReference = mappingReferecePath;
    }

    /**
     * set dataset  file name list
     * @param datasetfilepath 
     */
    public void setDatasetList(String datasetfilepath) {
        this.path2DatasetFile = datasetfilepath;
    }

    /**
     * set input folder contains fastq files
     * @param fastqFolder 
     */
    public void setInputPath(String fastqFolder) {
        this.path2FastqFolder =  fastqFolder;
    }

    /**
     * set output folder
     * @param outputfolder 
     */
    public void setOutputPath(String outputfolder) {
        this.folder4Output = outputfolder;
    }

    void setMappingReference() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setConfigurations(HashMap<String, String> configurationsHashMap) {
        this.CONFIG_HASH = configurationsHashMap;
    }

    public void setSteps(ArrayList<String> steps) {
        this.RUNSTEPS = steps;
    }
    
}
