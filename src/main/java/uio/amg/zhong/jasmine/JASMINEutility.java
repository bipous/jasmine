/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class JASMINEutility {
    Logger logger = LogManager.getRootLogger();
    private ArrayList<String> singlecolumn ;
    private ArrayList<String> seqTextArrayList;
    private ArrayList<String> seqStrArrayList ;
    private HashMap<String, String> seqStrNameHash ;
    private HashMap<String, String> seqLenHash ;
    private ArrayList<String> seqNameArray ;
    private ArrayList<Integer> seqLengthArray  ;
    
    public void MISOutility(){
        logger.info("hi");
    }

    ArrayList<String> readSingleColumn(String topExpressedGeFilestr) throws IOException {
        singlecolumn = new ArrayList<>();
        try{
            logger.info("reading mapping summary file <" +  topExpressedGeFilestr + ">");
            BufferedReader sralistBR = new BufferedReader(new FileReader(new File(topExpressedGeFilestr)));
            String lineStr = null;
            while ((lineStr = sralistBR.readLine())!=null){
                if(lineStr.startsWith("dataset")) continue;
                singlecolumn.add(lineStr.trim());
            }
            sralistBR.close();
            logger.info("read " + singlecolumn.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading mapping summary file <" +  topExpressedGeFilestr + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" +  topExpressedGeFilestr + ">");
        }
        return singlecolumn;
    }

    ArrayList<String> getSingleCol() {
        return singlecolumn;
    }
    
    public void readFasta(String fastaFilePath) throws FileNotFoundException, IOException{
        seqStrArrayList = new ArrayList<>();
        seqStrNameHash = new HashMap<>();
        seqLenHash = new HashMap<>();
        seqNameArray = new ArrayList<>();
        seqTextArrayList = new ArrayList<>();
        seqLengthArray = new ArrayList<>() ;
        BufferedReader fastaBR = new BufferedReader(new FileReader(new File(fastaFilePath)));
        String fastaLine = null;
        StringBuilder seqStr = new StringBuilder();
        String seqID = "";
        
        try {
            logger.info("reading fasta file <" + fastaFilePath + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(fastaFilePath)));
            String lineFA = null;
            while ((lineFA = brFA.readLine()) != null) {
                String fastaSeq = brFA.readLine().trim();
                String fastaName = lineFA.split(" ")[0].substring(1);
                seqStrNameHash.put(fastaName, fastaSeq);
                seqStrArrayList.add(fastaSeq.toString());
                seqTextArrayList.add(seqID + "\t" + seqStr.toString());
                seqLenHash.put(fastaName, String.valueOf(fastaSeq.length()));
                seqNameArray.add(fastaName + "\t" + fastaSeq);
                seqLengthArray.add(fastaSeq.length());
            }
            brFA.close();
            logger.info("read " + seqStrNameHash.size() + " entries");
            logger.info("done");

        } catch (IOException ex) {
            logger.error("error in reading fasta file <" + fastaFilePath + ">\n" + ex.toString());
            throw new IOException("error in reading fasta file <" + fastaFilePath + ">");
        }
        fastaBR.close();
    }
    
    public void readFasta(String fastaFilePath, String selectHost) throws FileNotFoundException, IOException{
        seqStrArrayList = new ArrayList<>();
        seqStrNameHash = new HashMap<>();
        seqTextArrayList = new ArrayList<>();
        seqLenHash = new HashMap<>();
        seqNameArray = new ArrayList<>();
        BufferedReader fastaBR = new BufferedReader(new FileReader(new File(fastaFilePath)));
        String fastaLine = null;
        
        StringBuilder seqStr = new StringBuilder();
        String seqID = "";
        while ((fastaLine = fastaBR.readLine()) != null) {
            if (fastaLine.startsWith(">") == true) {
                if(seqID.contains(selectHost)){
                    seqStrArrayList.add(seqStr.toString());
                    seqStrNameHash.put(seqID, seqStr.toString());
                    seqTextArrayList.add(seqID+"\t"+ seqStr.toString());
                    seqLenHash.put(seqID, String.valueOf(seqStr.length()));
                    seqNameArray.add(seqID +"\t"+ seqStr);
                }
                seqStr = new StringBuilder();
                seqID = fastaLine.trim();
                seqID = seqID.split(" ")[0];
                seqID = seqID.substring(1,(seqID.length()));                            
            } else {
                fastaLine = fastaLine.trim().replaceAll("\\s+", "");
                seqStr.append(fastaLine);
            }
        }
        fastaBR.close();
    }

    public ArrayList<String> getSeqArrayList() {
        return seqStrArrayList;
    }
    
    public ArrayList<Integer> getSeqLengthArray(){
        return seqLengthArray;
    }
    
    public ArrayList<String> getSequenceText(){
        return seqTextArrayList;
    }
    
    public HashMap<String, String> getSeqStrNameHash() {
        return seqStrNameHash;
    }
    
    public void writeStrArrayList(String outputFolder, String projectTitle, ArrayList<String> strArrayList, String fileSuffix) throws IOException {
        String outputfile = outputFolder + projectTitle+ fileSuffix; 
        BufferedWriter templatedBW  = new BufferedWriter(new FileWriter(new File(outputfile)));
        for(String istr : strArrayList){
            templatedBW.write(istr+ "\n");
        }
        templatedBW.close();
    }

    HashMap<String, String> getSeqLengthHash() {
        return seqLenHash;
    }
    
    public ArrayList<String> getSeqStrNameArray(){
    
    return seqNameArray;
    }
    
    /*
    this is to build a table of the relationship between miRNA and pre-miRNAs
    */
    public void buildAllMiRNAparental(String mirbasePath) throws FileNotFoundException, IOException{
        HashMap<String, String> matureHashMapNEW = new HashMap<>();
        for (final File fileEntry : (new File(mirbasePath)).listFiles()) {
            if (!fileEntry.isDirectory()) {
                String thisfilename = fileEntry.getName();
                if(thisfilename.contains("gff3")){
                    BufferedReader gffBR = new BufferedReader(new FileReader(new File(mirbasePath+ thisfilename)));
                    ArrayList<String> miRNArrayList = new ArrayList<>();
                    ArrayList<String> premiRNArrayList = new ArrayList<>();
                    HashMap<String, String> matureHashMap = new HashMap<>();
                    HashMap<String, String> premirnaHashMap = new HashMap<>();

                    String  gffLine = null;
                    while((gffLine=gffBR.readLine())!= null){
                        if(gffLine.startsWith("#")) continue;
                        gffLine = gffLine.trim();
                        String[] gfflineStrings = gffLine.split("\t");
                        if(gfflineStrings[2].equals("miRNA")){
                            String[] nameIDinfo = gfflineStrings[8].split(";");
                            String mirnaID = nameIDinfo[0].split("=")[1];
                            String mirnaAlias = nameIDinfo[1].split("=")[1];
                            String mirnaName = nameIDinfo[2].split("=")[1];
                            String mirnaDerives = nameIDinfo[3].split("=")[1];
                            String tmpMirnaStr = mirnaID +"\t"+ 
                                    mirnaAlias +"\t"+ 
                                    mirnaName +"\t"+ 
                                    mirnaDerives +"\t"+
                                    gfflineStrings[0] +"\t"+
                                    gfflineStrings[3] +"\t"+
                                    gfflineStrings[4] +"\t"+
                                    gfflineStrings[6]; 
                            miRNArrayList.add(tmpMirnaStr);
                            if(matureHashMap.containsKey(mirnaName)){
                                matureHashMap.replace(mirnaName, (matureHashMap.get(mirnaName) +";"+mirnaDerives));
                            }
                            else{
                                matureHashMap.put(mirnaName, mirnaDerives);
                            } 
                        }
                        else if(gfflineStrings[2].equals("miRNA_primary_transcript")){
                            String[] nameIDinfo = gfflineStrings[8].split(";");
                            String premirnaID = nameIDinfo[0].split("=")[1];
                            String premirnaAlias = nameIDinfo[1].split("=")[1];
                            String premirnaName = nameIDinfo[2].split("=")[1];
                            String tmpMirnaStr = premirnaID +"\t"+ 
                                    premirnaAlias +"\t"+ 
                                    premirnaName +"\t"+ 
                                    gfflineStrings[0] +"\t"+
                                    gfflineStrings[3] +"\t"+
                                    gfflineStrings[4] +"\t"+
                                    gfflineStrings[6]; 
                            premiRNArrayList.add(tmpMirnaStr);
                            premirnaHashMap.put(premirnaID, premirnaName);
                        }
                    }
                    
                    for (Map.Entry<String, String> matureEntry : matureHashMap.entrySet()) {
                        String ikey = matureEntry.getKey();
                        String ivalue = matureEntry.getValue();
                        if(ivalue.contains(";")){
                            String[] preIDs = ivalue.split(";");
                            String prename = "";
                            for(String ipreid: preIDs){
                                prename = prename +premirnaHashMap.get(ipreid) + ";" ;
                            }
                            matureHashMapNEW.put(ikey, prename.substring(0, (prename.length()-1)));
                        }
                        else{
                            matureHashMapNEW.put(ikey, premirnaHashMap.get(ivalue));
                        }
                    }
                }
            }
        }
        /*
        write out mature miRNA and pre-miRNA relationship information
        */
        String drivenInfoFile = mirbasePath + "miRBase.derivesInfo.tsv";
        BufferedWriter derivesBW  = new BufferedWriter(new FileWriter(new File(drivenInfoFile)));
        for (Map.Entry<String, String> entry : matureHashMapNEW.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            derivesBW.write(key +"\t"+ value + "\n");            
        }
        derivesBW.close();
    }
    
    public  ArrayList<String> getDatasetList(String dataLayoutFilename) throws IOException {
        ArrayList datasetArrayList = new ArrayList();
        try{
            logger.info("reading data layout file <" +  dataLayoutFilename + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(dataLayoutFilename)));
            String lineFA = null;
            while ((lineFA = brFA.readLine())!=null){
                if(lineFA.startsWith("File")) continue;
                String datasetString = lineFA.trim().split("\t")[0];
                datasetArrayList.add(datasetString);
            }
            brFA.close();
            logger.info("read " + datasetArrayList.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading data layout file <" +  dataLayoutFilename + ">\n" + ex.toString());
            throw new IOException("error reading data layout file <" +  dataLayoutFilename + ">");
        }
        return datasetArrayList;
    }

    public String getReverseComplment(String fastaSeqStr) {
        String rcSeqStr = "";
        for(int i=fastaSeqStr.length()-1; i >= 0; i --){
            char curr = fastaSeqStr.charAt(i);
            
            if(curr == 'A')
                rcSeqStr += 'T';
            
            else if(curr == 'T')
                rcSeqStr += 'A';
            
            else if(curr == 'C')
                rcSeqStr += 'G';
            
            else if(curr == 'G')
                rcSeqStr += 'C';
            
            else if(curr == 'N')
                rcSeqStr += 'N';
            
            else {
                logger.info("ERROR: Input is not a DNA Sequence.");
            }
        }
        return rcSeqStr;
    }

    public double getGCcontent(String seqStr){
        double gcontent = 0.0;
        int countG = seqStr.length() - seqStr.toUpperCase().replace("G", "").length();
        int countC = seqStr.length() - seqStr.toUpperCase().replace("C", "").length();
        
        gcontent = (countG + countC) /(double)seqStr.length() ;
        return gcontent;
    }
    
    public void appendTextArray2File(ArrayList<String> strArrayList, String appednedFileName){
        BufferedWriter bufferAppendWriter = null;
        
        try {
            bufferAppendWriter = new BufferedWriter(new FileWriter(appednedFileName, true));
            for(String istr : strArrayList){
                bufferAppendWriter.write(istr+ "\n");
            }
            bufferAppendWriter.close();
        } 
        catch (IOException e) {
            logger.info("ERROR from appending text to file");
            e.printStackTrace();
        }
    }
}
