/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class SAMParser {
    Logger logger = LogManager.getRootLogger();
    private String TASK_FOLDER = "IsomiRsReports";
    private String  HOST;
    private String OUTPUTFOLDER;
    private String projectID;
    private String mirBasePath;
    private int mismatch;
    private String inputFolder ;
    private String datasetlstFile;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder ;
    private int readCopyThreshold;  
    
    private HashMap<String,String> matureMBfastaDict;
    private HashMap<String,String> hairpinMBfastaDict;
    private HashMap<String,Integer> mappingSummaryHashMap;
    private ArrayList<String> mergedMiRNAsList;
    private HashMap<String,ArrayList> matureInhairpinProfile;
    private ArrayList<String> top100Expressed = new ArrayList<>();
    
    private ArrayList<String> multiLociArray = new ArrayList<>();
    private ArrayList<String> closeLocationArray = new ArrayList<>();
    private static final String RAW_INPUT_EXTENSION = ".fastq.gz";

    private static final String INFILE_EXTENSION = ".trim.clp.gen.sam";
    private HashMap<String, String> CONFIG_HASH;
    private String CUSTOMIZED_FASTA;
    private SNPMatcher snpMatcher;
    private int READ_minLen = 10;
    private int READ_maxLen = 40;
    private HashMap<String, Integer> iSAM_preMiR_CountingHash;
    private HashMap<String, Integer> iSAM_MiR_CountingHash;
    private HashMap<String, Integer[]> iSAM_armUsage_Hash;
    private ArrayList<String> iSAM_isoReportArray_isomiR;
    private int NT_SHIFTING = 3;
    
    SAMParser(HashMap<String, String> CONFIGHASH) throws IOException {
        this.CONFIG_HASH = CONFIGHASH;
        setConfigurations(CONFIG_HASH);
        
        
        snpMatcher = new SNPMatcher(HOST);
        logger.info("SNP profile available for "+HOST+" : "+snpMatcher.isAvailable());
        
        File refFile= new File( projectFolder+"Reference" );
        if(refFile.exists() && refFile.isDirectory()) {
            startParsingSAM();  // this can infer the SAM files are generated within jasmine
        }
        else{
            parsingCustomizedSAM(); // this can infer the SAM files are generated from customized mapping
        }
 
    }

    private void setConfigurations(HashMap<String, String> CONFIG_HASH) throws IOException {
        this.HOST = CONFIG_HASH.get("HOST");   
        this.mismatch = Integer.valueOf(CONFIG_HASH.get("MISMATCH"));
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        JASMINEutility misoUtility = new JASMINEutility();
        if(CONFIG_HASH.get("DATASET_ARRAY_STR").equals("NA")){
            this.datasetArray = misoUtility.getDatasetList(CONFIG_HASH.get("DATASETLIST"));
        }
        else{
            this.datasetArray = new ArrayList<String>(Arrays.asList(CONFIG_HASH.get("DATASET_ARRAY_STR").split("#"))) ;
        }
                
        if(CONFIG_HASH.containsKey("DATASETLIST")){
            this.datasetlstFile = CONFIG_HASH.get("DATASETLIST");
        }
        else{
            this.datasetlstFile = CONFIG_HASH.get("PROJECT_FOLDER") +"configs/mirna-hairpin.tsv";
        }
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        }
        else{
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER") +FILESEPARATOR;
        }   
        if (new File(CONFIG_HASH.get("INPUTFOLDER")).isDirectory()) {
            this.inputFolder = CONFIG_HASH.get("INPUTFOLDER");
        }
        else{
            this.inputFolder = projectFolder + CONFIG_HASH.get("INPUTFOLDER") +FILESEPARATOR;
        }
        this.OUTPUTFOLDER = projectFolder +TASK_FOLDER +FILESEPARATOR;
  
        if(CONFIG_HASH.get("MIRBASE_PATH").endsWith(FILESEPARATOR)){
            this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH");
        }
        else{
            this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH")+FILESEPARATOR;
        }
        this.CUSTOMIZED_FASTA = CONFIG_HASH.get("CUSTOMIZED_FASTA");
        /*
        set read count threshold
        */
        String readCountConf = CONFIG_HASH.get("READ_FILTERING_COUNT");
        if (readCountConf.equals("NA")) {
            readCopyThreshold = 0;
        }
        else{
            try {
                readCopyThreshold = Integer.valueOf(readCountConf);
            } catch (Exception e) {
                readCopyThreshold = 0;
                logger.warn("jasmine count not convert READ_FILTERING_COUNT: "+ readCountConf+ " into Integer. So using > 0 <");
            }
        }
        /*
        set read length threshold
        */
        String readLenThresholdStr = CONFIG_HASH.get("READ_FILTERING_LENGTH");
        if(readLenThresholdStr.equals("NA")){
            logger.info("jasmine is going to use default read length filter, any reads shorter than 10 or longer than 40 nts are filtered out!");
        }
        else{
            if (readLenThresholdStr.contains(":")) {
                int sepIndex = readLenThresholdStr.indexOf(":");
                if (sepIndex == 0) {
                    logger.info("no min read length defined");
                    try {
                        READ_maxLen = Integer.valueOf(readLenThresholdStr.split(":")[1].replaceAll("\\s",""));
                    } catch (Exception e) {
                        logger.error("error in defining max read length");
                        logger.warn("jasmine is using default max read length filter 40");
                    }
                }
                else if(sepIndex == (readLenThresholdStr.length()-1)) {
                    logger.info("no max read length defined");
                    try {
                        READ_minLen = Integer.valueOf(readLenThresholdStr.split(":")[0].replaceAll("\\s",""));
                    } catch (Exception e) {
                        logger.error("error in defining min read length");
                        logger.warn("jasmine is using default min read length filter 10");
                    }
                }
                else{
                    try {
                        READ_maxLen = Integer.valueOf(readLenThresholdStr.split(":")[1].replaceAll("\\s",""));
                    } catch (Exception e) {
                        logger.error("error in defining max read length");
                        logger.warn("jasmine is using default max read length filter 40");
                    }
                    
                    try {
                        READ_minLen = Integer.valueOf(readLenThresholdStr.split(":")[0].replaceAll("\\s",""));
                    } catch (Exception e) {
                        logger.error("error in defining min read length");
                        logger.warn("jasmine is using default min read length filter 10");
                    }
                }
            }
            else{
                logger.error("jasmine require \":\" as seperator ");
                logger.warn("jasmine is going to use default read length filter, any reads shorter than 10 or longer than 40 nts are filtered out!");
            }
        }
        logger.info("jasmine is filtering reads by " + String.valueOf(READ_minLen) +"<= read length <="+ String.valueOf(READ_maxLen));
        
        /*
        set up the number of shifting nucleotides at each teminals
        */
        String configedShiftingStr = CONFIG_HASH.get("NT_SHIFTING");
        if(configedShiftingStr.equals("NA")){
            logger.info("jasmine is using default NT_SHIFTING: 3 ");
        }
        else {
            try {
                this.NT_SHIFTING = Integer.valueOf(configedShiftingStr);
                logger.info("jasmine is using user defined NT_SHIFTING: "+ configedShiftingStr);
            } catch (Exception e) {
                logger.error("jasmine could not convert NT_SHIFTING: >" + configedShiftingStr+"< into Integer");
                logger.warn("jasmine is using default NT_SHIFTING: 3 ");
            }
        }
        
    }


    private void startParsingSAM() throws IOException {
        /*
        this method is depend on 
            (1) gff3 file;
            (2) jasmine curated mature fasta sequences;
            (3) jasmine curated miRNA precursor sequences;
            (4) jasmine build profile of miRNA locating in precursor
        */
        if(!inputFolder.endsWith(FILESEPARATOR)){
            this.inputFolder = inputFolder+FILESEPARATOR;
        }
        
        this.datasetArray = getDatasetList(datasetlstFile);
        
        Boolean fA = new File(OUTPUTFOLDER).mkdir();
        if (fA) {
            logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
        }
        logger.info("Starting parsing SAM file, and building isomiR report...");
        
//        String gffFileMirBase = mirBasePath+FILESEPARATOR +"genomes"+FILESEPARATOR+HOST+".gff3";
        String miRNAFastaFileMirBase = projectFolder+"Reference"+FILESEPARATOR+"miRBase."+HOST+".unique.mature.fasta";
        String hairpinFastaFileMirBase = projectFolder+"Reference"+FILESEPARATOR+"miRBase."+HOST+".unique.hairpin.fasta";

        ArrayList<String> updatedMappingSummary = new ArrayList<>();
        String mappingSummaryFile = this.cleanPath(inputFolder + projectID + ".mappingSummary.tsv");

        String mature2HairpinProfileFile = projectFolder+"Reference"+FILESEPARATOR+"miRBase"+"."+HOST+".matureInHairpin.tsv";
        matureInhairpinProfile = readMatureInHairpinProfile(mature2HairpinProfileFile);
        parseMatureInhairpin(mature2HairpinProfileFile);
        mergedMiRNAsList = getMergedMiRNAs(matureInhairpinProfile);
        matureMBfastaDict = getNameSeqDict(miRNAFastaFileMirBase);
        hairpinMBfastaDict = getNameSeqDict(hairpinFastaFileMirBase);
        HashMap<String, String> mappingCountSummary = readMappingSummary(mappingSummaryFile);
        String samLine = null;
        String samInputFile = "";

        HashMap<String, HashMap> premirnaCountAmongDatasets = new HashMap<>();
        HashMap<String, HashMap> degradationCountAmongDatasets = new HashMap<>();
        HashMap<String, HashMap> mirnaCountAmongDatasets = new HashMap<>();
        ArrayList<String> datasetList = new ArrayList<>();
        HashMap<String, HashMap> armRatioAmongDatasets = new HashMap<>();

        ArrayList<String> alignRefFaArray = new ArrayList<>();
        
        logger.info("--processing dataset");
        for (String idatasetFastaGz : datasetArray) {
            int reliableTotal = 0;
            int ambiguousTotal = 0;
            int unmappedTotal =0;
            int totalSum =0;
            int mappedSum = 0;
            try {   
                samInputFile = this.cleanPath(inputFolder + idatasetFastaGz.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION));
                logger.info("--parsing SAM file <" + samInputFile + ">");
                logger.info(samInputFile);
                String datasetName = idatasetFastaGz.replace(RAW_INPUT_EXTENSION, "");
                datasetList.add(datasetName);   
                iSAM_preMiR_CountingHash = new HashMap<>();
                HashMap<String, Integer> degradationCounting = new HashMap<>();
                iSAM_MiR_CountingHash = new HashMap<>();
                HashMap<String, Integer> isomirCounting = new HashMap<>();
                iSAM_isoReportArray_isomiR = new ArrayList<>();
                iSAM_armUsage_Hash = new HashMap<>();
                samLine = null;

                BufferedReader brSAM = new BufferedReader(new FileReader(new File(samInputFile)));
                while ((samLine = brSAM.readLine()) != null) {
                    /*
                        1   QNAME	   Query template NAME
                        2   FLAG	   bitwise FLAG
                        3   RNAME	   Reference sequence NAME
                        4   POS	   1-based leftmost mapping POSition
                        5   MAPQ	   MAPping Quality
                        6   CIGAR	   CIGAR string
                        7   RNEXT	   Ref. name of the mate/next read
                        8   PNEXT	   Position of the mate/next read
                        9   TLEN	   observed Template LENgth
                        10  SEQ	   segment SEQuence
                        11  QUAL	   ASCII of Phred-scaled base QUALity+33

                     */
                    if (samLine.startsWith("@")) {
                        if (samLine.startsWith("@SQ")) {
                            alignRefFaArray.add(samLine.split("SN:")[1].split("\t")[0]);
                        }
                        continue;
                    }

                    String[] linevalues = samLine.trim().split("\t");
                    String mappedFlag = linevalues[1];
                    int readcopy = 0;
                    if(linevalues[0].contains("-")){
                        readcopy = Integer.valueOf(linevalues[0].split("-")[1]); // for collapsed reads
                        totalSum += readcopy;
                    }
                    else{
                        readcopy = 1;  // for uncollapsed reads
                        totalSum += 1;
                    }

                    if (mappedFlag.equals("0")) {
                        String readName = linevalues[0];
                        String targetPremiRNAname = linevalues[2].toLowerCase();
                        String targetPremiRNAseq = hairpinMBfastaDict.get(targetPremiRNAname).replace("U", "T");
                        int startPosition = Integer.valueOf(linevalues[3]);
                        String cigarStr = linevalues[5];
                        String readSeq = linevalues[9];
                        int readSeqLen = readSeq.length();
                        
                        mappedSum = mappedSum+ readcopy;
                        
                        /*
                        read filtering
                        assume working on collapsed read datasets
                        */
                        boolean isDiscarding = false;
                        if (readcopy < readCopyThreshold) {
                            isDiscarding = true;
                        }
                        if (readSeqLen < READ_minLen) {
                            isDiscarding = true;
                        }
                        if (readSeqLen > READ_maxLen) {
                            isDiscarding = true;
                        }
                        
                        /*
                        now, working on isomiR identification
                        */
                        int sumFilterOur = 0;
                        if (! isDiscarding) {
                            // working on isomiR
                            JASMINE_Identifing_IsomiR(datasetName, linevalues);
                        }
                        else{
                            // how many filter out
                            sumFilterOur += readcopy;
                        }
                        
                        
                        
                        
                        // this means this reads mapped to hairpin
                        if (readcopy > readCopyThreshold) {
                            // here, we only look at "confident" reads
                            ArrayList<String> derivesToMiRNAs = matureInhairpinProfile.get(targetPremiRNAname);
                            /*
                            counting reads that mapped to pre-miRNAs
                             */
                            iSAM_preMiR_CountingHash = updateCountHashMap(targetPremiRNAname, readcopy, iSAM_preMiR_CountingHash);

                            boolean findit = false;
                            String refMappedName = "";
                            for (String iRefMappedInfo : derivesToMiRNAs) {
                                int refMappedPos = Integer.valueOf(iRefMappedInfo.split(":")[2]) + 1;
                                refMappedName = iRefMappedInfo.split(":")[0];
//                                    logger.info("-->"+refMappedName);
                                String singleName = refMappedName;
//                                if (singleName.contains("|")){
//                                    singleName = refMappedName.split("\\|")[0];
//                                }
                                String refMappedSeq = (matureMBfastaDict.get(singleName)).replace("U", "T");
                                int refMappedLen = refMappedSeq.length();
                                int iReadEndPos = readSeqLen + startPosition - 1;
                                int iReferEndPos = refMappedLen + refMappedPos - 1;

                                String amgiguousTag = checkAmbiguous(refMappedName);
                                if (!amgiguousTag.equals("#")) {
                                    refMappedName = amgiguousTag + refMappedName;
                                }  
                            }
                            if(findit){
                                if(refMappedName.contains("*")){
                                    ambiguousTotal = ambiguousTotal+readcopy;
                                }
                                else{
                                    reliableTotal = reliableTotal + readcopy;
                                }
                            }
                            /*
                            counting degradation
                             */
                            if (!findit) {
                                /*
                                if this read not fulfil the requirement as miRNA based on start position and length
                                we will consider it as product of degradation
                                 */
                                degradationCounting = updateCountHashMap(targetPremiRNAname, readcopy, degradationCounting);
                            }
                        }
                        else{
                            ArrayList<String> derivesToMiRNAs = matureInhairpinProfile.get(targetPremiRNAname);
                            for (String iRefMappedInfo : derivesToMiRNAs) {
                                int refMappedPos = Integer.valueOf(iRefMappedInfo.split(":")[2]) + 1;
                                String refMappedName = iRefMappedInfo.split(":")[0];
                                String singleName = refMappedName;
                                String refMappedSeq = (matureMBfastaDict.get(singleName)).replace("U", "T");
                                int refMappedLen = refMappedSeq.length();
                                int iReadEndPos = readSeqLen + startPosition - 1;
                                int iReferEndPos = refMappedLen + refMappedPos - 1;
                                String amgiguousTag = checkAmbiguous(refMappedName);
                                if (!amgiguousTag.equals("#")) {
                                    refMappedName = amgiguousTag + refMappedName;
                                }
                                if(refMappedName.contains("*")){
                                    ambiguousTotal = ambiguousTotal+readcopy;
                                }
                                else{
                                    reliableTotal = reliableTotal + readcopy;
                                }
                            }
                            //TODO
                        }
                    }
                    else{
                        unmappedTotal = unmappedTotal + Integer.valueOf(linevalues[0].split("-")[1]);
                    }
                }
                brSAM.close();
                logger.info("finished <<--" + datasetName);
                logger.info(mappedSum +"\t"+ String.valueOf(totalSum)+"\t"+ String.valueOf(mappedSum));
                updatedMappingSummary.add(mappedSum +"\t"+ String.valueOf(totalSum)+"\t"+ String.valueOf(mappedSum));
                /*
                write out isomiR identifying report to file
                 */
//                iSAM_isoReportArray_isomiR
//                ArrayList<String> writeoutIsoReport = new ArrayList<>();
                String isomiRreportfile = OUTPUTFOLDER + datasetName + ".isomiRs.report.tsv";
                BufferedWriter isoReportBW = new BufferedWriter(new FileWriter(new File(isomiRreportfile)));
                for (String isomiRreportLine : iSAM_isoReportArray_isomiR) {
                    isoReportBW.write(isomiRreportLine + "\n");
                }
                isoReportBW.close();
                premirnaCountAmongDatasets.put(datasetName, iSAM_preMiR_CountingHash);
                degradationCountAmongDatasets.put(datasetName, degradationCounting);
                mirnaCountAmongDatasets.put(datasetName, iSAM_MiR_CountingHash);
                armRatioAmongDatasets.put(datasetName, iSAM_armUsage_Hash);
                /*
                first sort the hash map and then add top 100 expressed miRNAs to the ArrayList
                 */
                addTopExpressedmiRNAs(iSAM_MiR_CountingHash);
                logger.info("completed processing SAM file\n\n");
            } catch (IOException ex) {
                logger.error("error processing sample <" + samInputFile + ">\n" + ex.toString());
                throw new IOException( ": error processing sample <" + samInputFile + ">");
            } catch (ArrayIndexOutOfBoundsException exBnd) {
                logger.error("error parsing line " + samLine);
                logger.error(exBnd);
                throw new IOException( ": error processing sample <" + samInputFile + ">: samLine was \n" + samLine);
            }
            String tmpnewmap = idatasetFastaGz.replace(RAW_INPUT_EXTENSION, "")+"\t"+String.valueOf(reliableTotal)
                    +"\t"+String.valueOf(ambiguousTotal)+"\t"+String.valueOf(unmappedTotal);
            updatedMappingSummary.add(mappingCountSummary.get(idatasetFastaGz.replace(RAW_INPUT_EXTENSION, "")) 
                    +"\t"+tmpnewmap);
            logger.info("--finished parsing SAM file <" + samInputFile + ">");
        }
        /*
        write out pre-miRNA count data
         */
        ArrayList<String> writeoutPreMiRNA = new ArrayList<>();
        for (String thisPremirna : hairpinMBfastaDict.keySet()) {
            String tmpprecountString = thisPremirna;
            for (String idataset : datasetList) {
//                logger.info(idataset);
                HashMap<String, Integer> iPremirnaCount = premirnaCountAmongDatasets.get(idataset);
                if (iPremirnaCount.containsKey(thisPremirna)) {
                    int premirnaCount = iPremirnaCount.get(thisPremirna);
                    tmpprecountString = tmpprecountString + "\t" + Integer.toString(premirnaCount);
                } else {
                    tmpprecountString = tmpprecountString + "\t" + "0";
                }
            }
            writeoutPreMiRNA.add(tmpprecountString);
        }
        String preMiRNACOUNTfile = OUTPUTFOLDER + projectID + ".count.premiRNA.tsv";
        BufferedWriter preMiRNABW = new BufferedWriter(new FileWriter(new File(preMiRNACOUNTfile)));
        for (String premirnaLine : writeoutPreMiRNA) {
            preMiRNABW.write(premirnaLine + "\n");
        }
        preMiRNABW.close();
        /*
        write out miRNA count data
         */
        ArrayList<String> writeoutMiRNA = new ArrayList<>();
        String headlineMirna = "name";

        for (int i = 0; i < datasetList.size(); i++) {
            headlineMirna = headlineMirna + "\t" + datasetList.get(i);
        }
        for (String mirna : mergedMiRNAsList) {
            String tmpMirnacountString = mirna;

            for (int i = 0; i < datasetList.size(); i++) {
                HashMap<String, Integer> iMirnaCount = mirnaCountAmongDatasets.get(datasetList.get(i));
                if (iMirnaCount.containsKey(mirna)) {
                    int mirnaCount = iMirnaCount.get(mirna);
                    tmpMirnacountString = tmpMirnacountString + "\t" + Integer.toString(mirnaCount);
                } else {
                    tmpMirnacountString = tmpMirnacountString + "\t" + "0";
                }
            }
            writeoutMiRNA.add(tmpMirnacountString);
        }
        String mirnaCOUNTfile = OUTPUTFOLDER  + projectID + ".count.miRNA.tsv";
        BufferedWriter mirnaBW = new BufferedWriter(new FileWriter(new File(mirnaCOUNTfile)));
        mirnaBW.write(headlineMirna + "\n");
        for (String mirnaLine : writeoutMiRNA) {
            mirnaBW.write(mirnaLine + "\n");
        }
        mirnaBW.close();
//      writeStrArrayList(OUTPUTFOLDER,projectID,updatedMappingSummary,"mappingSummary.tsv");     
        /*
        write out top 100 expressed miRNAs among all dataset
         */
//        ArrayList<String> writeoutMiRNAtoplist = new ArrayList<>();
        String mirnaExpressedTopfile = OUTPUTFOLDER + projectID + ".topExpressedList.tsv";
        BufferedWriter mirnaTopBW = new BufferedWriter(new FileWriter(new File(mirnaExpressedTopfile)));
        for (String mirnaname : top100Expressed) {
            mirnaTopBW.write(mirnaname + "\n");
        }
        mirnaTopBW.close();
        /*
        write out degradation count data
         */
        ArrayList<String> writeoutDegradation = new ArrayList<>();
        for (String thisPremirna : hairpinMBfastaDict.keySet()) {
            String tmpprecountString = thisPremirna;
            for (String idataset : datasetList) {
                HashMap<String, Integer> iPremirnaCount = premirnaCountAmongDatasets.get(idataset);
                if (iPremirnaCount.containsKey(thisPremirna)) {
                    int premirnaCount = iPremirnaCount.get(thisPremirna);
                    HashMap<String, Integer> iDegradationCount = degradationCountAmongDatasets.get(idataset);
                    int degradationCount = 0;
                    if (iDegradationCount.containsKey(thisPremirna)) {
                        degradationCount = iDegradationCount.get(thisPremirna);
                    }
                    double ratio = (double) degradationCount / (double) premirnaCount;
                    tmpprecountString = tmpprecountString + "\t" + Double.toString(ratio);
                } else {
                    /*
                    here we are using "NA" to distinguish from zero reads mapped hairpin and no degradation
                     */
                    tmpprecountString = tmpprecountString + "\t" + "NA";
                }
            }
            writeoutDegradation.add(tmpprecountString);
        }
        String degradationCOUNTfile = OUTPUTFOLDER + projectID + ".count.degradation.tsv";
        BufferedWriter degradationBW = new BufferedWriter(new FileWriter(new File(degradationCOUNTfile)));
        for (String premirnaLine : writeoutDegradation) {
            degradationBW.write(premirnaLine + "\n");
        }
        degradationBW.close();
        /*
        write out arm ratio to file
         */
        ArrayList<String> writeoutArmRatio = new ArrayList<>();
        for (String premirna : hairpinMBfastaDict.keySet()) {
            String tmpArmCountString = "";
            for (String idataset : datasetList) {
                HashMap<String, Integer[]> iArmCount = armRatioAmongDatasets.get(idataset);
                HashMap<String, Integer> iPremirnaCount = premirnaCountAmongDatasets.get(idataset);
                if (iArmCount.containsKey(premirna)) {
                    int premirnaTotalCount = iPremirnaCount.get(premirna);
                    Integer[] tmparmCount = iArmCount.get(premirna);
                    double arm5ratio = (double) tmparmCount[0] / (double) premirnaTotalCount;
                    double arm3ratio = (double) tmparmCount[1] / (double) premirnaTotalCount;
                    tmpArmCountString = tmpArmCountString + idataset + "\t" + premirna + "\t" + Double.toString(arm5ratio) + "\t" + Double.toString(arm3ratio) + "\n";
                }
            }
            if (tmpArmCountString != "") {
                writeoutArmRatio.add(tmpArmCountString);
            }
        }
        String armratioCOUNTfile = OUTPUTFOLDER + projectID + ".count.armRatio.tsv";
        BufferedWriter armratioBW = new BufferedWriter(new FileWriter(new File(armratioCOUNTfile)));
        for (String armratioLine : writeoutArmRatio) {
            armratioBW.write(armratioLine);
        }
        armratioBW.close();
    }
    
    private String cleanPath(String pathstring) {
        return pathstring.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
    }
    
    private  ArrayList<String> getDatasetList(String dataLayoutFilename) throws IOException {
        ArrayList datasetArrayList = new ArrayList();
        try{
            logger.info("reading data layout file <" +  dataLayoutFilename + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(dataLayoutFilename)));
            String lineFA = null;
            while ((lineFA = brFA.readLine())!=null){
                if(lineFA.startsWith("File")) continue;
                String datasetString = lineFA.trim().split("\t")[0];
                datasetArrayList.add(datasetString);
            }
            brFA.close();
            logger.info("read " + datasetArrayList.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading data layout file <" +  dataLayoutFilename + ">\n" + ex.toString());
            throw new IOException("error reading data layout file <" +  dataLayoutFilename + ">");
        }
        return datasetArrayList;
    }

    private void parseMatureInhairpin(String mature2HairpinProfileFile) throws IOException {
        ArrayList<String> uniqueMatureArray = new ArrayList<>();
        ArrayList<String> duplicateMatureArray = new ArrayList<>();
        
        HashMap <String, ArrayList> profileHashMap =  new HashMap();
        try{
            logger.info("reading mapping summary file <" +  mature2HairpinProfileFile + ">");
            String profileLine = null;
            BufferedReader profileBR = new BufferedReader(new FileReader(new File(mature2HairpinProfileFile)));
            while ((profileLine = profileBR.readLine())!=null){
                profileLine = profileLine.trim();
                if (profileLine.contains(";")) {
                    for(String imatureStr :profileLine.split("\t")[1].split(";") ){
                        String imaturename = imatureStr.split(":")[0];
                        if (uniqueMatureArray.contains(imaturename)) {
                            if (!duplicateMatureArray.contains(imaturename)){
                                duplicateMatureArray.add(imaturename);
                                multiLociArray.add(imaturename);
                            }
                        }
                        else{
                            uniqueMatureArray.add(imaturename);
                        }
                    }
                    checkCloseLocation(profileLine.split("\t")[1]);
                }
                else{
                    String imaturename = profileLine.split("\t")[1].split(":")[0];
                    if (uniqueMatureArray.contains(imaturename)) {
                        if (!duplicateMatureArray.contains(imaturename)){
                            duplicateMatureArray.add(imaturename);
                            multiLociArray.add(imaturename);
                        }
                    }
                    else{
                        uniqueMatureArray.add(imaturename);
                    }
                }
            }
            profileBR.close();
            logger.info("read " + profileHashMap.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading mapping summary file <" +  mature2HairpinProfileFile + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" +  mature2HairpinProfileFile + ">");
        }
        logger.info(uniqueMatureArray.size());
        logger.info(duplicateMatureArray.size());
        logger.info(multiLociArray.size());
    }
    
    private HashMap<String, Integer> readMappingSummaryThreshold(String mappingSummaryFile) throws IOException {
        HashMap <String, Integer> summaryHashMap =  new HashMap();
        try{
            logger.info("reading mapping summary file <" +  mappingSummaryFile + ">");
            String summamryLine = null;
            BufferedReader summaryBR = new BufferedReader(new FileReader(new File(mappingSummaryFile)));
            while ((summamryLine = summaryBR.readLine())!=null){
                if(summamryLine.startsWith("dataset")) continue;
                String datasetname = summamryLine.split("\t")[0];
                String mappedReadCounts = summamryLine.split("\t")[2];
                int thresholdMapping = Integer.valueOf(mappedReadCounts) /100000;
                summaryHashMap.put(datasetname, thresholdMapping);  
            }
            summaryBR.close();
            logger.info("read " + summaryHashMap.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading mapping summary file <" +  mappingSummaryFile + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" +  mappingSummaryFile + ">");
        }
        return summaryHashMap;
    }
    
    private HashMap<String, String> readMappingSummary(String mappingSummaryFile) throws IOException {
        HashMap <String, String> summaryHashMap =  new HashMap();
        try{
            logger.info("reading mapping summary file <" +  mappingSummaryFile + ">");
            String summamryLine = null;
            BufferedReader summaryBR = new BufferedReader(new FileReader(new File(mappingSummaryFile)));
            while ((summamryLine = summaryBR.readLine())!=null){
                if(summamryLine.startsWith("dataset")) continue;
                String datasetname = summamryLine.split("\t")[0];
//                String mappedReadCounts = summamryLine.split("\t")[2];
//                int thresholdMapping = Integer.valueOf(mappedReadCounts) /100000;
                summaryHashMap.put(datasetname, summamryLine.trim());  
            }
            summaryBR.close();
            logger.info("read " + summaryHashMap.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading mapping summary file <" +  mappingSummaryFile + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" +  mappingSummaryFile + ">");
        }
        return summaryHashMap;
    }
    
    private HashMap<String, String> getNameSeqDict(String fastaFile) throws IOException {
        HashMap <String, String> fastaHashMap = new HashMap();
        try{
            logger.info("reading miRBase fasta reference file <" +  fastaFile + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(fastaFile)));
            String lineFA = null;
            while ((lineFA = brFA.readLine())!=null){
                String fastaSeq = brFA.readLine().trim();
                String fastaName = lineFA.split(" ")[0].substring(1);
                fastaHashMap.put(fastaName, fastaSeq);
            }
            brFA.close();
            logger.info("read " + fastaHashMap.size() + " entries");
        }
        catch(IOException ex){
            logger.error("error reading miRBase fasta reference file <" +  fastaFile + ">\n" + ex.toString());
            throw new IOException("error reading miRBase fasta reference file <" +  fastaFile + ">");
        }
        return fastaHashMap;
    }
    
    private HashMap<String, ArrayList> readMatureInHairpinProfile(String mature2HairpinProfileFile) throws IOException {
        HashMap <String, ArrayList> profileHashMap =  new HashMap();
        try{
            logger.info("reading mapping summary file <" +  mature2HairpinProfileFile + ">");
            String profileLine = null;
            BufferedReader profileBR = new BufferedReader(new FileReader(new File(mature2HairpinProfileFile)));
            while ((profileLine = profileBR.readLine())!=null){
                /*
                example line:
                hsa-mir-6858	hsa-miR-6858-3p:22:41:63;hsa-miR-6858-5p:22:0:22
                hsa-mir-5091	hsa-miR-5091:23:10:33
                */
                String premirnaStr = profileLine.split("\t")[0];
                ArrayList<String> containMatures = new ArrayList<>();
                
                String matureClusters = profileLine.split("\t")[1].trim();
                if(matureClusters.contains(";")){
                    for(String imature : matureClusters.split(";")){
                        containMatures.add(imature);
                    }
                }
                else{
                    containMatures.add(matureClusters);
                }
                profileHashMap.put(premirnaStr, containMatures);
            }
            profileBR.close();
            logger.info("read " + profileHashMap.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading mapping summary file <" +  mature2HairpinProfileFile + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" +  mature2HairpinProfileFile + ">");
        }
        return profileHashMap;
    }
    
    
    private ArrayList<String> getMergedMiRNAs(HashMap<String, ArrayList> matureInhairpinProfile) {
        ArrayList<String> miRNAlist = new ArrayList<>();
        for (Map.Entry<String, ArrayList> entry : matureInhairpinProfile.entrySet()) {
            String key = entry.getKey();
            ArrayList<String> value = entry.getValue();
            for (String mappedMiR: value){
                /*
                avoid duplicate miRNA name list; 
                those same miRNAs bur from different pre-mirnas
                */
                if (!miRNAlist.contains(mappedMiR.split(":")[0])){
                    miRNAlist.add(mappedMiR.split(":")[0]);
                } 
            } 
        }
        ArrayList<String> newmiRNAlist = new ArrayList<>();
        for(String imatureString: miRNAlist){
            String amgiguousTag = checkAmbiguous(imatureString);
            if(!amgiguousTag.equals("#")){
                String taggedMature = amgiguousTag+imatureString;
                newmiRNAlist.add(taggedMature);
//                logger.info(taggedMature);
            }
            else{
                newmiRNAlist.add(imatureString);
            }
        }
        
        return newmiRNAlist;
    }
    
    private HashMap<String, Integer> updateCountHashMap(String ikey, int icount, HashMap<String, Integer> countHashmap) {
        if (countHashmap.containsKey(ikey)) {
            int tmpCount = countHashmap.get(ikey) + icount;
            countHashmap.replace(ikey, tmpCount);
        }
        else{
            countHashmap.put(ikey, icount);
        }
        return countHashmap;
    }
    
    
    private String checkAmbiguous(String refMappedName) {
        String checktag = "#";
        if(multiLociArray.contains(refMappedName)){
            if(closeLocationArray.contains(refMappedName)){
                checktag = "***";
            }
            else{
                checktag = "*";
            }
        }
        else{
            if(closeLocationArray.contains(refMappedName)){
                checktag = "**";
            }
            else{
                checktag = "#";
            }
        }
        return checktag;
    }
    
    private String findPolymorphicSite(String referenceStr, String queryStr) {
        ArrayList<String> polyDetails = new ArrayList<>();
        String[] referStrArray = referenceStr.split("");
        String[] queryStrArray = queryStr.split("");
        for(int i =0; i < referStrArray.length; i++){
            if (!referStrArray[i].equals(queryStrArray[i])) {
                String polymorphismEvent = String.valueOf(i+1)+"p:" +referStrArray[i].toUpperCase() +">"+ queryStrArray[i].toUpperCase();
                polyDetails.add(polymorphismEvent);
            }
        }
        // build result string
        ArrayList<String> polyArray = new ArrayList<>();
        for (String tmpStr : polyDetails)
        {
            polyArray.add(tmpStr);
        }
        return StringUtils.join(polyArray, ";");
    }

    private String findPolyInOverlap(String referenceStr, String queryStr, int gapStart) {
        ArrayList<String> polyDetails = new ArrayList<>();
        String[] referStrArray = referenceStr.split("");
        String[] queryStrArray = queryStr.split("");
        for(int i =0; i < referStrArray.length; i++){
            if (!referStrArray[i].equals(queryStrArray[i])) {
                String polymorphismEvent = String.valueOf(i+1+gapStart)+"p:" +referStrArray[i].toUpperCase() +">"+ queryStrArray[i].toUpperCase();
                polyDetails.add(polymorphismEvent);
            }
        }
        // build result string
        ArrayList<String> polyInfoArray = new ArrayList<>();
        for (String tmpStr : polyDetails)
        {
            polyInfoArray.add(tmpStr);
        }
        return StringUtils.join(polyInfoArray, ";");
    }

    private void addTopExpressedmiRNAs(HashMap<String, Integer> mirnaCounting) {
        boolean orderType = false; // sorting descindeng order
        List<Map.Entry<String, Integer>> countlist = new LinkedList<Map.Entry<String, Integer>>(mirnaCounting.entrySet());
//        List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(countlist, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> tmpObj1,
                    Map.Entry<String, Integer> tmpObj2)
            {
                if (orderType)
                {
                    return tmpObj1.getValue().compareTo(tmpObj2.getValue());
                }
                else
                {
                    return tmpObj2.getValue().compareTo(tmpObj1.getValue());

                }
            }
        });
        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : countlist)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        logger.info(sortedMap.size());
        int jindex = 0;
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            String orderedKey = entry.getKey();
            if(jindex <= 100){
                if (!top100Expressed.contains(orderedKey)){
                    top100Expressed.add(orderedKey);
                }
            }
            jindex +=1; 
        }
    }

    private void checkCloseLocation(String nameClusterStr) {
        String[] splitedNameCluster = nameClusterStr.split(";");
        for(int i=0;i < (splitedNameCluster.length -1); i ++){
            int diffStart = Math.abs(Integer.valueOf(splitedNameCluster[i].split(":")[2]) - Integer.valueOf(splitedNameCluster[i+1].split(":")[2]));
            int diffEnd = Math.abs(Integer.valueOf(splitedNameCluster[i].split(":")[3]) - Integer.valueOf(splitedNameCluster[i+1].split(":")[3]));
            if(diffStart <=2 | diffEnd <= 2){
//                logger.info(nameClusterStr);
//                logger.info(splitedNameCluster[i].split(":")[0] +"\t\t\t"+ splitedNameCluster[i+1].split(":")[0]);
                if(!closeLocationArray.contains(splitedNameCluster[i].split(":")[0])){
                    closeLocationArray.add(splitedNameCluster[i].split(":")[0]);
                }
                if(!closeLocationArray.contains(splitedNameCluster[i+1].split(":")[0])){
                    closeLocationArray.add(splitedNameCluster[i+1].split(":")[0]);
                }
            }
        }
    }

    public HashMap<String, String> updateConfig() {
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        CONFIG_HASH.replace("DATASET_ARRAY_STR", StringUtils.join(datasetArray, "#"));
        return CONFIG_HASH;
    }

    private void parsingCustomizedSAM() throws IOException {
        logger.info("parsing user provided SAM files");
        logger.info("jasmine need mapping reference in fasta format");
        if (CUSTOMIZED_FASTA.equals("NA") | !(new File(CUSTOMIZED_FASTA).exists())) {
            logger.error("jasmine could not do anything without fasta file");
        }
        else {
            /*
            reading customized fasta file Approach 1
            */
            HashMap<String, String> custFaDict = new HashMap();
//            try {
//                logger.info("reading customized fasta reference file <" + CUSTOMIZED_FASTA + ">");
//                BufferedReader brFA = new BufferedReader(new FileReader(new File(CUSTOMIZED_FASTA)));
//                String lineFA = null;
//                while ((lineFA = brFA.readLine()) != null) {
//                    String fastaSeq = brFA.readLine().trim();
//                    String fastaName = lineFA.split(" ")[0].substring(1);
//                    custFaDict.put(fastaName, fastaSeq);
//                }
//                brFA.close();
//                logger.info("read " + custFaDict.size() + " entries");
//                logger.info("done");
//
//            } catch (IOException ex) {
//                logger.error("error reading customized fasta reference file <" + CUSTOMIZED_FASTA + ">\n" + ex.toString());
//                throw new IOException("error reading customized fasta reference file <" + CUSTOMIZED_FASTA + ">");
//            }
            /*
            reading customized fasta file Approach 2
            */
            JASMINEutility misoTool = new JASMINEutility();
            misoTool.readFasta(CUSTOMIZED_FASTA);
            custFaDict = misoTool.getSeqStrNameHash();
            int iCustMean = getMeanFastaLen(misoTool.getSeqLengthArray());
            logger.info(iCustMean);
            
            for (String idatasetFastaGz : datasetArray) {
                ArrayList<String> alignRefFaArray = new ArrayList<>();
                String samInputFile = null;
                try {   
                    samInputFile = this.cleanPath(inputFolder + idatasetFastaGz.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION));
                    logger.info(samInputFile);
                    String datasetName = idatasetFastaGz.replace(RAW_INPUT_EXTENSION, "");

                    String samLine = null;
                    
                    int totalSum = 0;

                    BufferedReader brSAM = new BufferedReader(new FileReader(new File(samInputFile)));
                    while ((samLine = brSAM.readLine()) != null) {
                        if (samLine.startsWith("@")) {
                            if (samLine.startsWith("@SQ")) {
                                alignRefFaArray.add(samLine.split("SN:")[1].split("\t")[0]);
                            }
                            continue;
                        }

                        String[] linevalues = samLine.trim().split("\t");
                        String mappedFlag = linevalues[1];
                        int readcopy = 0;
                        if(linevalues[0].contains("-")){
                            readcopy = Integer.valueOf(linevalues[0].split("-")[1]); // for collapsed reads
                            totalSum += readcopy;
                        }
                        else{
                            readcopy = 1;  // for uncollapsed reads
                            totalSum += 1;
                        }
                    }
                }catch (IOException ex) {
                logger.error("error processing sample <" + samInputFile + ">\n" + ex.toString());
                throw new IOException( ": error processing sample <" + samInputFile + ">");
            } catch (ArrayIndexOutOfBoundsException exBnd) {
                logger.error(exBnd);
            }
            }

        }
    }

    private int getMeanFastaLen(ArrayList<Integer> seqLengthArray) {
        int lensum = 0;
        for(int tempj =0; tempj < seqLengthArray.size(); tempj ++){
            lensum += seqLengthArray.get(tempj);
        }
        return ( lensum / seqLengthArray.size());
    }

    private void JASMINE_Identifing_IsomiR(String iSAMdataset, String[] samLineSplitted) {
        String readName = samLineSplitted[0];
        String targetPremiRNAname = samLineSplitted[2].toLowerCase();
        String targetPremiRNAseq = hairpinMBfastaDict.get(targetPremiRNAname).replace("U", "T");
        int startPosition = Integer.valueOf(samLineSplitted[3]);
        String cigarStr = samLineSplitted[5];
        String readSeq = samLineSplitted[9];
        int readSeqLen = readSeq.length();
        int readcopy = 0;
        if(samLineSplitted[0].contains("-")){
            readcopy = Integer.valueOf(samLineSplitted[0].split("-")[1]); // for collapsed reads
        }
        else{
            readcopy = 1;  // for uncollapsed reads
        }
        String iSNPinfoStr = "NA";
        if (snpMatcher.isAvailable()) {
            
        }
        
        ArrayList<String> derivesToMiRNAs = matureInhairpinProfile.get(targetPremiRNAname);
        /*
        counting reads that mapped to pre-miRNAs
         */
        iSAM_preMiR_CountingHash = updateCountHashMap(targetPremiRNAname, readcopy, iSAM_preMiR_CountingHash);
        boolean findit = false;
        String refMappedName = "";
        
        for (String iRefMappedInfo : derivesToMiRNAs) {
            int refMappedPos = Integer.valueOf(iRefMappedInfo.split(":")[2]) + 1;
            refMappedName = iRefMappedInfo.split(":")[0];
            String singleName = refMappedName;
            String refMappedSeq = (matureMBfastaDict.get(singleName)).replace("U", "T");
            int refMappedLen = refMappedSeq.length();
            int iReadEndPos = readSeqLen + startPosition - 1;
            int iReferEndPos = refMappedLen + refMappedPos - 1;
            String amgiguousTag = checkAmbiguous(refMappedName);
            if (!amgiguousTag.equals("#")) {
                refMappedName = amgiguousTag + refMappedName;
            }

            if (refMappedPos - NT_SHIFTING < startPosition && startPosition < refMappedPos + NT_SHIFTING) {
                if (iReferEndPos - NT_SHIFTING < iReadEndPos && iReadEndPos < iReferEndPos + NT_SHIFTING) {
                    /*
                    in this case, we consider this read as miRNA reads
                     */
                    findit = true;
                    iSAM_MiR_CountingHash = updateCountHashMap(refMappedName, readcopy, iSAM_MiR_CountingHash);
                    /*
                    isomiR counting
                     */
                    String reportString = "";
                    String isoformInfo = "";
                    String isoType = "";
                    String seedInfo = "";
                    if (readSeqLen == refMappedLen) {
                        if (startPosition == refMappedPos) {
                            if (readSeq.equals(refMappedSeq)) {
                                isoType = refMappedName + ":" + "mature";
                                isoformInfo = "reference mature";
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "mature" + "\t" + isoformInfo+ "\tmature\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                isoType = refMappedName + ":" + "poly";
                                isoformInfo = findPolymorphicSite(refMappedSeq, readSeq);
                                iSNPinfoStr = buildSNPinfo(targetPremiRNAname, refMappedName, isoformInfo);
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "poly" + "\t" + isoformInfo+ "\t"+ seedInfo +"\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        } else if (refMappedPos > startPosition) {
                            // isomiR that sliding to 5 end a bit
                            isoType = refMappedName + ":" + "sl-5e-3d";
                            // how many nt are different from start position, as an polymorphism index
                            int gapStart = Math.abs(refMappedPos - startPosition);
                            // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                            String compReferString = targetPremiRNAseq.substring(startPosition - 1, (refMappedPos - 1 + readSeqLen));
                            String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                            String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (startPosition - 1 + readSeqLen));
                            String overlapReadSeqStr = readSeq.substring(gapStart, readSeqLen);
                            String extendedReferStr = targetPremiRNAseq.substring((startPosition - 1), (refMappedPos - 1));
                            String extendedRedStr = readSeq.substring(0, gapStart);
                            String deletedSeqStr = targetPremiRNAseq.substring((startPosition - 1 + refMappedLen), (refMappedPos - 1 + readSeqLen));
                            String extendedReferStrRev = new StringBuffer(extendedReferStr).reverse().toString();
                            String extendedRedStrRev = new StringBuffer(extendedRedStr).reverse().toString();

                            // summary extension event
                            ArrayList<String> extensionArrayList = new ArrayList<>();
                            for (int i = 0;
                                    i < extendedRedStrRev.length();
                                    i++) {
                                String tmpReport = "5e+" + String.valueOf(i + 1) + ":" + extendedReferStrRev.substring(i, i + 1) + ">" + extendedRedStrRev.substring(i, i + 1);
                                extensionArrayList.add(tmpReport);
                            }
                            for (int i = 0;
                                    i < deletedSeqStr.length();
                                    i++) {
                                String tmps = "3d" + String.valueOf(refMappedLen - deletedSeqStr.length() + i + 1) + ":" + refMappedSeq.substring((refMappedLen - deletedSeqStr.length() + i), (refMappedLen - deletedSeqStr.length() + i + 1)) + ">D";
                                extensionArrayList.add(tmps);
                            }

                            if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                //same length isomiR with 5 end deletion and 3 end extension
                                isoType = refMappedName + ":" + "sl-5e-3d";
                                isoformInfo = StringUtils.join(extensionArrayList,";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sl-5e-3d" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                //same length isomiR with 5 end deletion and 3 end extension
                                // and polymorphism in overlap region
                                isoType = refMappedName + ":" + "sl-5e-3d-p";
                                String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                extensionArrayList.add(polyInOverlap);
                                isoformInfo = StringUtils.join(extensionArrayList,";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sl-5e-3d-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        } else if (refMappedPos < startPosition) {
                            // isomiR that sliding to 3 end a bit
                            isoType = refMappedName + ":" + "sl-5d-3e";
                            // how many nt are different from start position, as an polymorphism index
                            int gapStart = Math.abs(refMappedPos - startPosition);
                            // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                            String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (startPosition - 1 + readSeqLen));
                            String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                            String overlapReferSeqStr = targetPremiRNAseq.substring(startPosition - 1, (refMappedPos - 1 + readSeqLen));
                            String overlapReadSeqStr = readSeq.substring(0, readSeqLen - gapStart);
                            String extendedReferStr = targetPremiRNAseq.substring((refMappedPos - 1 + refMappedLen), (startPosition - 1 + readSeqLen));
                            String extendedRedStr = readSeq.substring(readSeqLen - gapStart, readSeqLen);
                            String deletedSeqStr = refMappedSeq.substring(0, gapStart);
                            String deletedSeqStrRev = new StringBuffer(deletedSeqStr).reverse().toString();
                            String extendedReferStrRev = new StringBuffer(extendedReferStr).reverse().toString();
                            String extendedRedStrRev = new StringBuffer(extendedRedStr).reverse().toString();

                            // summary modification event
                            ArrayList<String> extensionArrayList = new ArrayList<>();
                            for (int i = 0;
                                    i < deletedSeqStrRev.length();
                                    i++) {
                                String tmps = "5d" + String.valueOf(i + 1) + ":" + deletedSeqStrRev.substring(i, (i + 1)) + ">D";
                                extensionArrayList.add(tmps);
                            }
                            for (int i = 0;
                                    i < extendedRedStr.length();
                                    i++) {
                                String tmpReport = "3e+" + String.valueOf(i + 1) + ":" + extendedReferStr.substring(i, i + 1) + ">" + extendedRedStr.substring(i, i + 1);
                                extensionArrayList.add(tmpReport);
                            }
                            if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                //same length isomiR with 5 end deletion and 3 end extension
                                isoType = refMappedName + ":" + "sl-5d-3e";
                                isoformInfo = StringUtils.join(extensionArrayList,";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sl-5d-3e" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                //same length isomiR with 5 end deletion and 3 end extension
                                // and polymorphism in overlap region
                                isoType = refMappedName + ":" + "sl-5d-3e-p";
                                String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                extensionArrayList.add(polyInOverlap);
                                isoformInfo = StringUtils.join(extensionArrayList,";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sl-5d-3e-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        }
                    } else if (readSeqLen < refMappedLen) {
                        // shorter isoform
                        int gapStart = Math.abs(refMappedPos - startPosition);
                        int gapEnd = Math.abs((startPosition + readSeqLen) - (refMappedLen + refMappedPos));
                        if (startPosition == refMappedPos) {
                            // only 3 end deletion
                            isoType = refMappedName + ":" + "sr-3d";
                            // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                            String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (startPosition - 1 + refMappedLen));
                            String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                            String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + readSeqLen));
                            String overlapReadSeqStr = readSeq;
                            String deletedSeqStr = refMappedSeq.substring(readSeqLen);
                            // summary modification event
                            ArrayList<String> extensionArrayList = new ArrayList<>();
                            for (int i = 0;
                                    i < deletedSeqStr.length();
                                    i++) {
                                String tmps = "3d" + String.valueOf(readSeqLen + i + 1) + ":" + deletedSeqStr.substring(i, (i + 1)) + ">D";
                                extensionArrayList.add(tmps);
                            }
                            if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                //same length isomiR with 5 end deletion and 3 end extension
                                isoType = refMappedName + ":" + "sr-3d";
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-3d" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                //same length isomiR with 5 end deletion and 3 end extension
                                // and polymorphism in overlap region
                                isoType = refMappedName + ":" + "sr-3d-p";
                                String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                extensionArrayList.add(polyInOverlap);
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-3d-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        } else if (startPosition > refMappedPos) {
                            //deletion at 5 end
                            if (gapEnd == 0) {
                                //only deletion at 5 end
                                isoType = refMappedName + ":" + "sr-5d";
                                // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                                String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen));
                                String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                                String overlapReferSeqStr = targetPremiRNAseq.substring(startPosition - 1, (startPosition - 1 + readSeqLen));
                                String overlapReadSeqStr = readSeq;
                                String deletedSeqStr = targetPremiRNAseq.substring((refMappedPos - 1), (startPosition - 1));
                                String deletedSeqStrRev = new StringBuffer(deletedSeqStr).reverse().toString();
                                // summary modification event
                                ArrayList<String> extensionArrayList = new ArrayList<>();
                                for (int i = 0;
                                        i < deletedSeqStrRev.length();
                                        i++) {
                                    String tmps = "5d" + String.valueOf(i + 1) + ":" + deletedSeqStrRev.substring(i, (i + 1)) + ">D";
                                    extensionArrayList.add(tmps);
                                }
                                if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                    //same length isomiR with 5 end deletion and 3 end extension
                                    isoType = refMappedName + ":" + "sr-5d";
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5d" + "\t" + isoformInfo+ "\t"+ seedInfo + "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                } else {
                                    //same length isomiR with 5 end deletion and 3 end extension
                                    // and polymorphism in overlap region
                                    isoType = refMappedName + ":" + "sr-5d-p";
                                    String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                    extensionArrayList.add(polyInOverlap);
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5d-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                }
                            } else if (gapEnd != 0) {
                                // both modification at 5 and 3 end
                                int readsEndPos = startPosition + readSeqLen - 1;
                                int referMappedEndPos = refMappedPos + refMappedLen - 1;
                                if (readsEndPos < referMappedEndPos) {
                                    //both deletion at 5 end and 3 end; 
                                    isoType = refMappedName + ":" + "sr-5d-3d";
                                    // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                                    String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen));
                                    String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                                    String overlapReferSeqStr = targetPremiRNAseq.substring(startPosition - 1, (startPosition - 1 + readSeqLen));
                                    String overlapReadSeqStr = readSeq;
                                    // fragment deleted at 5 end
                                    String deleted5ENDSeqStr = targetPremiRNAseq.substring((refMappedPos - 1), (startPosition - 1));
                                    // fragment deleted at 3 end
                                    String deleted3ENDSeqStr = targetPremiRNAseq.substring((startPosition - 1 + readSeqLen), (refMappedPos - 1 + refMappedLen));
                                    String deleted5ENDSeqStrRev = new StringBuffer(deleted5ENDSeqStr).reverse().toString();
                                    // summary modification event
                                    ArrayList<String> extensionArrayList = new ArrayList<>();
                                    for (int i = 0;
                                            i < deleted5ENDSeqStrRev.length();
                                            i++) {
                                        String tmps = "5d" + String.valueOf(i + 1) + ":" + deleted5ENDSeqStrRev.substring(i, (i + 1)) + ">D";
                                        extensionArrayList.add(tmps);
                                    }
                                    for (int i = 0;
                                            i < deleted3ENDSeqStr.length();
                                            i++) {
                                        String tmps = "3d" + String.valueOf(refMappedLen - gapEnd + i + 1) + ":" + deleted3ENDSeqStr.substring(i, (i + 1)) + ">D";
                                        extensionArrayList.add(tmps);
                                    }
                                    if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                        isoType = refMappedName + ":" + "sr-5d-3d";
                                        isoformInfo = StringUtils.join(extensionArrayList, ";");
                                        seedInfo = findSeedChange(isoformInfo);
                                        reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5d-3d" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                        iSAM_isoReportArray_isomiR.add(reportString);
                                    } else {
                                        //same length isomiR with 5 end deletion and 3 end extension
                                        // and polymorphism in overlap region
                                        isoType = refMappedName + ":" + "sr-5d-3d-p";
                                        String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                        extensionArrayList.add(polyInOverlap);
                                        isoformInfo = StringUtils.join(extensionArrayList, ";");
                                        seedInfo = findSeedChange(isoformInfo);
                                        reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5d-3d-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                        iSAM_isoReportArray_isomiR.add(reportString);
                                    }
                                } else if (readsEndPos > referMappedEndPos) {
                                    //deletion at 5 end; extension at 3 end; <--- more deletion
                                    isoType = refMappedName + ":" + "sr-5d-3e";
                                    // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                                    String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (startPosition - 1 + readSeqLen));
                                    String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                                    String overlapReferSeqStr = targetPremiRNAseq.substring(startPosition - 1, (refMappedPos - 1 + refMappedLen));
                                    String overlapReadSeqStr = readSeq.substring(0, (readSeqLen + gapStart - 1 - gapEnd - 1));
                                    String deletedSeqStr = targetPremiRNAseq.substring((refMappedPos - 1), (startPosition - 1));
                                    String deletedSeqStrRev = new StringBuffer(deletedSeqStr).reverse().toString();
                                    String extendedRefStr = targetPremiRNAseq.substring((refMappedPos + refMappedLen - 1), (startPosition + readSeqLen - 1));
                                    String extendedSeqStr = readSeq.substring(refMappedLen - gapEnd - 1);
                                    // summary modification event
                                    ArrayList<String> extensionArrayList = new ArrayList<>();
                                    for (int i = 0;
                                            i < deletedSeqStrRev.length();
                                            i++) {
                                        String tmps = "5d" + String.valueOf(i + 1) + ":" + deletedSeqStrRev.substring(i, (i + 1)) + ">D";
                                        extensionArrayList.add(tmps);
                                    }
                                    for (int i = 0;
                                            i < extendedSeqStr.length();
                                            i++) {
                                        String tmps = "3e+" + String.valueOf(i + 1) + ":" + extendedRefStr.substring(i, (i + 1)) + ">" + extendedSeqStr.substring(i, (i + 1));
                                        extensionArrayList.add(tmps);
                                    }
                                    if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                        //same length isomiR with 5 end deletion and 3 end extension
                                        isoType = refMappedName + ":" + "sr-5d-3e";
                                        isoformInfo = StringUtils.join(extensionArrayList, ";");
                                        seedInfo = findSeedChange(isoformInfo);
                                        reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5d-3e" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                        iSAM_isoReportArray_isomiR.add(reportString);
                                    } else {
                                        //same length isomiR with 5 end deletion and 3 end extension
                                        // and polymorphism in overlap region
                                        isoType = refMappedName + ":" + "sr-5d-3e-p";
                                        String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                        extensionArrayList.add(polyInOverlap);
                                        isoformInfo = StringUtils.join(extensionArrayList, ";");
                                        seedInfo = findSeedChange(isoformInfo);
                                        reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5d-3e-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                        iSAM_isoReportArray_isomiR.add(reportString);
                                    }
                                }
                            }
                        } else if (startPosition < refMappedPos) {
                            //sr-5e-3d??
                            // extension at 5 end deletion at 3 end; but more deletion at 3 end
                            isoType = refMappedName + ":" + "sr-5e-3d";
                            int readsEndPos = startPosition + readSeqLen - 1;
                            // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                            String compReferString = targetPremiRNAseq.substring(startPosition - 1, (refMappedPos - 1 + refMappedLen));
                            String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                            String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen - gapEnd));
                            String overlapReadSeqStr = readSeq.substring(gapStart);
                            String extendedRefStr = targetPremiRNAseq.substring((startPosition - 1), (refMappedPos - 1));
                            String extendedSeqStr = readSeq.substring(0, gapStart);
                            String extendedRefStrRev = new StringBuffer(extendedRefStr).reverse().toString();
                            String extendedSeqStrRev = new StringBuffer(extendedSeqStr).reverse().toString();
                            String deletedSeqStr = targetPremiRNAseq.substring(readsEndPos, (refMappedPos - 1 + refMappedLen));
                            // summary modification event
                            ArrayList<String> extensionArrayList = new ArrayList<>();
                            for (int i = 0;
                                    i < extendedSeqStrRev.length();
                                    i++) {
                                String tmps = "5e+" + String.valueOf(i + 1) + ":" + extendedRefStrRev.substring(i, (i + 1)) + ">" + extendedSeqStrRev.substring(i, (i + 1));
                                extensionArrayList.add(tmps);
                            }
                            for (int i = 0;
                                    i < deletedSeqStr.length();
                                    i++) {
                                String tmps = "3d" + String.valueOf(overlapReferSeqStr.length() + i + 1) + ":" + deletedSeqStr.substring(i, (i + 1)) + ">D";
                                extensionArrayList.add(tmps);
                            }
                            if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                //same length isomiR with 5 end extension  and 3 end deletion
                                isoType = refMappedName + ":" + "sr-5e-3d";
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5e-3d" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                //same length isomiR with 5 end deletion and 3 end extension
                                // and polymorphism in overlap region
                                isoType = refMappedName + ":" + "sr-5e-3d-p";
                                String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                extensionArrayList.add(polyInOverlap);
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "sr-5e-3d-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        }
                    } else if (readSeqLen > refMappedLen) {
                        int gapStart = Math.abs(refMappedPos - startPosition);
                        int gapEnd = Math.abs((startPosition + readSeqLen - 1) - (refMappedLen + refMappedPos - 1));
                        // longer isoform
                        if (startPosition == refMappedPos) {
                            // only extension at 3 end
                            isoType = refMappedName + ":" + "lr-3e";
                            int readsEndPos = startPosition + readSeqLen - 1;
                            // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                            String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + readSeqLen));
                            String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                            String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen));
                            //+1-1 should not merge into 0!!
                            // +1 ---> calculature the position
                            // -1 transfer the position to index
                            String overlapReadSeqStr = readSeq.substring(0, (readSeqLen - gapEnd + 1 - 1));
                            String extendedRefStr = targetPremiRNAseq.substring((refMappedPos - 1 + refMappedLen), (refMappedPos - 1 + refMappedLen + gapEnd));
                            String extendedSeqStr = readSeq.substring((readSeqLen - gapEnd + 1 - 1));
                            // summary modification event
                            ArrayList<String> extensionArrayList = new ArrayList<>();
                            for (int i = 0;
                                    i < extendedSeqStr.length();
                                    i++) {
                                String tmps = "3e+" + String.valueOf(i + 1) + ":" + extendedRefStr.substring(i, (i + 1)) + ">" + extendedSeqStr.substring(i, (i + 1));
                                extensionArrayList.add(tmps);
                            }
                            if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                //same length isomiR with 3 end extension  
                                isoType = refMappedName + ":" + "lr-3e";
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-3e" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                //same length isomiR with 3 end extension
                                // and polymorphism in overlap region
                                isoType = refMappedName + ":" + "lr-3e-p";
                                String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                extensionArrayList.add(polyInOverlap);
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-3e-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        } else if (startPosition > refMappedPos) {
                            //deletion at 5 end, must have 3 end extension                                                
                            isoType = refMappedName + ":" + "lr-5d-3e";
                            int readsEndPos = startPosition + readSeqLen - 1;
                            // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                            String compReferString = targetPremiRNAseq.substring(refMappedPos - 1, (startPosition - 1 + readSeqLen));
                            String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                            String overlapReferSeqStr = targetPremiRNAseq.substring(startPosition - 1, (refMappedPos - 1 + refMappedLen));
                            //+1-1 should not merge into 0!!
                            // +1 ---> calculature the position
                            // -1 transfer the position to index
                            String overlapReadSeqStr = readSeq.substring(0, (readSeqLen - gapEnd + 1 - 1));
                            String extendedRefStr = targetPremiRNAseq.substring((refMappedPos - 1 + refMappedLen), (startPosition - 1 + readSeqLen));
                            String extendedSeqStr = readSeq.substring((readSeqLen - gapEnd + 1 - 1));
                            String deletedSeqStr = targetPremiRNAseq.substring((refMappedPos - 1), (startPosition - 1));
                            String deletedSeqStrRev = new StringBuffer(deletedSeqStr).reverse().toString();
                            // summary modification event
                            ArrayList<String> extensionArrayList = new ArrayList<>();
                            //summary deletion events
                            for (int i = 0;
                                    i < deletedSeqStrRev.length();
                                    i++) {
                                String tmps = "5d" + String.valueOf(i + 1) + ":" + deletedSeqStrRev.substring(i, (i + 1)) + ">" + "D";
                                extensionArrayList.add(tmps);
                            }
                            // summary extension events
                            for (int i = 0;
                                    i < extendedSeqStr.length();
                                    i++) {
                                String tmps = "3e+" + String.valueOf(refMappedLen + i + 1) + ":" + extendedRefStr.substring(i, (i + 1)) + ">" + extendedSeqStr.substring(i, (i + 1));
                                extensionArrayList.add(tmps);
                            }
                            if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                //same length isomiR with 3 end extension  
                                isoType = refMappedName + ":" + "lr-5d-3e";
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5d-3e" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            } else {
                                //same length isomiR with 3 end extension
                                // and polymorphism in overlap region
                                isoType = refMappedName + ":" + "lr-5d-3e-p";
                                String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                extensionArrayList.add(polyInOverlap);
                                isoformInfo = StringUtils.join(extensionArrayList, ";");
                                seedInfo = findSeedChange(isoformInfo);
                                reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5d-3e-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                iSAM_isoReportArray_isomiR.add(reportString);
                            }
                        } else if (startPosition < refMappedPos) {
                            //extension at 5 end
                            int readEndPos = startPosition + readSeqLen - 1;
                            int referEndPos = refMappedLen + refMappedPos - 1;
                            if (readEndPos == referEndPos) {
                                // only 5 extension
                                isoType = refMappedName + ":" + "lr-5e";
                                int readsEndPos = startPosition + readSeqLen - 1;
                                // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                                String compReferString = targetPremiRNAseq.substring(startPosition - 1, (refMappedPos - 1 + refMappedLen));
                                String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                                String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen));
                                //+1-1 should not merge into 0!!
                                // +1 ---> calculature the position
                                // -1 transfer the position to index
                                String overlapReadSeqStr = readSeq.substring(gapStart);
                                String extendedRefStr = targetPremiRNAseq.substring((startPosition - 1), (refMappedPos - 1));
                                String extendedSeqStr = readSeq.substring(0, gapStart);
                                // summary modification event
                                ArrayList<String> extensionArrayList = new ArrayList<>();
                                for (int i = 0;
                                        i < extendedSeqStr.length();
                                        i++) {
                                    String tmps = "5e+" + String.valueOf(i + 1) + ":" + extendedRefStr.substring(i, (i + 1)) + ">" + extendedSeqStr.substring(i, (i + 1));
                                    extensionArrayList.add(tmps);
                                }
                                if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                    //same length isomiR with 3 end extension  
                                    isoType = refMappedName + ":" + "lr-5e";
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5e" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                } else {
                                    //same length isomiR with 3 end extension
                                    // and polymorphism in overlap region
                                    isoType = refMappedName + ":" + "lr-5e-p";
                                    String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                    extensionArrayList.add(polyInOverlap);
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5e-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                }
                            } else if (readEndPos < referEndPos) {
                                // 5 end extendsion + 3 end deletion
                                isoType = refMappedName + ":" + "lr-5e-3d";
                                int readsEndPos = startPosition + readSeqLen - 1;
                                // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                                String compReferString = targetPremiRNAseq.substring(startPosition - 1, (startPosition - 1 + readSeqLen));
                                String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                                String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen - gapEnd));
                                String overlapReadSeqStr = readSeq.substring((gapStart), readSeqLen);
                                String extendedRefStr = targetPremiRNAseq.substring((startPosition - 1), (refMappedPos - 1));
                                String extendedRefStrRev = new StringBuffer(extendedRefStr).reverse().toString();
                                String extendedSeqStr = readSeq.substring(0, gapStart);
                                String extendedSeqStrRev = new StringBuffer(extendedSeqStr).reverse().toString();
                                String deletedSeqStr = targetPremiRNAseq.substring(readsEndPos, (refMappedPos - 1 + refMappedLen));
                                // summary modification event
                                ArrayList<String> extensionArrayList = new ArrayList<>();
                                for (int i = 0;
                                        i < extendedRefStrRev.length();
                                        i++) {
                                    String tmps = "5e+" + String.valueOf(i + 1) + ":" + extendedRefStrRev.substring(i, (i + 1)) + ">" + extendedSeqStrRev.substring(i, (i + 1));
                                    extensionArrayList.add(tmps);
                                }
                                for (int i = 0;
                                        i < deletedSeqStr.length();
                                        i++) {
                                    String tmps = "3d" + String.valueOf(refMappedLen - i - 1) + ":" + deletedSeqStr.substring(i, (i + 1)) + ">" + "D";
                                    extensionArrayList.add(tmps);
                                }
                                if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                    //same length isomiR with 3 end extension  
                                    isoType = refMappedName + ":" + "lr-5e-3d";
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString = readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5e-3d" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                } else {
                                    //same length isomiR with 3 end extension
                                    // and polymorphism in overlap region
                                    isoType = refMappedName + ":" + "lr-5e-3d-p";
                                    String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                    extensionArrayList.add(polyInOverlap);
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5e-3d-p" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                }
                            } else if (readEndPos > referEndPos) {
                                // 5 end extendsion + 3 end extension
                                isoType = refMappedName + ":" + "lr-5e-3e";
                                int readsEndPos = startPosition + readSeqLen - 1;
                                // the whole region contains extended and deleted and mature sequence for comparaison; templated or not
                                String compReferString = targetPremiRNAseq.substring(startPosition - 1, (startPosition - 1 + readSeqLen));
                                String tmpSeqBlock = compReferString + "\n" + refMappedSeq + "\n" + readSeq;
                                String overlapReferSeqStr = targetPremiRNAseq.substring(refMappedPos - 1, (refMappedPos - 1 + refMappedLen));
                                //+1-1 should not merge into 0!!
                                // +1 ---> calculature the position
                                // -1 transfer the position to index
                                String overlapReadSeqStr = readSeq.substring((gapStart), (readSeqLen - gapEnd));
                                String extendedRef5str = targetPremiRNAseq.substring((startPosition - 1), (refMappedPos - 1));
                                String extendedSeq5str = readSeq.substring(0, gapStart);
                                String extendedRef5strRev = new StringBuffer(extendedRef5str).reverse().toString();
                                String extendedSeq5strRev = new StringBuffer(extendedSeq5str).reverse().toString();
                                String extendedRef3str = targetPremiRNAseq.substring((refMappedPos - 1 + refMappedLen), (readEndPos));
                                String extendedSeq3str = readSeq.substring((readSeqLen - gapEnd + 1 - 1));
                                // summary modification event
                                ArrayList<String> extensionArrayList = new ArrayList<>();
                                // summary 5 end extension
                                for (int i = 0;
                                        i < extendedRef5strRev.length();
                                        i++) {
                                    String tmps = "5e+" + String.valueOf(i + 1) + ":" + extendedRef5strRev.substring(i, (i + 1)) + ">" + extendedSeq5strRev.substring(i, (i + 1));
                                    extensionArrayList.add(tmps);
                                }
                                // summary 3 end extension
                                for (int i = 0;
                                        i < extendedRef3str.length();
                                        i++) {
                                    String tmps = "3e+" + String.valueOf(i + 1) + ":" + extendedRef3str.substring(i, (i + 1)) + ">" + extendedSeq3str.substring(i, (i + 1));
                                    extensionArrayList.add(tmps);
                                }
                                if (overlapReadSeqStr.equals(overlapReferSeqStr)) {
                                    //same length isomiR with 3 end extension  
                                    isoType = refMappedName + ":" + "lr-5e-3e";
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5e-3e" + "\t" + isoformInfo+ "\t"+ seedInfo+ "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                } else {
                                    //same length isomiR with 3 end extension
                                    // and polymorphism in overlap region
                                    isoType = refMappedName + ":" + "lr-5e-3e-p";
                                    String polyInOverlap = findPolyInOverlap(overlapReferSeqStr, overlapReadSeqStr, gapStart);
                                    extensionArrayList.add(polyInOverlap);
                                    isoformInfo = StringUtils.join(extensionArrayList, ";");
                                    seedInfo = findSeedChange(isoformInfo);
                                    reportString =  readName + "\t" + refMappedName + "\t" + isoType + "\t" + String.valueOf(readcopy) + "\t" + "lr-5e-3e-p" + "\t" + isoformInfo+ "\t"+ seedInfo + "\t" + iSNPinfoStr;
                                    iSAM_isoReportArray_isomiR.add(reportString);
                                }
                            }
                        }
                    }
                    /*
                    ########################################
                    #### finished isomiR counting##########
                    ########################################
                     */

                    /*
                    arm ratio. to count how many reads mapped onto 3/5 arm
                     */
                    int tmp5ArmCount = 0;
                    int tmp3ArmCount = 0;
                    int targetPremiRNAseqLen = targetPremiRNAseq.length();
                    if (startPosition < (targetPremiRNAseqLen / 2 - refMappedLen / 2)) {
                        // read mapped to 5 arm
                        tmp5ArmCount = readcopy;
                    } else if (startPosition > (targetPremiRNAseqLen / 2)) {
                        // reads mapped to 3 arm
                        tmp3ArmCount = readcopy;
                    }
                    if (iSAM_armUsage_Hash.containsKey(targetPremiRNAname)) {
                        Integer[] tmpIntArray = iSAM_armUsage_Hash.get(targetPremiRNAname);
                        tmpIntArray[0] = tmpIntArray[0] + tmp5ArmCount;
                        tmpIntArray[1] = tmpIntArray[1] + tmp3ArmCount;
                        iSAM_armUsage_Hash.replace(targetPremiRNAname, tmpIntArray); // update hash map
                    } else {
                        Integer[] tmpIntArray = {tmp5ArmCount, tmp3ArmCount};
                        iSAM_armUsage_Hash.put(targetPremiRNAname, tmpIntArray);
                    }
                }
                // end out of NT_SHIFTING
            }
            // start out of NT_SHIFTING
        }
    }

    private String buildSNPinfo(String mappedPrecursorName, String mappedRefName, String isoInfoStr) {
        String isnpInfoStr = "NA";
        if (snpMatcher.isAvailable()) {
            ArrayList<String> snpArray = new ArrayList<>();
            String[] isoSplitted = isoInfoStr.split(";");
            for (int tmj = 0; tmj < isoSplitted.length; tmj++) {
                String posStr = isoSplitted[tmj].split(":")[0];
                String ntChangeStr = isoSplitted[tmj].split(":")[1];
                ArrayList<String> tmpMother2ChildInfo = matureInhairpinProfile.get(mappedPrecursorName);
                int matureLength = 0;
                int matureStart = 0;
                int matureEnd = 0;
                for(int i=0; i <tmpMother2ChildInfo.size(); i++ ){
                    if(tmpMother2ChildInfo.get(i).contains(mappedRefName)){
                        matureLength = Integer.valueOf(tmpMother2ChildInfo.get(i).split(":")[1]);
                        matureStart = Integer.valueOf(tmpMother2ChildInfo.get(i).split(":")[2]);
                        matureEnd = Integer.valueOf(tmpMother2ChildInfo.get(i).split(":")[3]);
                    }
                }
                /* converting position into position on precursor sequence
                */
                if(posStr.contains("p")){
                    posStr = posStr.substring(0, posStr.length()-1 );
                    posStr = String.valueOf( Integer.valueOf(posStr) +  matureStart);
                }
                else if (posStr.contains("3d")){
                    posStr = posStr.substring(2, posStr.length());
                    posStr = String.valueOf( matureEnd - Integer.valueOf(posStr) -1);
                }
                else if (posStr.contains("5d")){
                    posStr = posStr.substring(2, posStr.length());
                    posStr = String.valueOf( matureStart + Integer.valueOf(posStr) -1);
                }
                else if (posStr.contains("3e")){
                    posStr = posStr.substring(3, posStr.length());
                    posStr = String.valueOf( matureEnd + Integer.valueOf(posStr) );
                }
                else if (posStr.contains("5e")){
                    posStr = posStr.substring(3, posStr.length());
                    posStr = String.valueOf( matureStart - Integer.valueOf(posStr) );
                }
                String retrievedSNPstr = snpMatcher.matching(mappedPrecursorName + "_"+posStr); // querying
                if (!retrievedSNPstr.equals("NA")) {
                    snpArray.add(retrievedSNPstr);
                }
            }
            if (snpArray.size() == 0) {
                isnpInfoStr = "NA";
            } else {
                isnpInfoStr = StringUtils.join(snpArray, " ");
            }
        }
        return isnpInfoStr;
    }

    private String findSeedChange(String isoformInfo) {
        String changeInSeed = "outSeed";
        for(String ichange: isoformInfo.split(";")){
            if(ichange.startsWith("5e") | ichange.startsWith("5d") ){
                /*
                any modification occur at 5' end will change seed sequence
                */
                changeInSeed = "seed";
            }
            if(ichange.endsWith("p")){
                int polyPos = Integer.valueOf(ichange.replace("p", ""));
                if(polyPos>=2 & polyPos<=8){
                    /*
                    if any polymorphism detect between 2 to 8 position,
                    it will change seed region sequence
                    */
                    changeInSeed = "seed";
                }
            }
        }
        return changeInSeed;
    }
    
}
