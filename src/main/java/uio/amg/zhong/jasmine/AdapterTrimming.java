/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
class AdapterTrimming extends JASMINEstep implements JASMINEbase{
    Logger logger = LogManager.getRootLogger();
    private String TASK_NAME = "Adapter Trimming";
    private String TASK_FOLDER = "TrimmedReads";
    private String ADAPTER_PATH;
    private String RAW_INPUT_EXTENSION =".fastq.gz";
    private String OUTFILE_EXTENSION = ".trim.fastq";
    private String NTHREADS;
    private String OUTPUTFOLDER;
    private String projectID;
    private String inputFolder_PATH ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder ;
    private String phredScore;
    private String TrimmingMismatches;
    private String TrimmingAlignScore;
    private String FASTQC_PATH;
    private String MULTIQC_PATH;
    private String TRIMMER_PATH;
    private String ADAPTER_FASTA;
    private String TRIMMER_CONF;
    private String TRIMMER_NAME; 
    private boolean Is_adapterSeq; 
    private HashMap<String, String> CONFIG_HASH;

    AdapterTrimming(HashMap<String, String> CONFIGHASH) throws IOException, InterruptedException {
        CONFIG_HASH = CONFIGHASH;
        setConfigurations(CONFIG_HASH);
        executeThisStep();
        doFASTQC();
       
    }

    @Override
    public void verifyInputData() throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void verifyOutputData() throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfigurations(HashMap<String, String> CONFIG_HASH)  throws IOException{
        logger.info("Configuring for adapter trimming");
        
        this.TRIMMER_PATH = CONFIG_HASH.get("TRIMMER_PATH");
        this.TRIMMER_CONF = CONFIG_HASH.get("TRIMMER_CONF");
        this.TRIMMER_NAME = CONFIG_HASH.get("TRIMMER_NAME"); 
        logger.info("You are using " +TRIMMER_NAME + " for adapter trimming");
        Is_adapterSeq = Is_Adapter_SeqStr(CONFIG_HASH.get("ADAPTER"));
        if(Is_adapterSeq){
            this.ADAPTER_FASTA = CONFIG_HASH.get("ADAPTER"); 
        }
        else{
            this.ADAPTER_PATH = CONFIG_HASH.get("ADAPTER"); 
        }
        
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.NTHREADS = CONFIG_HASH.get("THREADS");        
        JASMINEutility jasmineUtility = new JASMINEutility();
        if(CONFIG_HASH.get("DATASET_ARRAY_STR").equals("NA")){
            this.datasetArray = jasmineUtility.getDatasetList(CONFIG_HASH.get("DATASETLIST"));
        }
        else{
            this.datasetArray = new ArrayList<String>(Arrays.asList(CONFIG_HASH.get("DATASET_ARRAY_STR").split("#"))) ;
        }
        
        this.phredScore = "33";
        
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        }
        else{
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER") +FILESEPARATOR;
        }   
        if (new File(CONFIG_HASH.get("INPUTFOLDER")).isDirectory()) {
            this.inputFolder_PATH = CONFIG_HASH.get("INPUTFOLDER");
        }
        else{
            this.inputFolder_PATH = projectFolder + CONFIG_HASH.get("INPUTFOLDER") +FILESEPARATOR;
        }
        this.OUTPUTFOLDER = projectFolder +TASK_FOLDER +FILESEPARATOR;

        this.TrimmingMismatches = "2";
        this.TrimmingAlignScore = "7";
        
        String[] namesplitted = datasetArray.get(0).split("\\.");
        if(namesplitted[namesplitted.length -1].equals("gz")){
            this.RAW_INPUT_EXTENSION =  "."+namesplitted[namesplitted.length -2]+"."+namesplitted[namesplitted.length -1];
        }
        else{
            this.RAW_INPUT_EXTENSION = "."+namesplitted[namesplitted.length -1];
        }
        
        FASTQC_PATH = CONFIG_HASH.get("FASTQC");
        MULTIQC_PATH = CONFIG_HASH.get("MULTIQC");
    }

    @Override
    public void executeThisStep() throws IOException {
        logger.info("Executing adapter trimming step");        
        String cmdTrimAdapters = "";
        for (String idataset: datasetArray){
            try{
                ArrayList<String> cmd = new ArrayList<>();
                
                if(TRIMMER_NAME.toLowerCase().equals("trimmomatic") ){
                    if(Is_adapterSeq){
                        logger.info("trimmomatic require adapter sequence in FASTA format!");
                    }
                    else {
                        cmd.add("java -jar");
                        cmd.add(TRIMMER_PATH);
                        cmd.add("SE");
                        cmd.add("-threads " + NTHREADS);
                        /**
                         * Illumina 1.8, the quality scores have basically
                         * returned to the use of the Sanger format (Phred+33).
                         * for detail check here :
                         * https://en.wikipedia.org/wiki/FASTQ_format
                         * https://en.wikipedia.org/wiki/Phred_quality_score
                         *
                         * so change cmd.add("-phred33"); ---->
                         * cmd.add("-phred"+ID_PHRED_SCORE);
                         */
                        cmd.add("-phred" + phredScore);
                        cmd.add(inputFolder_PATH + FILESEPARATOR + idataset);
                        Boolean f = new File(OUTPUTFOLDER).mkdir();
                        if (f) {
                            logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
                        }
                        cmd.add(OUTPUTFOLDER + FILESEPARATOR + idataset.replace(RAW_INPUT_EXTENSION, OUTFILE_EXTENSION));
                        cmd.add("ILLUMINACLIP:" + ADAPTER_PATH
                                + ":" + TrimmingMismatches + ":30"
                                + ":" + TrimmingAlignScore);
                        cmdTrimAdapters = this.cleanPath(StringUtils.join(cmd, " "));
                        logger.info("Adapter Trim command:\t" + cmdTrimAdapters);

                        Runtime rt = Runtime.getRuntime();
                        Process proc = rt.exec(cmdTrimAdapters);
                        BufferedReader brStdin = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                        BufferedReader brStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
                        //cmd.add("AVGQUAL:" + this.getMinAvgReadQuality());
                        /*
                            java -jar Trimmomatic-0.33/trimmomatic-0.33.jar 
                            SE 
                            -phred64	
                            <input file fastq file>
                            <trimmed output fastq file> 	
                            ILLUMINACLIP:Trimmomatic-0.33/adapters/TruSeqE-SE.fa:2:30:7 2:30:7 (Mismatches:N/A:Alignment Score)
                            AVGQUAL (drop read if average quality is below this threshold)
                            -trimlog (write log)
                            -threads
                         */
                        String line = null;
                        logger.info("<OUTPUT>");
                        while ((line = brStdin.readLine()) != null) {
                            logger.info(line);
                        }
                        logger.info("</OUTPUT>");
                        logger.info("<ERROR>");
                        int totalInputReadCount = 0;
                        int survivingReadCount = 0;
                        while ((line = brStdErr.readLine()) != null) {
                            String tmplineString = line;
                            if (tmplineString.contains("Input")) {
                                totalInputReadCount = Integer.parseInt(tmplineString.split(":")[1].trim().split(" ")[0]);
                                survivingReadCount = Integer.parseInt(tmplineString.split(":")[2].trim().split(" ")[0]);
                                logger.info(totalInputReadCount + "\t" + survivingReadCount);
                            }
                            logger.info(line);
                        }
                        String allCountsFile = projectFolder + projectID + ".rawTotalReadCount.tsv";
                        // write to file by APPEND 
                        BufferedWriter bwAc = new BufferedWriter(new FileWriter(new File(allCountsFile), true));
                        bwAc.write(idataset.replace(RAW_INPUT_EXTENSION, "") + "\t" + totalInputReadCount + "\t" + survivingReadCount + "\n");
                        bwAc.close();
                        logger.info("</ERROR>");
                        int exitVal = proc.waitFor();
                        logger.info("Process exitValue: " + exitVal);
                        brStdin.close();
                        brStdErr.close();                     
                    }
                }
                else if (TRIMMER_NAME.toLowerCase().equals("cutadapt")) {
                    // sth for cutadapt
                    
                    if(Is_adapterSeq){
                        cmd.add(TRIMMER_PATH );
                        cmd.add("-a" );
                        cmd.add(ADAPTER_FASTA);
                        cmd.add("-o");
                        cmd.add(OUTPUTFOLDER + FILESEPARATOR + idataset.replace(RAW_INPUT_EXTENSION, OUTFILE_EXTENSION));

                        cmd.add(inputFolder_PATH + FILESEPARATOR + idataset);
                        cmdTrimAdapters = this.cleanPath(StringUtils.join(cmd, " "));
                        logger.info("Adapter Trimming command:\t" + cmdTrimAdapters);

                        Runtime rt = Runtime.getRuntime();
                        Process proc = rt.exec(cmdTrimAdapters);
                    }
                    else{
                        logger.info("cutadapt take adapter sequence as direct input, NO FASTA");
                    }
                    
                } 
                else {
                    String customizedTrimmerCmd = TRIMMER_CONF;
                    customizedTrimmerCmd = customizedTrimmerCmd.replace("INPUT", inputFolder_PATH + FILESEPARATOR + idataset);
                    Boolean f = new File(OUTPUTFOLDER).mkdir();
                    if (f) {
                        logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
                    }
                    customizedTrimmerCmd = customizedTrimmerCmd.replace("OUTPUT", (OUTPUTFOLDER + FILESEPARATOR + idataset.replace(RAW_INPUT_EXTENSION, OUTFILE_EXTENSION)));
                    Runtime rt = Runtime.getRuntime();
                    Process proc = rt.exec(customizedTrimmerCmd);
                }
    
            }
            catch(IOException|InterruptedException ex){
                logger.error("error executing AdapterTrimming command\n" + ex.toString());
                throw new IOException(TASK_FOLDER + "error executing AdapterTrimming command " + cmdTrimAdapters);
            }
        }
        logger.info(TASK_FOLDER + ": completed");
    }
    
    private  ArrayList<String> getDatasetList(String dataLayoutFilename) throws IOException {
        ArrayList datasetArrayList = new ArrayList();
        try{
            logger.info("reading data layout file <" +  dataLayoutFilename + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(dataLayoutFilename)));
            String lineFA = null;
            while ((lineFA = brFA.readLine())!=null){
                if(lineFA.startsWith("File")) continue;
                String datasetString = lineFA.trim().split("\t")[0];
                datasetArrayList.add(datasetString);
            }
            brFA.close();
            logger.info("read " + datasetArrayList.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading data layout file <" +  dataLayoutFilename + ">\n" + ex.toString());
            throw new IOException("error reading data layout file <" +  dataLayoutFilename + ">");
        }
        return datasetArrayList;
    }
    
    private String cleanPath(String pathstring) {
        return pathstring.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
    }

    /**
     * do FASTQC and multiQC before collapse reads and mapping
     * @throws IOException 
     */
    private void doFASTQC() throws IOException, InterruptedException {
        /*
        do FASTQC
        */
        String fastqcCmdStr= "";
        String QCreportFolder = projectFolder +"QCreports" +FILESEPARATOR;
        Boolean fA = new File(QCreportFolder).mkdir();
        if (fA) {
            logger.info("created output folder <" + QCreportFolder + "> for results");
        }

        try{
            logger.info("performing FastQC analysis on trimmed reads...");
            ArrayList fastqcCmdArray = new ArrayList<>(); 
            fastqcCmdArray.add(FASTQC_PATH);
            fastqcCmdArray.add("--extract");
            fastqcCmdArray.add("-f fastq");
            fastqcCmdArray.add(" -t  " + NTHREADS);
            fastqcCmdArray.add( OUTPUTFOLDER+"*.fastq  ");
            fastqcCmdArray.add(" -o " + QCreportFolder);  
            fastqcCmdStr = StringUtils.join(fastqcCmdArray, " "); 

            logger.info("command for fastqc:\n");
            logger.info(fastqcCmdStr +"\n");
            Runtime rt = Runtime.getRuntime();
            Process procFQC = rt.exec(fastqcCmdStr);

            BufferedReader brStdin = new BufferedReader(new InputStreamReader(procFQC.getInputStream()));
            BufferedReader brStdErr = new BufferedReader(new InputStreamReader(procFQC.getErrorStream()));
            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brStdin.readLine()) != null) {
                logger.info(line);
            }
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ((line = brStdErr.readLine()) != null) {
                logger.info(line);
            }
        } catch(IOException ex){
            logger.error("error executing FastQC command\n" + ex.toString());
            throw new IOException(TASK_FOLDER + "error executing FastQC command " 
                    + fastqcCmdStr + "\n"
                    + ex.toString());
        }
           
        
        

        /*
        do multiQC
        */
        try{
            logger.info("performing MultiQC analysis on trimmed reads...");
            String multiQCCmdStr =  MULTIQC_PATH + "  "+ QCreportFolder; 
            logger.info("command for multiQC:\n");
            logger.info(multiQCCmdStr +"\n");
            Runtime rtTQC = Runtime.getRuntime();
            Process procMQC = rtTQC.exec(multiQCCmdStr);
            BufferedReader brStdin = new BufferedReader(new InputStreamReader(procMQC.getInputStream()));
            BufferedReader brStdErr = new BufferedReader(new InputStreamReader(procMQC.getErrorStream()));
            String line = null;
            logger.info("<OUTPUT>");
            while ((line = brStdin.readLine()) != null) {
                logger.info(line);
            }
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
             while ((line = brStdErr.readLine()) != null) {
                logger.info(line);
            }
        }
        catch(IOException ex){
            logger.error("error executing MultiQC command\n" + ex.toString());
            throw new IOException(TASK_FOLDER + "error executing MultiQC command " 
                    + fastqcCmdStr+ "\n"
                    + ex.toString());
        }
        logger.info(TASK_FOLDER + ": completed");
    }

    private boolean Is_Adapter_SeqStr(String adapterinfoStr) {
        /*
        Approach 1
        */
//        File tmpDir = new File(adapterinfoStr);
//        boolean isFile = tmpDir.exists();
//        boolean isAdapterSeq = ! isFile;
        
        /*
        Approach 2
        if adapterinfoStr contains any chatacter rather than "A", "T", "G", "C"
        then it is NOT a adapter sequence string
        */
        boolean isAdapterSeq = false;
        String[] nucleotides = {"A","T","G","C", "U"}; // also add U, just in case
        char[] chrArray = adapterinfoStr.toCharArray();
        boolean notContain = true;
        int tempi = 0;
        do {
            notContain = Arrays.stream(nucleotides).anyMatch(Character.toString(adapterinfoStr.charAt(tempi)).toUpperCase()::equals);
            tempi++;
        } while (notContain & tempi < adapterinfoStr.length());
        if(notContain){
            isAdapterSeq = true;
        }
        return isAdapterSeq;
    }

    public HashMap<String, String> updateConfig() {
        /*
        updating configuration hash, passing it to next step
        */
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        CONFIG_HASH.replace("DATASET_ARRAY_STR", StringUtils.join(datasetArray, "#"));
        return CONFIG_HASH;
    }
    
}
