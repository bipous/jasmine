/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uio.amg.zhong.utility.ReadFastaFile;
import org.apache.commons.text.similarity.*;

/**
 *
 * @author xiangfuz
 */
public class CheckReference {
    Logger logger = LogManager.getRootLogger();
    private String TASK_FOLDER = "Source";
    private String TASK_NAME = "Checking Mapping Reference";
    private String mirBasePath;
    private String mirGeneDBPath;
    private ArrayList<String> mirAmbiguousList ;
    private String textLine;
    private ArrayList<String> premirAmbiguousList ;
    private String OUTPUTFOLDER;
    private String HOST ;
    private ArrayList<String> similarMatureArray = new ArrayList<>();
    private ArrayList<String> similarHairpinArray = new ArrayList<>();
    private String RSCRIPT_PATH;
    
    private String matureInfoFile;
    private String premiRNAInfoFile;
    private String drivenInfoFile;
    private String projectFolder ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private HashMap<String, String> gff_matureInfo_Hash ;
    private HashMap<String, String> gff_hairpinInfo_Hash ;
    private HashMap<String, String> gff_mature_in_precursor_StrandHash;
    private HashMap<String, String> gff_precursor_to_mature_Hash ;
    private ArrayList<String> gffAnnotated_Mature_Array;
    private ArrayList<String> gffAnnotated_Hairpin_Array ;
    private ArrayList<String> noCoordinate_Mature_Array;
    private ArrayList<String> noCoordinate_Hairpin_Array ;
    
    private HashMap<String, String> matureSeq_fw ;
    private HashMap<String, String> matureSeq_rc ;
    private HashMap<String, String> hairpinSeq_fw ;
    private HashMap<String, String> hairpinSeq_rc ;
    private int INT_MISMATCH;
    private ArrayList<String> AMBIGUOUS_MIRS;
    private HashMap<String, String> decision_on_fastaMature;
    private HashMap<String, String> decision_on_fastaHairpin;
    private String REFERENCE_SOURCE;
    private String REFERENCE_FOLDER_PATH;

    CheckReference(HashMap<String, String> CONFIG_HASH) throws IOException, FileNotFoundException, InterruptedException {
        setConfigHash(CONFIG_HASH);
        startChecking();
    }
    
    private void setConfigHash(HashMap<String, String> CONFIG_HASH) {
        this.HOST = CONFIG_HASH.get("HOST").toLowerCase();
        
        if(CONFIG_HASH.get("MIRBASE_PATH").endsWith(FILESEPARATOR)){
            this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH");
        }
        else{
            this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH")+FILESEPARATOR;
            this.REFERENCE_SOURCE = "miRBase";
        }
        
        /*
        give up of oncluding MirGeneDB
        REASON: different miRNA nomenclature in MirGeneDB
        but plan to include it in next version of jasmine
        */
        if(CONFIG_HASH.get("MIRGENEDB_PATH").endsWith(FILESEPARATOR)){
            this.mirGeneDBPath = CONFIG_HASH.get("MIRGENEDB_PATH");
//            this.REFERENCE_SOURCE = "MirGeneDB";
        }
        else{
            this.mirGeneDBPath = CONFIG_HASH.get("MIRGENEDB_PATH")+FILESEPARATOR;
//            this.REFERENCE_SOURCE = "MirGeneDB";
        }
        
        /*
        if both aMIRBASE_PATH and MIRGENEDB_PATH are configured in xml,
        Jasmine prioritizes miRBase annotation
        */
        File tmpf1 = new File(mirBasePath);
        File tmpf2 = new File(mirGeneDBPath);
        if(tmpf1.exists() && tmpf1.isDirectory()) {
            this.REFERENCE_SOURCE = "miRBase";
            REFERENCE_FOLDER_PATH = mirBasePath;
        }
        else if(tmpf2.exists() && tmpf2.isDirectory()) {
            this.REFERENCE_SOURCE = "MirGeneDB";
            REFERENCE_FOLDER_PATH = mirGeneDBPath;
            
            /*
            Because MirGeneDB uses different file name, 
            so first will rename files
            SPECIES.fas ----> mature.fa
            SPECIES-pre.fas ----> hairpin.fa
            */
            boolean success = new File(REFERENCE_FOLDER_PATH+HOST+".fas").renameTo(new File(REFERENCE_FOLDER_PATH+"mature.fa"));
            if (!success) {
                System.out.println("Error in renaming MirGeneDB file: SPECIES.fas ----> mature.fa ");
            }
            success = new File(REFERENCE_FOLDER_PATH+HOST+"-pre.fas").renameTo(new File(REFERENCE_FOLDER_PATH+"hairpin.fa"));
            if (!success) {
                System.out.println("Error in renaming MirGeneDB file: SPECIES-pre.fas ----> hairpin.fa ");
            }
        }
        else{
            logger.error("NO valid path for miRBase or MirGeneDB found");
            System.exit(1);
        }
        
        
                
        this.RSCRIPT_PATH = CONFIG_HASH.get("RSCRIPT");
        
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        }
        else{
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER") +FILESEPARATOR;
        }   

        this.OUTPUTFOLDER = projectFolder +TASK_FOLDER +FILESEPARATOR;
        this.INT_MISMATCH = Integer.valueOf(CONFIG_HASH.get("MISMATCH"));
    }
    
    public void setMiRBasePath(String mirbasePath){ 
        this.mirBasePath = mirbasePath;
    }
    
    public ArrayList<String> getSimilarMature(){
        return similarMatureArray;
    }
    
    public ArrayList<String> getSimilarHairpin(){
        return similarHairpinArray;
    }
    
    public void startChecking() throws FileNotFoundException, IOException, InterruptedException{
        Boolean fA = new File(OUTPUTFOLDER).mkdir();       
        if (fA) logger.info("created output folder <" + OUTPUTFOLDER+ "> for results" );
        
        buildMinDistance("mature");
        buildMinDistance("hairpin");
        
        buildMiRNAparental();
        infoSimilarPremirna();

        similarMaturemirnaInfo();
        identicalPremirna();
        
//        curatingRef();
    }
    
    public ArrayList<String> getAmbiguousList() throws IOException, FileNotFoundException, InterruptedException {
        if(mirAmbiguousList.equals(null)){
           startChecking(); 
        }
        return mirAmbiguousList;
    }

    private void loadDrivenInfo() throws FileNotFoundException, IOException {
        // load derive information
        BufferedReader drivenInfoBR = new BufferedReader(new FileReader(new File(drivenInfoFile)));
        textLine = null;
        HashMap<String, String> drivenInfoHashMap = new HashMap<>();
        while ((textLine = drivenInfoBR.readLine()) != null) {
            textLine = textLine.trim();
            int multipleCount = textLine.split("\t")[1].length() - (textLine.split("\t")[1]).replace(";", "").length() +1;            
            if(textLine.split("\t")[1].contains(";")){
                mirAmbiguousList.add(textLine.split("\t")[0]);
            }
            drivenInfoHashMap.put(textLine.split("\t")[0], textLine.split("\t")[1]);
        }
        drivenInfoBR.close();

    }

    private ArrayList<String> loadMinDist(String distanceFile, int distThreshold) throws FileNotFoundException, IOException {
        /*
        load  minunum Levenshtein Distance matrix of mature miRNAs
        */
        ArrayList<String> seqNamwArray = new ArrayList<>();
        BufferedReader distanceMatrixBR = new BufferedReader(new FileReader(new File(distanceFile)));
        textLine = null;
        while ((textLine = distanceMatrixBR.readLine()) != null) {
            textLine = textLine.trim();
            if(textLine.contains(HOST)){
                String[] textlineStrs = textLine.split("\t");
                String mirnanameStr = textlineStrs[0];
//                logger.info(textLine);
                int iDistance = Integer.valueOf(textlineStrs[1]);
                if(iDistance < distThreshold){
                    seqNamwArray.add(mirnanameStr);
                }
            }
            
        }
        distanceMatrixBR.close();
        return seqNamwArray;
    }

    private void writeOutArrayList(ArrayList<String> mirAmbiguousList, String outputFilename) throws IOException {
//        String outputfile = outputFilePath+iversionString+".mirnaInfo.tsv";
        BufferedWriter outputBW = new BufferedWriter(new FileWriter(new File(outputFilename)));
        
        for (String istr : mirAmbiguousList) {
            outputBW.write(istr + "\n");
        }
        outputBW.close();
    }
    
    private void buildMinDistance(String molecularType) throws FileNotFoundException, IOException, InterruptedException{
         //load  Levenshtein Distance matrix of mature miRNAs
        String thisOutPutFolder = OUTPUTFOLDER;
          
        String distMatrixFile = thisOutPutFolder + REFERENCE_SOURCE +"."+HOST+ "-"+ molecularType+"-LevenshteinDistance.matrix.tsv";
              
        if(!(new File(distMatrixFile)).exists()){            
            /*
            first read in fasta file, and turn it into text format
            */
            logger.info("--building distMatrixFile");
            String thisFastaFile = REFERENCE_FOLDER_PATH +FILESEPARATOR+ molecularType+".fa";
            ReadFastaFile readFasta = new ReadFastaFile(thisFastaFile);
            logger.info(HOST);
            readFasta.readFasta(thisFastaFile,HOST);
            String seqTextFile = thisOutPutFolder+ REFERENCE_SOURCE +"."+HOST+ "-"+ molecularType+"-seqText.tsv";
            logger.info("--seqTextFile is <" + seqTextFile + ">");
            logger.info("----writing seqTextFile");
            writeOutArrayList(readFasta.getSequenceText(), seqTextFile);
            logger.info("--done");

            /*
            build r script
            */
            logger.info("--building Rscript");
            ArrayList<String> rScriptSet = new ArrayList<>();
            rScriptSet.add("#");
            rScriptSet.add("#   Rscript generated for project to build a Levenshtein Distance matrix of mature miRNAs or pre-miRNAs");
            rScriptSet.add("#   created: " + new Date());
            rScriptSet.add("#");
            rScriptSet.add("#");
            rScriptSet.add("fastaseq <- read.delim(\"" + seqTextFile +"\", sep='\\t',header=FALSE)");
            rScriptSet.add("colnames(fastaseq) <- c(\"mirnaName\",\"seqstr\")");
            rScriptSet.add("rownames(fastaseq) <- fastaseq$mirnaName");
            rScriptSet.add("fastaseq.dist  <- adist(fastaseq$seqstr)");
            rScriptSet.add("rownames(fastaseq.dist )  <- fastaseq$mirnaName");
            rScriptSet.add("colnames(fastaseq.dist )  <- fastaseq$mirnaName");
            rScriptSet.add("write.table(fastaseq.dist, file = \"" + distMatrixFile+"\",row.names=TRUE, na=\"\", quote=FALSE, col.names=TRUE, sep=\"\\t\")");
            rScriptSet.add("");
        
            String rscriptFile = thisOutPutFolder +molecularType +"."+REFERENCE_SOURCE +".LevenshteinDistance.R";
            logger.info("--rscriptFile is <" + rscriptFile + ">");
            logger.info("----writing rscriptFile");
            try{
                BufferedWriter bwRC = new BufferedWriter(new FileWriter(new File(rscriptFile)));
                    for(String cmd: rScriptSet){
                        bwRC.write(cmd + "\n");
                    }
                bwRC.close();
            }
            catch(IOException exIO){
                logger.error("error writing generated RScript to file <" + rscriptFile + ">");
                logger.error(exIO);
                throw new IOException(TASK_NAME + ": error writing generated RScript to file <" + rscriptFile + ">");
            }
            logger.info("--done");

            /*
            run r script to generate Levenshtein Distance matrix
            */
            ArrayList<String> cmd = new ArrayList<>();
            cmd.add(RSCRIPT_PATH);  
            cmd.add(rscriptFile);
            String cmdRunRScript = StringUtils.join(cmd, " ");
            logger.info("--Rscript command:\t" + cmdRunRScript);
            logger.info("----executing ");
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmdRunRScript);
            proc.waitFor();
            logger.info("--finished Rscript command");
        }
        else{
            logger.info("--distMatrixFile exists");
        }
        
        /*
        build minimum distance table 
        */
        logger.info("--building minimum distance table");
        logger.info("----read distMatrixFile");
        BufferedReader distanceMatrixBR = new BufferedReader(new FileReader(new File(distMatrixFile)));
        textLine = null;
        ArrayList<String> minDist = new ArrayList<>();
        int rowIndex = 0;
        while ((textLine = distanceMatrixBR.readLine()) != null) {
            textLine = textLine.trim();
            if(! textLine.split("\t")[2].contains("-")){
                String[] textlineStrs = textLine.split("\t");
                String iMirnaStr = textlineStrs[0];
                int minDistance = 22;
                for(int colIndex=1; colIndex< (textlineStrs.length); colIndex ++){
                    if(colIndex != (rowIndex+1)){
                        minDistance = Integer.min(minDistance, Integer.valueOf(textlineStrs[colIndex]));
                    }
                }
                minDist.add(iMirnaStr+"\t"+String.valueOf(minDistance)+"\n");
                rowIndex ++;
            }
        }
        distanceMatrixBR.close();
        logger.info("----done");
        /*
        write minimum distance table into file
        */
        String minDistanceFile = thisOutPutFolder + REFERENCE_SOURCE +"."+HOST+ "-"+ molecularType+"-minLDist.tsv";
        logger.info("--minDistanceFile is <" + minDistanceFile + ">");
        logger.info("--writing minDistanceFile");
        BufferedWriter outputBW = new BufferedWriter(new FileWriter(new File(minDistanceFile)));
        for (String istr : minDist) {
            outputBW.write(istr);
        }
        outputBW.write("\n");
        outputBW.close();
        logger.info("--done");
        logger.info("finished\n\n");

    }

    public void setOutputFolder(String outoutfolder) {
        this.OUTPUTFOLDER = outoutfolder; 
    }
    
    public void setHost(String ihostStr ){
        this.HOST = ihostStr;
    }

    private void infoSimilarPremirna() throws IOException {
        /*
         this is to build information table for mature miRNAs and pre-miRNAs
        */
        String matureMinDistFile = OUTPUTFOLDER + REFERENCE_SOURCE +"."+HOST+ "-mature-minLDist.tsv";
        String hairpinMinDistFile = OUTPUTFOLDER + REFERENCE_SOURCE +"."+HOST+ "-hairpin-minLDist.tsv";
        similarMatureArray = loadMinDist(matureMinDistFile, 3);
        similarHairpinArray = loadMinDist(hairpinMinDistFile, 3);
        
        /*
        parse gff file, for now, this only work for gff3 format rather than gff
        by default, jasmine will first search the gff file according miRBase folder system, 
        which means, first search gff file in the "/genome" subfolder
        */
        String gffFilepath = REFERENCE_FOLDER_PATH+"genomes"+FILESEPARATOR+HOST+".gff3";
        File tmpGffFile = new File(gffFilepath);
        BufferedReader gffBR;
        
        if(! tmpGffFile.exists() ){
            tmpGffFile = new File(REFERENCE_FOLDER_PATH+HOST+".gff3");
        }
        else if(! tmpGffFile.exists() ){
            tmpGffFile = new File(REFERENCE_FOLDER_PATH+HOST+".gff");
        }
        else if( tmpGffFile.exists() ){
            try{
                logger.info("GFF file found to use: " + tmpGffFile.toString());
                gffBR = new BufferedReader(new FileReader(tmpGffFile));
                String gffLine = null;
                ArrayList<String> miRNArrayList = new ArrayList<>();
                ArrayList<String> premiRNArrayList = new ArrayList<>();
                HashMap<String, String> matureHashMap = new HashMap<>();
                HashMap<String, String> premirnaHashMap = new HashMap<>();

                while ((gffLine = gffBR.readLine()) != null) {
                    if (gffLine.startsWith("#")) {
                        continue;
                    }
                    gffLine = gffLine.trim();
                    String[] gfflineStrings = gffLine.split("\t");

                    if (gfflineStrings[2].equals("miRNA")) {
                        String[] nameIDinfo = gfflineStrings[8].split(";");
                        String mirnaID = nameIDinfo[0].split("=")[1];
                        String mirnaAlias = nameIDinfo[1].split("=")[1];
                        String mirnaName = nameIDinfo[2].split("=")[1];
                        String mirnaDerives = nameIDinfo[3].split("=")[1];

                        String tmpMirnaStr = mirnaID + "\t"
                                + mirnaAlias + "\t"
                                + mirnaName + "\t"
                                + mirnaDerives + "\t"
                                + gfflineStrings[0] + "\t"
                                + gfflineStrings[3] + "\t"
                                + gfflineStrings[4] + "\t"
                                + gfflineStrings[6];

                        miRNArrayList.add(tmpMirnaStr);

                        if (matureHashMap.containsKey(mirnaName)) {
                            matureHashMap.replace(mirnaName, (matureHashMap.get(mirnaName) + ";" + mirnaDerives));
                        } 
                        else {
                            matureHashMap.put(mirnaName, mirnaDerives);
                        }

                    } 
                    else if (gfflineStrings[2].equals("miRNA_primary_transcript")) {
                        String[] nameIDinfo = gfflineStrings[8].split(";");
                        String premirnaID = nameIDinfo[0].split("=")[1];
                        String premirnaAlias = nameIDinfo[1].split("=")[1];
                        String premirnaName = nameIDinfo[2].split("=")[1];

                        String tmpMirnaStr = premirnaID + "\t"
                                + premirnaAlias + "\t"
                                + premirnaName + "\t"
                                + gfflineStrings[0] + "\t"
                                + gfflineStrings[3] + "\t"
                                + gfflineStrings[4] + "\t"
                                + gfflineStrings[6];
                        premiRNArrayList.add(tmpMirnaStr);
                    }
                }
            }
            catch (FileNotFoundException fileEx) {
                fileEx.printStackTrace();
            } catch (IOException ioEx) {
               ioEx.printStackTrace();
            }
        }
        else{
            logger.error(tmpGffFile.toString());
            logger.error("NO GFF FILE FOUND FOR THE SPECIES:"+HOST);
            
        }

          
    }

    private void similarMaturemirnaInfo() throws FileNotFoundException, IOException {
        logger.info("similarMaturemirnaInfo");
        BufferedReader drivenInfoBR = new BufferedReader(new FileReader(new File(drivenInfoFile)));
        logger.info("--output drivenInfoFile is <" + drivenInfoFile + ">");
        textLine = null;
        HashMap<String, String> drivemInfoHash = new HashMap<>();
        int rowIndex = 0;
        logger.info("--reading");
        while ((textLine = drivenInfoBR.readLine()) != null) {
            textLine = textLine.trim();
            drivemInfoHash.put(textLine.split("\t")[0], textLine.split("\t")[1]);
        }
        drivenInfoBR.close();
        logger.info("--done");

        BufferedReader matureInfoBR = new BufferedReader(new FileReader(new File(matureInfoFile)));
        logger.info("--output matureInfoFile is <" + matureInfoFile + ">");
        textLine = null;
        logger.info("--reading");
        ArrayList<String> matureInfo = new ArrayList<>();
        while ((textLine = matureInfoBR.readLine()) != null) {
            textLine = textLine.trim();
            if(!textLine.startsWith("miRNA")){
                matureInfo.add(textLine+"\t"+drivemInfoHash.get(textLine.split("\t")[0]));
            } 
        }
        matureInfoBR.close();
        logger.info("--done");
        
        logger.info("--write matureInfo to <" + OUTPUTFOLDER + "miRBase" +"."+HOST+".simimarEntry-info.tsv" + ">");
        writeOutArrayList(matureInfo, OUTPUTFOLDER + "miRBase" +"."+HOST+".simimarEntry-info.tsv");
        logger.info("--done");
        logger.info("finished");
    }

    private void identicalPremirna() throws IOException {
        logger.info("identicalPremirna");
        String minHairpinDistFile = projectFolder+"Source/miRBase"+"."+HOST+"-hairpin-minLDist.tsv";
        String hairpinFastaFile = mirBasePath+FILESEPARATOR+"hairpin.fa";
        logger.info("--hairpinFastaFile is <" + mirBasePath+FILESEPARATOR+"hairpin.fa" + ">");
        logger.info("--reading hairpinFastaFile");
        ReadFastaFile readFasta = new ReadFastaFile();
        readFasta.readFasta(hairpinFastaFile,HOST);
        logger.info("--done");
        /*
        parse gff file, for now, this only work for gff3 format rather than gff
        */
        String gffFile = mirBasePath+FILESEPARATOR+"genomes"+FILESEPARATOR+HOST+".gff3";
        logger.info("--gffFile is <" + gffFile + ">");
        logger.info("--reading gffFile");
        BufferedReader gffBR = new BufferedReader(new FileReader(new File(gffFile)));
        String gffLine = null;
        ArrayList<String> miRNArrayList = new ArrayList<>();
        ArrayList<String> premiRNArrayList = new ArrayList<>();
        HashMap<String, String> matureHashMap = new HashMap<>();
        HashMap<String, String> premirnaHashMap = new HashMap<>();
                    
        while ((gffLine = gffBR.readLine()) != null) {
            if (gffLine.startsWith("#")) {
                continue;
            }
            gffLine = gffLine.trim();
            String[] gfflineStrings = gffLine.split("\t");
            if (gfflineStrings[2].equals("miRNA")) {
                String[] nameIDinfo = gfflineStrings[8].split(";");
                String mirnaID = nameIDinfo[0].split("=")[1];
                String mirnaAlias = nameIDinfo[1].split("=")[1];
                String mirnaName = nameIDinfo[2].split("=")[1];
                String mirnaDerives = nameIDinfo[3].split("=")[1];
                String tmpMirnaStr = mirnaID + "\t"
                        + mirnaAlias + "\t"
                        + mirnaName + "\t"
                        + mirnaDerives + "\t"
                        + gfflineStrings[0] + "\t"
                        + gfflineStrings[3] + "\t"
                        + gfflineStrings[4] + "\t"
                        + gfflineStrings[6];
                miRNArrayList.add(tmpMirnaStr);
                if (matureHashMap.containsKey(mirnaName)) {
                    matureHashMap.replace(mirnaName, (matureHashMap.get(mirnaName) + ";" + mirnaDerives));
                } 
                else {
                    matureHashMap.put(mirnaName, mirnaDerives);
                }
            } 
            else if (gfflineStrings[2].equals("miRNA_primary_transcript")) {
                String[] nameIDinfo = gfflineStrings[8].split(";");
                String premirnaID = nameIDinfo[0].split("=")[1];
                String premirnaAlias = nameIDinfo[1].split("=")[1];
                String premirnaName = nameIDinfo[2].split("=")[1];
                String chromoStr = gfflineStrings[0];
                String startPos = gfflineStrings[3];
                String endPos = gfflineStrings[4];
                String strandStr = gfflineStrings[6];
                String preinfoStr = chromoStr +"\t"+ strandStr +"\t"+ startPos +"\t"+ endPos;
                String tmpMirnaStr = premirnaID + "\t"
                        + premirnaAlias + "\t"
                        + premirnaName + "\t"
                        + gfflineStrings[0] + "\t"
                        + gfflineStrings[3] + "\t"
                        + gfflineStrings[4] + "\t"
                        + gfflineStrings[6];
                premiRNArrayList.add(tmpMirnaStr);

                premirnaHashMap.put(premirnaName, preinfoStr);
            }
        }
        logger.info("--done");

        logger.info("--minHairpinDistFile is <" + projectFolder+"Source/miRBase"+"."+HOST+"-hairpin-minLDist.tsv" + ">");
        logger.info("--reading file");
        BufferedReader minDistBR = new BufferedReader(new FileReader(new File(minHairpinDistFile)));
        textLine = null;
        ArrayList<String> seqinfo = new ArrayList<>();
        HashMap<String, String > hairpinNameSeqHash = readFasta.getSeqStrNameHash();
        while ((textLine = minDistBR.readLine()) != null) {
            textLine = textLine.trim();
            seqinfo.add(textLine +"\t"+ hairpinNameSeqHash.get(textLine.split("\t")[0]) +"\t"+ premirnaHashMap.get(textLine.split("\t")[0]));
        }
        minDistBR.close();
        logger.info("--done");
        
        String matureMinDistFile = OUTPUTFOLDER + "miRBase"+"."+HOST + "-mature-LevenshteinDistance.matrix.tsv";
        logger.info("--matureMinDistFile is <" + matureMinDistFile + ">");
        BufferedReader matureMinDistBR = new BufferedReader(new FileReader(new File(matureMinDistFile)));
        textLine = null;
//        ArrayList<String> seqinfo = new ArrayList<>();
        logger.info("--reading file");
        HashMap<String, String > matureMinDistHash = readFasta.getSeqStrNameHash();
        while ((textLine = matureMinDistBR.readLine()) != null) {
            textLine = textLine.trim();
            matureMinDistHash.put(textLine.split("\t")[0], textLine.split("\t")[1]);
//            seqinfo.add(textLine +"\t"+ hairpinNameSeqHash.get() +"\t"+ premirnaHashMap.get(textLine.split("\t")[0]));
        }
        matureMinDistBR.close();
        logger.info("--done");
        logger.info("--write out seqinfo to <" + OUTPUTFOLDER + "miRBase"+"."+HOST +".similar-premirna.tsv" + ">");
        writeOutArrayList(seqinfo, (OUTPUTFOLDER + "miRBase"+"."+HOST +".similar-premirna.tsv"));
        logger.info("--done");
        /*
        
        */
//        String drivenInfoFile = OUTPUTFOLDER + "miRBase"+mirBaseVersion+"."+HOST + ".derivesInfo.tsv";
        logger.info("--drivenInfoFile is <" + OUTPUTFOLDER + "miRBase"+"."+HOST +".similar-premirna.tsv" + ">");
        logger.info("--reading file");
        String drivenInfoFile =  projectFolder+"Source/miRBase"+"."+HOST +".derivesInfo.tsv";
        BufferedReader drivenInfoBR = new BufferedReader(new FileReader(new File(drivenInfoFile)));
        textLine = null;
        ArrayList<String> multipleLociArray = new ArrayList<>();
        while ((textLine = drivenInfoBR.readLine()) != null) {
            textLine = textLine.trim();
            if(textLine.contains(";")){
                String[] premirnalist = textLine.split("\t")[1].split(";");
                for (String ipremirna:premirnalist ){
                    multipleLociArray.add(textLine +"\t"+ ipremirna +"\t"+ matureMinDistHash.get(textLine.split("\t")[0])+"\t"+premirnaHashMap.get(ipremirna) +"\t"+ hairpinNameSeqHash.get(ipremirna) );
                }
                multipleLociArray.add("\n");
            }
        }
        drivenInfoBR.close();
        logger.info("--done");
        logger.info("--write out multipleLociArray to <" + OUTPUTFOLDER + "miRBase"+"."+HOST +".multipleLoci-info.tsv" + ">");
        writeOutArrayList(multipleLociArray, OUTPUTFOLDER + "miRBase"+"."+HOST +".multipleLoci-info.tsv");            
        logger.info("--done");
        logger.info("finished");
    }

    /*
    this is to build a table of the relationship between miRNA and pre-miRNAs
    */
    public void buildMiRNAparental() throws FileNotFoundException, IOException{
        try{
            logger.info("buildMiRNAparental");
            String inputFile = mirBasePath+FILESEPARATOR+"genomes"+FILESEPARATOR+HOST+".gff3";
            String outputFilePath = OUTPUTFOLDER;
            logger.info("--outputFilePath is <" + outputFilePath + ">");
            logger.info("--inputFile is <" + inputFile + ">");

            BufferedReader gffBR = new BufferedReader(new FileReader(new File(inputFile)));
            ArrayList<String> miRNArrayList = new ArrayList<>();
            ArrayList<String> premiRNArrayList = new ArrayList<>();
            HashMap<String, String> matureHashMap = new HashMap<>();
            HashMap<String, String> premirnaHashMap = new HashMap<>();

            logger.info("----reading file");
        
            String  gffLine = null;
            while((gffLine=gffBR.readLine())!= null){
                if(gffLine.startsWith("#")) continue;
                gffLine = gffLine.trim();
                String[] gfflineStrings = gffLine.split("\t");
                if(gfflineStrings[2].equals("miRNA")){
                    String[] nameIDinfo = gfflineStrings[8].split(";");
                    String mirnaID = nameIDinfo[0].split("=")[1];
                    String mirnaAlias = nameIDinfo[1].split("=")[1];
                    String mirnaName = nameIDinfo[2].split("=")[1];
                    String mirnaDerives = nameIDinfo[3].split("=")[1];
                    String tmpMirnaStr = mirnaID +"\t"+ 
                            mirnaAlias +"\t"+ 
                            mirnaName +"\t"+ 
                            mirnaDerives +"\t"+
                            gfflineStrings[0] +"\t"+
                            gfflineStrings[3] +"\t"+
                            gfflineStrings[4] +"\t"+
                            gfflineStrings[6]; 
                    miRNArrayList.add(tmpMirnaStr);
                    if(matureHashMap.containsKey(mirnaName)){
                        matureHashMap.replace(mirnaName, (matureHashMap.get(mirnaName) +";"+mirnaDerives));
                    }
                    else{
                        matureHashMap.put(mirnaName, mirnaDerives);
                    } 
                }
                else if(gfflineStrings[2].equals("miRNA_primary_transcript")){
                    String[] nameIDinfo = gfflineStrings[8].split(";");
                    String premirnaID = nameIDinfo[0].split("=")[1];
                    String premirnaAlias = nameIDinfo[1].split("=")[1];
                    String premirnaName = nameIDinfo[2].split("=")[1];
                    String tmpMirnaStr = premirnaID +"\t"+ 
                            premirnaAlias +"\t"+ 
                            premirnaName +"\t"+ 
                            gfflineStrings[0] +"\t"+
                            gfflineStrings[3] +"\t"+
                            gfflineStrings[4] +"\t"+
                            gfflineStrings[6]; 
                    premiRNArrayList.add(tmpMirnaStr);
                    premirnaHashMap.put(premirnaID, premirnaName);
                }
            }
            logger.info("--done");        
            logger.info("--parsing hashMap");  
            HashMap<String, String> matureHashMapNEW = new HashMap<>();
            for (Map.Entry<String, String> matureEntry : matureHashMap.entrySet()) {
                String ikey = matureEntry.getKey();
                String ivalue = matureEntry.getValue();
                if(ivalue.contains(";")){
                    String[] preIDs = ivalue.split(";");
                    String prename = "";
                    for(String ipreid: preIDs){
                        prename = prename +premirnaHashMap.get(ipreid) + ";" ;
                    }
                    matureHashMapNEW.put(ikey, prename.substring(0, (prename.length()-1)));
                }
                else{
                    matureHashMapNEW.put(ikey, premirnaHashMap.get(ivalue));
                }
            }
            logger.info("--done"); 
            
            /*
            write out mature information
            */
            matureInfoFile = outputFilePath+"miRBase"+"."+HOST+".matureInfo.tsv";
            BufferedWriter matureinfoBW  = new BufferedWriter(new FileWriter(new File(matureInfoFile)));
            logger.info("--matureInfoFile is <" + matureInfoFile + ">");
            logger.info("--writing"); 
            for(String istr : miRNArrayList){
                matureinfoBW.write(istr+ "\n");
            }
            matureinfoBW.close();
            logger.info("--done");
            
            /*
            write out pre-miRNA information
            */
            premiRNAInfoFile= outputFilePath+"miRBase"+"."+HOST+".premiRNAInfo.tsv";
            BufferedWriter premirnainfoBW  = new BufferedWriter(new FileWriter(new File(premiRNAInfoFile)));
            for(String istr : premiRNArrayList){
                premirnainfoBW.write(istr+ "\n");
            }
            premirnainfoBW.close();
            logger.info("--done");
            
            /*
            write out mature miRNA and pre-miRNA relationship information
            */
            drivenInfoFile = outputFilePath+"miRBase"+"."+HOST+".derivesInfo.tsv";
            BufferedWriter derivesBW  = new BufferedWriter(new FileWriter(new File(drivenInfoFile)));
            logger.info("--premiRNAInfoFile is <" + premiRNAInfoFile + ">");
            logger.info("--writing");
            for (Map.Entry<String, String> entry : matureHashMapNEW.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                derivesBW.write(key +"\t"+ value + "\n");            
            }
            derivesBW.close();
            logger.info("--done");
            
            /*
            write out mature miRNA and pre-miRNA relationship information
            in a Levenshtein Distance ktable format
            */
            String drivenLevenshteinDistanceFile = outputFilePath+"miRBase"+"."+HOST+".gffInLD.tsv";
            BufferedWriter derivesLDBW  = new BufferedWriter(new FileWriter(new File(drivenLevenshteinDistanceFile)));
            logger.info("--drivenInfoFile is <" + drivenInfoFile + ">");
            logger.info("--writing");  
            for (Map.Entry<String, String> entry : matureHashMapNEW.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if(value.contains(";")){
                    for(String ipremirna : value.split(";")){
                        derivesLDBW.write(key +"\t"+ ipremirna +"\t-1\t3"+ "\n");
                    }
                }
                else{
                    derivesLDBW.write(key +"\t"+ value +"\t-1\t3"+ "\n");
                }
            }
            derivesLDBW.close();
            logger.info("--done");   
            logger.info("finished");  

        }
        catch(IOException ex){
            logger.error("Exception caught in 'buildMiRNAparental");
            logger.error(ex.toString());
            throw new IOException("Exception caught in 'buildMiRNAparental'", ex);
        }
    }

    private void curatingRef() throws IOException {
        JASMINEutility jasmineTool = new JASMINEutility();
        String releaseInforStr = getVersionInformation(mirBasePath);
        String inputFile = mirBasePath+FILESEPARATOR+"genomes"+FILESEPARATOR+HOST+".gff3";
        String hairpinFastaFile = mirBasePath+FILESEPARATOR+"hairpin.fa";
        String matureFastaFile = mirBasePath+FILESEPARATOR+"mature.fa";
        String miRstrFile = mirBasePath+FILESEPARATOR+"mature.str";
        AMBIGUOUS_MIRS = new ArrayList<>();
        /*
        parsing gff file
        */
        gff_hairpinInfo_Hash = new HashMap<>();
        gff_matureInfo_Hash = new HashMap<>();
        gffAnnotated_Mature_Array = new ArrayList<>();
        gffAnnotated_Hairpin_Array = new ArrayList<>();
        gff_mature_in_precursor_StrandHash = new HashMap<>();
        noCoordinate_Hairpin_Array = new ArrayList<>(); // a list of precursors without genomic coordinates 
        noCoordinate_Mature_Array = new ArrayList<>(); // a list of miRNAs without genomic coordinates
        ParsingGFF(inputFile);
        gff_precursor_to_mature_Hash = convertingBiogenesis(gff_mature_in_precursor_StrandHash);
        /*
        read in fasta sequence, mature and hairpin
        */
        jasmineTool.readFasta(matureFastaFile, HOST);
        HashMap<String, String> matureFastaDict = jasmineTool.getSeqStrNameHash();
        decision_on_fastaMature = initializeDecision(matureFastaDict);
        jasmineTool.readFasta(hairpinFastaFile, HOST);
        HashMap<String, String> hairpinFastaDict = jasmineTool.getSeqStrNameHash();
        decision_on_fastaHairpin = initializeDecision(hairpinFastaDict);
        int originMatureFasta = matureFastaDict.size();
        int originHairpinFasta = hairpinFastaDict.size();
        logger.info("Jasmine read in "+String.valueOf(originMatureFasta) +" mature miRNA sequences in fasta format for species "+HOST);
        logger.info("Jasmine read in "+String.valueOf(originHairpinFasta) +" pre-miRNA sequences in fasta format for species "+HOST);
        /*
        check how many miRNAs/pre-miRNAs without genomic coordinates
        */
        matureFastaDict = findingNoCoordinate(matureFastaDict, gffAnnotated_Mature_Array, true);
        hairpinFastaDict = findingNoCoordinate(hairpinFastaDict, gffAnnotated_Hairpin_Array, false);
        logger.info("Jasmine detected "+String.valueOf(originMatureFasta - matureFastaDict.size()  ) +" mature miRNA annotated without genomic coordinates in "+HOST);
        logger.info("They are: \n"+ StringUtils.join(noCoordinate_Mature_Array, "; ") +"\n");
        logger.info("Jasmine detected "+String.valueOf(originHairpinFasta - hairpinFastaDict.size() ) +" pre-miRNA annotated without genomic coordinates in "+HOST);
        logger.info("They are: \n"+ StringUtils.join(noCoordinate_Hairpin_Array, "; ") +"\n");
        originMatureFasta = matureFastaDict.size();
        originHairpinFasta = hairpinFastaDict.size();
        /*
        sorting sequence based on strand
        */
        logger.info("\nJasmine is going to sorting sequences based on strand information\n");
        matureSeq_fw = new HashMap<>();
        matureSeq_rc = new HashMap<>();
        SortingSeqByStrand(matureFastaDict,true); // sorting mature miRNA sequences
        hairpinSeq_fw = new HashMap<>();
        hairpinSeq_rc = new HashMap<>();
        SortingSeqByStrand(hairpinFastaDict,false); // sorting miRNA precursor sequences
        int count_matureSeq_fw = matureSeq_fw.size(); // calculate the number of sequences 
        int count_matureSeq_rc = matureSeq_rc.size();
        int count_hairpinSeq_fw = hairpinSeq_fw.size();
        int count_hairpinSeq_rc = hairpinSeq_rc.size();
        logger.info("In "+HOST+"\t"+String.valueOf(matureSeq_fw.size())+" miRNAs are annotated on forward strand(+);");
        logger.info("In "+HOST+"\t"+String.valueOf(matureSeq_rc.size())+" miRNAs are annotated on reverse strand(-);");
        logger.info("In "+HOST+"\t"+String.valueOf(hairpinSeq_fw.size())+" pre-miRNAs are annotated on forward strand(+);");
        logger.info("In "+HOST+"\t"+String.valueOf(hairpinSeq_rc.size())+" pre-miRNAs are annotated on reverse strand(-);");
        /*
        check identical sequences
        */
        matureSeq_fw = mergeIdenticalEntry(matureSeq_fw);
        matureSeq_rc = mergeIdenticalEntry(matureSeq_rc);
        hairpinSeq_fw = mergeIdenticalEntry(hairpinSeq_fw);
        hairpinSeq_rc = mergeIdenticalEntry(hairpinSeq_rc);
        String mergedMatureFW = getMergedMiRs(matureSeq_fw, true);  // get miRNAs name list which are merged into single entry due to identical sequences
        String mergedMatureRC = getMergedMiRs(matureSeq_rc, true); 
        String mergedHairpinFW = getMergedMiRs(hairpinSeq_fw, false); // get pre-miRNAs name list which are merged into single entry due to identical sequences
        String mergedHairpinRC = getMergedMiRs(hairpinSeq_rc, false); 
        String markder = " -- ";
        logger.info("In "+HOST+"\t"+String.valueOf(countingMergedMiRs(mergedMatureFW,markder) )+" miRNAs(+) share identical sequences;");
        logger.info("They are: \n"+ mergedMatureFW +"\n");
        logger.info("In "+HOST+"\t"+String.valueOf(countingMergedMiRs(mergedMatureRC,markder) )+" miRNAs(-) share identical sequences;");
        logger.info("They are: \n"+ mergedMatureRC +"\n");
        logger.info("In "+HOST+"\t"+String.valueOf(countingMergedMiRs(mergedHairpinFW,markder))+" pre-miRNAs(+) share identical sequences;");
        logger.info("They are: \n"+ mergedHairpinFW +"\n");
        logger.info("In "+HOST+"\t"+String.valueOf(countingMergedMiRs(mergedHairpinRC,markder))+" pre-miRNAs(-) share identical sequences;");
        logger.info("They are: \n"+ mergedHairpinRC +"\n");
        count_matureSeq_fw = matureSeq_fw.size(); // recalculate the number of sequence left
        count_matureSeq_rc = matureSeq_rc.size();
        count_hairpinSeq_fw = hairpinSeq_fw.size();
        count_hairpinSeq_rc = hairpinSeq_rc.size();
        
        /*
        check overlapping
        */
        findingOverlapping(matureSeq_fw, true);
        findingOverlapping(matureSeq_rc, true);
        findingOverlapping(hairpinSeq_fw, false);
        findingOverlapping(hairpinSeq_rc, false);
        
        /*
        sequence similarity
        */
        SeqStrSimilarity(matureSeq_fw,true,  true);
        SeqStrSimilarity(matureSeq_rc,false, true);
        SeqStrSimilarity(hairpinSeq_fw, true, false);
        SeqStrSimilarity(hairpinSeq_rc, false, false);
        
    }
    
    private HashMap<String, String> getNameSeqDict(String fastaFile, String hostCode) throws IOException {
        /*
        read in fasta sequence, selecting based on defined species 
        replacing U with T
        */
        HashMap<String, String> fastaHashMap = new HashMap();
        try {
            BufferedReader brFA = new BufferedReader(new FileReader(new File(fastaFile)));
            String lineFA = null;
            while ((lineFA = brFA.readLine()) != null) {
                String fastaSeq = brFA.readLine().trim();
                String fastaName = lineFA.split(" ")[0].substring(1);
                if (fastaName.startsWith(hostCode)) {
                    fastaHashMap.put(fastaName, fastaSeq.replace("U", "T"));
                }
            }
            brFA.close();
        } catch (IOException ex) {
            throw new IOException("error reading miRBase fasta reference file <" + fastaFile + ">");
        }
        return fastaHashMap;
    }

    private String getVersionInformation(String mirbasepath) throws FileNotFoundException, IOException {
        /*
        based on the user defined path to miRBase
        Jasmine trying to figure out which version it is
        */
        String versioninfo = "The miRBase release information is unreachable!";
        String readmeFile = mirbasepath+FILESEPARATOR+"README";
        try {
            BufferedReader gffBR = new BufferedReader(new FileReader(new File(readmeFile)));
            String firstLinestr = gffBR.readLine();
            gffBR.close();
            versioninfo = firstLinestr.split("--")[1];
            logger.info("You are using miRBAse " +versioninfo);
        } catch (Exception e) {
            logger.info(versioninfo);
            e.printStackTrace();
        }
        return versioninfo;
    }

    private HashMap<String, String> mergeIdenticalEntry(HashMap<String, String> fastaDict) {
        HashMap<String, String> keySeqHash = new HashMap<>();
        /*
        merge multiple entries which contain identical sequence, into single entry
        */
        for (Map.Entry<String, String> entry : fastaDict.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            
            if(keySeqHash.containsKey(value)){
                keySeqHash.replace(value, (keySeqHash.get(value) +"|"+ key));
            } else{
                keySeqHash.put(value, key);
            }
        }
        /*
        revert sequence name as key
        */
        HashMap<String, String> keyNameHash = new HashMap<>(); 
        for (Map.Entry<String, String> entry : keySeqHash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            keyNameHash.put(value, key);
        }
        return keyNameHash;
    }

    private String getMergedMiRs(HashMap<String, String> fastaDict, boolean isMature) {
        /*
        find out which miRNAs or pre-miRNAs share identical sequences
        return in the form of miR1 -- miR2
        */
        ArrayList<String> seqIdArray = new ArrayList<>();
        for (Map.Entry<String, String> entry : fastaDict.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key.contains("|")) {
                seqIdArray.add(key.replace("|", " -- "));

                if (isMature) {// mature
                    String[] splitedStrs = key.split("\\|");
                    for (String ikey : splitedStrs) {
                        if (decision_on_fastaMature.get(ikey).equals("KEEP")) {
                            decision_on_fastaMature.replace(ikey, "merged identical sequence");
                        } else {
                            decision_on_fastaMature.replace(ikey, decision_on_fastaMature.get(ikey) + "; merged identical sequence");
                        }
                    }
                } else {// hairpin
                    for (String ikey : key.split("\\|")) {
                        if (decision_on_fastaHairpin.get(ikey).equals("KEEP")) {
                            decision_on_fastaHairpin.replace(ikey, "merged identical sequence");
                        } else {
                            decision_on_fastaHairpin.replace(ikey, decision_on_fastaHairpin.get(ikey) + "; merged identical sequence");
                        }
                    }
                }
            }

        }
        return StringUtils.join(seqIdArray, "; ");
    }

    private void ParsingGFF(String path2gff) {
        /*
        parsing GFF file for building complete profile about which strand and precursor generates mature miRNA
        */
        try {
            BufferedReader gffReader = new BufferedReader(new FileReader(new File(path2gff)));
            String gffLineStr = null;
            while ((gffLineStr = gffReader.readLine()) != null) {
                if(gffLineStr.startsWith("#")){
                    continue;
                }
                String[] gffSplitted = gffLineStr.split("\t");
                if(gffSplitted[2].equals("miRNA_primary_transcript")){
                    String strandStr = gffSplitted[6];
                    String[] jfeatures = gffSplitted[8].split(";");
                    String hairpinName = jfeatures[2].split("=")[1];
                    String hairpinID = jfeatures[0].split("=")[1];
                    if (gff_hairpinInfo_Hash.containsKey(hairpinID)) {
                        logger.warn("identical ID in precursor");
                    } else{
                        gff_hairpinInfo_Hash.put(hairpinID, (hairpinName+":"+strandStr));
                    }
                }
                else if(gffSplitted[2].equals("miRNA")){
                    String strandStr = gffSplitted[6];
                    String[] jfeatures = gffSplitted[8].split(";");
                    String matureName = jfeatures[2].split("=")[1];
                    String precursorID = jfeatures[3].split("=")[1];
                    if(gff_matureInfo_Hash.containsKey(matureName)){
                        gff_matureInfo_Hash.replace(matureName, (gff_matureInfo_Hash.get(matureName) +";" + precursorID));
                    } else{
                        gff_matureInfo_Hash.put(matureName, precursorID);
                    }
                }
            }
            gffReader.close();
            for (Map.Entry<String, String> entry
                    : gff_matureInfo_Hash.entrySet()) {
                String gffkey = entry.getKey();
                String gffvalue = entry.getValue();
                gffAnnotated_Mature_Array.add(gffkey);
                if (gffvalue.contains(";")) {
                    ArrayList<String> premirArray = new ArrayList<>();
                    for (String iprecursorID : gffvalue.split(";")) {
                        premirArray.add(gff_hairpinInfo_Hash.get(iprecursorID));
                        gffAnnotated_Hairpin_Array.add(gff_hairpinInfo_Hash.get(iprecursorID).split(":")[0]);
                    }
                    gff_mature_in_precursor_StrandHash.put(gffkey, StringUtils.join(premirArray, ";"));
                } else {
                    gff_mature_in_precursor_StrandHash.put(gffkey, gff_hairpinInfo_Hash.get(gffvalue));
                    gffAnnotated_Hairpin_Array.add(gff_hairpinInfo_Hash.get(gffvalue).split(":")[0]);
                }
            }
        } catch (Exception e) {
            logger.error("Jasmine could not access to gff file: " + path2gff);
        }
    }

    private HashMap<String, String> findingNoCoordinate(HashMap<String, String> fastaDict, ArrayList<String> gffAnnotated_Array, boolean ismature) {
        /*
        given a  fasta sequence hash and an array list of gff annotated miRNAs/pre-miRNAs
        only subset sequences with annotated genomic coordinates
        and return
         */
        HashMap<String, String> newFastaSeqHash = new HashMap<>();
        for (Map.Entry<String, String> entry : fastaDict.entrySet()) {
            String seqkey = entry.getKey();
            String seqvalue = entry.getValue();
            if (ismature) { // mature
                if (gffAnnotated_Mature_Array.contains(seqkey)) {
                    newFastaSeqHash.put(seqkey, seqvalue);
                } else {
                    noCoordinate_Mature_Array.add(seqkey);
                    if (decision_on_fastaMature.get(seqkey).equals("KEEP")) {
                        decision_on_fastaMature.replace(seqkey, "without coordinates");
                    }
                }
            } else { // hairpin
                if (gffAnnotated_Hairpin_Array.contains(seqkey)) {
                    newFastaSeqHash.put(seqkey, seqvalue);
                } else {
                    noCoordinate_Hairpin_Array.add(seqkey);
                    if (decision_on_fastaHairpin.get(seqkey).equals("KEEP")) {
                        decision_on_fastaHairpin.replace(seqkey, "without coordinates");
                    }
                }
            }
        }
        return newFastaSeqHash;
    }

    private void SortingSeqByStrand(HashMap<String, String> fastaDict, boolean isMature) {
        /*
        jasmine is strand wise,
        so here, jasmine is sorting sequence based on which strand it annotated on 
         */
        if (isMature) {
            /*
            sorting mature miRNA sequences
             */
            for (Map.Entry<String, String> entry : fastaDict.entrySet()) {
                String seqkey = entry.getKey();
                String seqvalue = entry.getValue();
                String precursorInfo = gff_mature_in_precursor_StrandHash.get(seqkey);
                if (precursorInfo.contains(";")) {
                    for (String iprecursor : precursorInfo.split(";")) {
                        String strandInfo = iprecursor.split(":")[1];
                        if (strandInfo.equals("+")) {
                            matureSeq_fw.put(seqkey, seqvalue);
                        } else {
                            matureSeq_rc.put(seqkey, seqvalue);
                        }
                    }
                } else {
                    String strandInfo = precursorInfo.split(":")[1];
                    if (strandInfo.equals("+")) {
                        matureSeq_fw.put(seqkey, seqvalue);
                    } else {
                        matureSeq_rc.put(seqkey, seqvalue);
                    }
                }
            }
        } else {
            /*
            sorting precursor sequences
             */
            HashMap<String, String> precursor_StrandHash = new HashMap<>();
            for (Map.Entry<String, String> entry
                    : gff_mature_in_precursor_StrandHash.entrySet()) {
                String gffkey = entry.getKey();
                String gffvalue = entry.getValue();
                if (gffvalue.contains(";")) {
                    for (String iprecursor : gffvalue.split(";")) {
                        precursor_StrandHash.put(iprecursor.split(":")[0], iprecursor.split(":")[1]);
                    }
                } else {
                    precursor_StrandHash.put(gffvalue.split(":")[0], gffvalue.split(":")[1]);
                }
            }
            for (Map.Entry<String, String> entry : fastaDict.entrySet()) {
                String seqkey = entry.getKey();
                String seqvalue = entry.getValue();
                String strandInfo = precursor_StrandHash.get(seqkey);
                if (strandInfo.equals("+")) {
                    hairpinSeq_fw.put(seqkey, seqvalue);
                } else {
                    hairpinSeq_rc.put(seqkey, seqvalue);
                }
            }
        }
    }

    private int countingMergedMiRs(String mergedMiRList, String markerStr) {
        /*
        counting how many entries in merged name list string
        */
        int countMiR = 0;
        for (String imerged : mergedMiRList.split(";")) {
            countMiR += imerged.split(markerStr).length;
        }
        return countMiR;
    }

    private void findingOverlapping(HashMap<String, String> queryFastaHash, boolean isMature) {
        /*
        given a HashMap of sequence, to find a pair of entries share overlapping sequences
        */
        ArrayList<String> hashKetSet = new ArrayList<>();
        for (Map.Entry<String, String> entry : queryFastaHash.entrySet()) {
            String key = entry.getKey();
            hashKetSet.add(key);
        }
        for (int i = 0; i < hashKetSet.size(); i++) {
            for (int j = i + 1; j < hashKetSet.size(); j++) {
                String ikeyStr = hashKetSet.get(i);
                String jkeyStr = hashKetSet.get(j);
                String iseqStr = queryFastaHash.get(ikeyStr);
                String jseqStr = queryFastaHash.get(jkeyStr);
                if (iseqStr.length() > jseqStr.length()) {
                    if (iseqStr.contains(jseqStr)) {
                        logger.info(ikeyStr + " containing " + jkeyStr);
                        adding2Ambiguous(ikeyStr);
                        adding2Ambiguous(jkeyStr);
                        if (isMature) { // mature
                            for (String ikeyisplit : ikeyStr.split("\\|")) {
                                if (decision_on_fastaMature.get(ikeyisplit).equals("KEEP")) {
                                    decision_on_fastaMature.replace(ikeyisplit, "sequence overlapping" );
                                } else {
                                    decision_on_fastaMature.replace(ikeyisplit, decision_on_fastaMature.get(ikeyisplit) + "; sequence overlapping" );
                                }
                            }
                            for (String jkeyisplit : jkeyStr.split("\\|")) {
                                if (decision_on_fastaMature.get(jkeyisplit).equals("KEEP")) {
                                    decision_on_fastaMature.replace(jkeyisplit, "sequence overlapping" );
                                } else {
                                    decision_on_fastaMature.replace(jkeyisplit, decision_on_fastaMature.get(jkeyisplit) + "; sequence overlapping") ;
                                }
                            }
                        } else{ // hairpin
                            for (String ikeyisplit : ikeyStr.split("\\|")) {
                                if (decision_on_fastaHairpin.get(ikeyisplit).equals("KEEP")) {
                                    decision_on_fastaHairpin.replace(ikeyisplit, "sequence overlapping" );
                                } else {
                                    decision_on_fastaHairpin.replace(ikeyisplit, decision_on_fastaHairpin.get(ikeyisplit) + "; sequence overlapping" );
                                }
                            }
                            for (String jkeyisplit : jkeyStr.split("\\|")) {
                                if (decision_on_fastaHairpin.get(jkeyisplit).equals("KEEP")) {
                                    decision_on_fastaHairpin.replace(jkeyisplit, "sequence overlapping" );
                                } else {
                                    decision_on_fastaHairpin.replace(jkeyisplit, decision_on_fastaHairpin.get(jkeyisplit) + "; sequence overlapping") ;
                                }
                            }
                        }
                    }
                } else {
                    if (jseqStr.contains(iseqStr)) {
                        logger.info(jkeyStr + " containing " + ikeyStr);
                        adding2Ambiguous(ikeyStr);
                        adding2Ambiguous(jkeyStr);
                        if (isMature) { // mature
                            logger.info(jkeyStr);
                            for (String jkeyStrisplit : jkeyStr.split("\\|")) {
                                if (decision_on_fastaMature.get(jkeyStrisplit).equals("KEEP")) {
                                    decision_on_fastaMature.replace(jkeyStrisplit, "sequence overlapping");
                                } else {
                                    decision_on_fastaMature.replace(jkeyStrisplit, decision_on_fastaMature.get(jkeyStrisplit) + "; sequence overlapping");
                                }
                            }
                            for (String ikeyStrisplit : ikeyStr.split("\\|")) {
                                if (decision_on_fastaMature.get(ikeyStrisplit).equals("KEEP")) {
                                    decision_on_fastaMature.replace(ikeyStrisplit, "sequence overlapping");
                                } else {
                                    decision_on_fastaMature.replace(ikeyStrisplit, decision_on_fastaMature.get(ikeyStrisplit) + "; sequence overlapping");
                                }
                            }
                        } else{ // hairpin
                            for (String jkeyStrisplit : jkeyStr.split("\\|")) {
                                if (decision_on_fastaHairpin.get(jkeyStrisplit).equals("KEEP")) {
                                    decision_on_fastaHairpin.replace(jkeyStrisplit, "sequence overlapping");
                                } else {
                                    decision_on_fastaHairpin.replace(jkeyStrisplit, decision_on_fastaHairpin.get(jkeyStrisplit) + "; sequence overlapping");
                                }
                            }
                            for (String ikeyStrisplit : ikeyStr.split("\\|")) {
                                if (decision_on_fastaHairpin.get(ikeyStrisplit).equals("KEEP")) {
                                    decision_on_fastaHairpin.replace(ikeyStrisplit, "sequence overlapping");
                                } else {
                                    decision_on_fastaHairpin.replace(ikeyStrisplit, decision_on_fastaHairpin.get(ikeyStrisplit) + "; sequence overlapping");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void SeqStrSimilarity(HashMap<String, String> queryFastaHash, boolean isFWstrand, boolean isMature) {
        /*
        applying Levenshtein Distance to measure the sequence similarity in either two sequences in the given hash
        */
        LevenshteinDistance distance = new LevenshteinDistance();
        ArrayList<String> hashKetSet = new ArrayList<>();
        for (Map.Entry<String, String> entry : queryFastaHash.entrySet()) {
            String key = entry.getKey();
            hashKetSet.add(key);
        }
        String iTaskMsg = "similar within "+String.valueOf(INT_MISMATCH)+" mismatches";
        for (int i = 0; i < hashKetSet.size(); i++) {
            for (int j = i + 1; j < hashKetSet.size(); j++) {
                String ikeyStr = hashKetSet.get(i);
                String jkeyStr = hashKetSet.get(j);
                String iseqStr = queryFastaHash.get(ikeyStr);
                String jseqStr = queryFastaHash.get(jkeyStr);
                
                int ijDist = distance.apply(iseqStr, jseqStr);
                if( ijDist <= INT_MISMATCH ){
                    
                    if (!isMature) {
                        /*
                        it's hairpin!
                        */
                        if (isFWstrand) {
                            //
                            for (String ikeysplit : ikeyStr.split("\\|")) {
                                String ikeyChild = gff_precursor_to_mature_Hash.get(ikeysplit);
                                logger.info(ikeyChild);
                            }
                            
                        }
                    }
                    adding2Ambiguous(ikeyStr);
                    adding2Ambiguous(jkeyStr);
                    logger.info(ikeyStr +" and "+jkeyStr+" are "+iTaskMsg);
                }
            }
        }
        
    }

    private void adding2Ambiguous(String queryEntryName) {
        /*
        check if the ambiguous list contains the query name
        if not, then add into it
        */
        if (!AMBIGUOUS_MIRS.contains(queryEntryName)) {
            AMBIGUOUS_MIRS.add(queryEntryName);
        }
    }

    private HashMap<String, String> convertingBiogenesis(HashMap<String, String> query_mature_AT_hairpinHash) {
        /*
        reconstract the profile, from precursor to mature miRNAs
        */
        HashMap<String, String> newBiogenesisHash = new HashMap<>();
        for (Map.Entry<String, String> entry
                : query_mature_AT_hairpinHash.entrySet()) {
            String matureKey = entry.getKey();
            String hairpinValue = entry.getValue();
            if (hairpinValue.contains(";")) {
                for (String ihairpin : hairpinValue.split(";")) {
                    ihairpin = ihairpin.split(":")[0];
                    if (newBiogenesisHash.containsKey(ihairpin)) {
                        newBiogenesisHash.replace(ihairpin, (newBiogenesisHash.get(ihairpin) + ";" + matureKey));
                    } else {
                        newBiogenesisHash.put(ihairpin, (matureKey));
                    }
                }
            } else {
                String ihairpin = hairpinValue.split(":")[0];
                if (newBiogenesisHash.containsKey(ihairpin)) {
                    newBiogenesisHash.replace(ihairpin, (newBiogenesisHash.get(ihairpin) + ";" + matureKey));
                } else {
                    newBiogenesisHash.put(ihairpin, (matureKey));
                }
            }
        }
        return newBiogenesisHash;
    }

    private HashMap<String, String> initializeDecision(HashMap<String, String> queryFastaDict) {
        /*
        initialize hashMap for recording curating decision on an entry
        */
        HashMap<String, String> fastaDecision = new HashMap<>();
        for (Map.Entry<String, String> entry : queryFastaDict.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            fastaDecision.put(key, "KEEP");
        }
        return fastaDecision;
    }
   
}
