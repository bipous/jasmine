/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class Collapsing extends JASMINEstep implements JASMINEbase {
    Logger logger = LogManager.getRootLogger();
    private String TASK_FOLDER = "CollapsedReads";
    private String FASTQ2FASTA;
    private String COLLAPSER ;
    private String RAW_INPUT_EXTENSION =".fastq.gz";
    private String OUTFILE_EXTENSION_FA = ".trim.fasta";
    private String OUTFILE_EXTENSION_COLLAPSED = ".trim.clp.fasta";
    private String INFILE_EXTENSION = ".trim.fastq";
    private String NTHREADS;
    private String OUTPUTFOLDER;
    private String projectID;
    private String inputFolder ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder ;
    private String inputFolder_PATH ;
    private HashMap<String, String> CONFIG_HASH;
    
    Collapsing(HashMap<String, String> CONFIGHASH) throws IOException {
        CONFIG_HASH = CONFIGHASH;
        setConfigurations(CONFIG_HASH);
        executeThisStep();
    }

    @Override
    public void verifyInputData() throws IOException {
        logger.info("Not supported yet.");
    }

    @Override
    public void verifyOutputData() throws IOException {
        logger.info("Not supported yet.");
    }

    @Override
    public void setConfigurations(HashMap<String, String> CONFIG_HASH) throws IOException {
        logger.info("Configuring for read collapsing");
        this.FASTQ2FASTA = CONFIG_HASH.get("FASTQ2FASTA");
        this.COLLAPSER = CONFIG_HASH.get("COLLAPSER");
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.NTHREADS = CONFIG_HASH.get("THREADS");
        JASMINEutility misoUtility = new JASMINEutility();
        if(CONFIG_HASH.get("DATASET_ARRAY_STR").equals("NA")){
            this.datasetArray = misoUtility.getDatasetList(CONFIG_HASH.get("DATASETLIST"));
        }
        else{
            this.datasetArray = new ArrayList<String>(Arrays.asList(CONFIG_HASH.get("DATASET_ARRAY_STR").split("#"))) ;
        }
        
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        }
        else{
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER") +FILESEPARATOR;
        }  
        logger.info("-------->");
        logger.info(CONFIG_HASH.get("INPUTFOLDER"));
        if (new File(CONFIG_HASH.get("INPUTFOLDER")).isDirectory()) {
            this.inputFolder_PATH = CONFIG_HASH.get("INPUTFOLDER");
        }
        else{
            this.inputFolder_PATH = projectFolder + CONFIG_HASH.get("INPUTFOLDER") +FILESEPARATOR;
        }
        this.OUTPUTFOLDER = projectFolder +TASK_FOLDER +FILESEPARATOR;

        String[] namesplitted = datasetArray.get(0).split("\\.");
        if(namesplitted[namesplitted.length -1].equals("gz")){
            this.RAW_INPUT_EXTENSION =  "."+namesplitted[namesplitted.length -2]+"."+namesplitted[namesplitted.length -1];
        }
        else{
            this.RAW_INPUT_EXTENSION = "."+namesplitted[namesplitted.length -1];
        }
    }

    @Override
    public void executeThisStep() throws IOException {
        logger.info("Executing read collapse step");
        String cmdFQ2FA = "";
        String cmdClp = "";
        String fastqInputFile = "";
        String fastaOutputFile = "";
        String clpOutputFile = ""; 
        for(String idataset: datasetArray) {
            try{
                Boolean f = new File(OUTPUTFOLDER).mkdir(); 
                if (f) logger.info("created output folder <" + OUTPUTFOLDER + "> for results" );
                /*
                    fastq_to_fasta -i 1000.fastq -o 1000.fasta -Q33
                */                      
                ArrayList<String> cmdQ2A = new ArrayList<>();
                cmdQ2A.add(FASTQ2FASTA);

                fastqInputFile = this.cleanPath(inputFolder_PATH  + idataset.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION));
                cmdQ2A.add("-i");
                cmdQ2A.add(fastqInputFile);
                cmdQ2A.add("-o");

                fastaOutputFile = OUTPUTFOLDER  + idataset.replace(RAW_INPUT_EXTENSION, OUTFILE_EXTENSION_FA);
                cmdQ2A.add(fastaOutputFile);
                cmdQ2A.add("-Q33");

                cmdFQ2FA = this.cleanPath(StringUtils.join(cmdQ2A, " "));
                logger.info("Fastq2Fasta command:\t" + cmdFQ2FA);
                Runtime rt = Runtime.getRuntime();
                Process proc = rt.exec(cmdFQ2FA);
                BufferedReader brStdin  = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                BufferedReader brStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
                    String line = null;
                    logger.info("<OUTPUT>");
                    while ( (line = brStdin.readLine()) != null)
                        logger.info(line);
                    logger.info("</OUTPUT>");
                    logger.info("<ERROR>");
                    while ( (line = brStdErr.readLine()) != null)
                        logger.info(line);
                    logger.info("</ERROR>");
                    int exitVal = proc.waitFor();            
                    System.out.println("Process exitValue: " + exitVal);            
                brStdin.close();
                brStdErr.close();
                logger.info("--completed");
            }
            catch(IOException|InterruptedException exIE){
                logger.error("error executing Fastq2Fasta command\n");
                logger.error("CMD is " + cmdFQ2FA);
                throw new IOException(TASK_FOLDER + ": error executing Fastq2Fasta command " 
                    + cmdFQ2FA + "\n"
                    + exIE.toString());
            }
            /*
                fastx_collapser -i 1000.fastq -o 1000.fasta -Q33
            */                      
            try{
                ArrayList<String> cmd2 = new ArrayList<>();
                cmd2.add(COLLAPSER);

                String fastaInputFile = fastaOutputFile;
                cmd2.add("-i");
                cmd2.add(fastaInputFile);

                cmd2.add("-o");

                clpOutputFile = OUTPUTFOLDER  + idataset.replace(RAW_INPUT_EXTENSION, OUTFILE_EXTENSION_COLLAPSED);
                cmd2.add(clpOutputFile);
                
                cmdClp = this.cleanPath(StringUtils.join(cmd2, " "));
                logger.info("Collapse fasta command:\t" + cmdClp);

                Runtime rtClp = Runtime.getRuntime();
                Process procClp = rtClp.exec(cmdClp);
                BufferedReader brStdinClp  = new BufferedReader(new InputStreamReader(procClp.getInputStream()));
                BufferedReader brStdErrClp = new BufferedReader(new InputStreamReader(procClp.getErrorStream()));
                
                String line = null;
                logger.info("<OUTPUT>");
                while ( (line = brStdinClp.readLine()) != null)
                    logger.info(line);
                logger.info("</OUTPUT>");
                
                logger.info("<ERROR>");
                while ( (line = brStdErrClp.readLine()) != null)
                    logger.info(line);
                logger.info("</ERROR>");
                
                
                int exitValclp = procClp.waitFor();            
                System.out.println("Process exitValue: " + exitValclp);            
            
                brStdinClp.close();
                brStdErrClp.close();
            
            }
            catch(IOException|InterruptedException exIE){
                logger.error("error executing Collapse Fasta command\n");
                logger.error("CMD is " + cmdClp);
                throw new IOException(TASK_FOLDER + ": error executing Collapse Fasta command " + cmdClp);
             }
        }
        logger.info(TASK_FOLDER + ": completed");
        
    }
    
     private String cleanPath(String pathstring) {
        return pathstring.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
    }

    public HashMap<String, String> updateConfig() {
        /*
        updating configuration hash, passing it to next step
        */
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        CONFIG_HASH.replace("DATASET_ARRAY_STR", StringUtils.join(datasetArray, "#"));
        return CONFIG_HASH;
    }

    
}
