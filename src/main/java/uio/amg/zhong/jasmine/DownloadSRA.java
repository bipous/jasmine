/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
class DownloadSRA {
    
    Logger logger = LogManager.getRootLogger();
    
    private String TASK_FOLDER = "readsData";
    private String  HOST;
    private String OUTPUTFOLDER;
    private String projectID;
    private String mirBasePath;
    private String mirBaseVersion;
    private String bowtiepath;
    private int mismatch;
    private String inputFolder ;
    private String datasetlstFile;
    private int threads ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder ;
    private String SRAlistFile;
    private ArrayList<String> SRAaccArrayList;
    private String inputFolder_PATH;

    DownloadSRA(HashMap<String, String> CONFIG_HASH) throws IOException, InterruptedException {
        setConfigurations(CONFIG_HASH);
        startDownloading(); 
    }

    private void setConfigurations(HashMap<String, String> CONFIG_HASH) {
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.threads = Integer.valueOf(CONFIG_HASH.get("THREADS"));
        this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        this.SRAlistFile = CONFIG_HASH.get("SRA_LIST");
        this.OUTPUTFOLDER = CONFIG_HASH.get("PROJECT_FOLDER")+TASK_FOLDER;
        
//        if (new File(CONFIG_HASH.get("INPUTFOLDER")).isDirectory()) {
////            this.inputFolder_PATH = CONFIG_HASH.get("INPUTFOLDER");
//        }
//        else{
//            this.inputFolder_PATH = projectFolder + CONFIG_HASH.get("INPUTFOLDER") +FILESEPARATOR;
//        }
    }

    private void startDownloading() throws IOException, InterruptedException {
        SRAaccArrayList = getSRAacc(this.SRAlistFile);
        downloadByWget();
        Boolean fA = new File(OUTPUTFOLDER).mkdir();
        if (fA) {
            logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
        }
    }

    private ArrayList<String> getSRAacc(String SRAlistFile) throws IOException {
        ArrayList<String> sraacc = new ArrayList<>();
        try{
            logger.info("reading mapping summary file <" +  SRAlistFile + ">");
            BufferedReader sralistBR = new BufferedReader(new FileReader(new File(SRAlistFile)));
            String lineStr = null;
            while ((lineStr = sralistBR.readLine())!=null){
                if(lineStr.startsWith("dataset")) continue;
                String datasetString = lineStr.trim().split("\t")[0];
                sraacc.add(lineStr.trim());
            }
            sralistBR.close();
            logger.info("read " + sraacc.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading mapping summary file <" +  SRAlistFile + ">\n" + ex.toString());
            throw new IOException("error reading mapping summary file <" +  SRAlistFile + ">");
        }
        return sraacc;
    }

    private void downloadByFetch() throws IOException, InterruptedException {
        String fetchPath = "prefetch"; //"/usr/local/bin/prefetch"
        String downloadCmd = fetchPath + "  --option-file   " + SRAlistFile;
        Runtime rtGenMap = Runtime.getRuntime();
        Process procGenMap = rtGenMap.exec(downloadCmd);

        procGenMap.waitFor();
//        for (String iSRAacc : SRAaccArrayList){
//            
//        }
    }

    private void downloadByWget() throws IOException, InterruptedException {
        
        Runtime downloadRuntime = Runtime.getRuntime();
        for(String iSRAacc : SRAaccArrayList){
            String sraftpurl =  "ftp://ftp-trace.ncbi.nih.gov";
            String srapath = "/sra/sra-instant/reads/ByRun/sra/"+iSRAacc.substring(0,3)+"/"+
                    iSRAacc.substring(0,6)+"/"+iSRAacc +"/"+iSRAacc+ ".sra";

            String wgetCmd = "wget " +sraftpurl+srapath+ "  -P "+OUTPUTFOLDER;
            logger.info(wgetCmd);
            Process procWget= downloadRuntime.exec(wgetCmd);
            procWget.waitFor();
//            while (!procWget.isAlive()) {
//                procWget.destroy();
//                
//            }
            
//            String fastqDumpPath = "/usr/local/bin/fastq-dump  ";
//            String fastqdumpCmd = fastqDumpPath + OUTPUTFOLDER + FILESEPARATOR +iSRAacc+".sra";
//            Process procFastqdump= downloadRuntime.exec(fastqdumpCmd);
//            procFastqdump.waitFor();
        }
    }

    HashMap<String, String> updateConfig(HashMap<String, String> CONFIG_HASH) {
        /*
        updating configuration hash, passing it to next step
        */
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        return CONFIG_HASH;
    }
    
}
