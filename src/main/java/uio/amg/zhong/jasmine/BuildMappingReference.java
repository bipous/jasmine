/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uio.amg.zhong.utility.ReadFastaFile;

/**
 * * This builds a mapping reference using the a miRBase hairpin.fa file specified in the XML configuration file
 * 
 * @author xiangfuz
 */
public class BuildMappingReference {
    Logger logger = LogManager.getRootLogger();
    private ArrayList<String> similarMatureArray = new ArrayList<>();
    private ArrayList<String> similarHairpinArray = new ArrayList<>();
    private String miRBasePath ;
    private String OUTPUTFolder;
    private String HOST ;
    private  int NTShifting ;
    private String TASK_FOLDER = "Reference";
    private String BOWTIE_BUILD ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private String REF_SOURCE;
    private String mirGeneDBPath_gff;
    private ArrayList<String> mirGeneDB_precursor_ACCs;
    private ArrayList<String> mirGeneDB_microrna_ACCs;
    private boolean  useMirGeneDB=false;

    BuildMappingReference(HashMap<String, String> CONFIG_HASH) throws IOException, InterruptedException {
        setConfigHash(CONFIG_HASH);
        
        startBuilding();
    }

    BuildMappingReference() {
        logger.info("Not supported yet.");
    }
    
    private void setConfigHash(HashMap<String, String> CONFIG_HASH) throws IOException {
        this.HOST = CONFIG_HASH.get("HOST");
//        this.NTShifting = Integer.valueOf(CONFIG_HASH.get("NT_SHIFTING"));
        this.BOWTIE_BUILD = CONFIG_HASH.get("BOWTIE_BUILD_PATH");
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.OUTPUTFolder = CONFIG_HASH.get("PROJECT_FOLDER")+TASK_FOLDER+FILESEPARATOR;
        }
        else{
            this.OUTPUTFolder = CONFIG_HASH.get("PROJECT_FOLDER")+FILESEPARATOR+TASK_FOLDER+FILESEPARATOR;
        }
        
        if(CONFIG_HASH.get("MIRBASE_PATH").endsWith(FILESEPARATOR)){
            this.miRBasePath = CONFIG_HASH.get("MIRBASE_PATH");
        }
        else{
            this.miRBasePath = CONFIG_HASH.get("MIRBASE_PATH")+FILESEPARATOR;
        }
        
        REF_SOURCE ="miRBase";
        
        /*
        Current, jasmine only support gff annotation in MirGeneDB
        */
        mirGeneDBPath_gff = CONFIG_HASH.get("MIRGENEDB_PATH");
        if (mirGeneDBPath_gff.equals("NA")) {
            logger.info("Jasmine will only use miRBase annotation. Because MirGeneDB gff is not defined. ");
            useMirGeneDB = false;
        }
        else{
            if (mirGeneDBPath_gff.toLowerCase().contains(HOST) && mirGeneDBPath_gff.toLowerCase().contains("gff")) {
                ExtractingAccessionIDfromGff(mirGeneDBPath_gff);
                /*
                double check whether these two ArrayList are empty or not
                */
                if (mirGeneDB_microrna_ACCs.size() > 0 && mirGeneDB_precursor_ACCs.size() > 0) {
                    useMirGeneDB = true;
                }
            }
            else{
                logger.info("The defined MirGeneDB gff is not in gff format, or not matcheing with current chosen species:"+HOST);
            }
        }
        
    }
    

    public void setSimilarMature(ArrayList<String> similarMature) {
        this.similarMatureArray = similarMature;
    }

    public void setSimilarHairpin(ArrayList<String> similarHairpin) {
        this.similarHairpinArray = similarHairpin;
    }

    public void setMiRBasePath(String path2miRbaseString) {
        this.miRBasePath = path2miRbaseString;
    }

    public void setOutputFolder(String outputFolder) {
        this.OUTPUTFolder = outputFolder;
    }

    public void setHost(String hostString) {
        this.HOST = hostString;
    }

    public void startBuilding() throws IOException, InterruptedException {
        Boolean fA = new File(OUTPUTFolder).mkdir();       
        if (fA) logger.info("created output folder <" + OUTPUTFolder+ "> for results" );
        
        ReadFastaFile readFastaFile = new ReadFastaFile();
        
        /*
        build unique fasta sequence of hairpin
        and select the sequences that match the host specified in the configuration file

        */
        logger.info("--hairpinFastaFile is <" + miRBasePath+"hairpin.fa" + ">");
        logger.info("--reading hairpinFastaFile");
        readFastaFile.readFasta((miRBasePath+"hairpin.fa"), HOST, useMirGeneDB, mirGeneDB_precursor_ACCs );
        logger.info("--done");
        logger.info("--parsing hashtable");
        HashMap<String,String>  hairpinFastaHash = readFastaFile.getSeqStrNameHash();
        HashMap<String,String>  uniqueHairpinSeqHash = new HashMap<>();
        for (Map.Entry<String, String> entry : hairpinFastaHash.entrySet()) {
            String hairpinName = entry.getKey();
            String hairpinSeq = entry.getValue().replace("U", "T");
            
            if(uniqueHairpinSeqHash.containsKey(hairpinSeq)){
                String tmpname = uniqueHairpinSeqHash.get(hairpinSeq) +"|"+hairpinName;
                uniqueHairpinSeqHash.replace(hairpinSeq,tmpname);
            }
            else{
                uniqueHairpinSeqHash.put(hairpinSeq,hairpinName);
            }
        }
        logger.info("--done");
        
        /*
        build unique fasta sequence of mature miRNAs
        */
        logger.info("--matureFastaFile is <" + miRBasePath+"mature.fa" + ">");
        logger.info("--reading matureFastaFile");
        readFastaFile.readFasta((miRBasePath+"mature.fa"), HOST, useMirGeneDB, mirGeneDB_microrna_ACCs);
        logger.info("--done");
        logger.info("--parsing hashtable");
        HashMap<String,String>  matureFastaHash = readFastaFile.getSeqStrNameHash();
        HashMap<String,String>  uniqueMatureSeqHash = new HashMap<>();
        for (Map.Entry<String, String> entry : matureFastaHash.entrySet()) {
            String matureName = entry.getKey();
            String matureSeq = entry.getValue().replace("U", "T");
            if(uniqueMatureSeqHash.containsKey(matureSeq)){
                String tmpname = uniqueMatureSeqHash.get(matureSeq) +"|"+matureName;
                uniqueMatureSeqHash.put(matureSeq,tmpname);
            }
            else{
                uniqueMatureSeqHash.put(matureSeq,matureName);
            }
        }
        logger.info("--done");
        
        /*
        get extended mature sequence
        */
        ArrayList<String> matureInHairpin = new ArrayList<>();
        for (Map.Entry<String, String> hairpinEntry : uniqueHairpinSeqHash.entrySet()) {
            String hairpinSeq  = hairpinEntry.getKey();
            String hairpinName = hairpinEntry.getValue();
            
            StringBuilder containMatures = new StringBuilder();
                    
            for (Map.Entry<String, String> matureEntry : uniqueMatureSeqHash.entrySet()) {
                String matureSeq = matureEntry.getKey();
                String matureName = matureEntry.getValue();
                int matureSeqLength = matureSeq.length();
                
                if(hairpinSeq.contains(matureSeq)){
                    int startIndex = hairpinSeq.indexOf(matureSeq);
                    String tmpstr = matureName+":"+String.valueOf(matureSeqLength)+":"
                            +String.valueOf(startIndex)+":"+String.valueOf(startIndex+matureSeqLength)+";";
                    containMatures.append(tmpstr);
                }               
            }
            String tmpFullMatures = containMatures.toString();
            matureInHairpin.add(hairpinName+"\t"+tmpFullMatures.substring(0,(tmpFullMatures.length()-1)));
        }
        
        writeOutArrayList(matureInHairpin, (OUTPUTFolder+REF_SOURCE+"."+HOST+".matureInHairpin.tsv"));
        
        
        
        
        logger.info("--unique.hairpin.fasta is <" + OUTPUTFolder+REF_SOURCE+"."+HOST+".unique.hairpin.fasta" + ">");
        String outputFilename = OUTPUTFolder+REF_SOURCE+"."+HOST+".unique.hairpin.fasta";
        logger.info("--writing");
        BufferedWriter outputHBW = new BufferedWriter(new FileWriter(new File(outputFilename)));
        for (Map.Entry<String, String> entry : uniqueHairpinSeqHash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            outputHBW.write(">"+value+"\n"+ key+ "\n");
        }
        outputHBW.close();
        logger.info("--done");
        
        /*
        go ahead and build the reference sequence based on the unique hairpin sequences
        */
        buildBowtieIndex(outputFilename, OUTPUTFolder+REF_SOURCE+"."+HOST+".hairpin");
        
        logger.info("--unique.mature.fasta is <" + OUTPUTFolder+REF_SOURCE+"."+HOST+".unique.mature.fasta" + ">");
        logger.info("--writing");
        String outputMFilename = OUTPUTFolder+REF_SOURCE+"."+HOST+".unique.mature.fasta";
        BufferedWriter outputMBW = new BufferedWriter(new FileWriter(new File(outputMFilename)));
        for (Map.Entry<String, String> entry : uniqueMatureSeqHash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            outputMBW.write(">"+value+"\n"+ key+ "\n");
        }
        outputMBW.close();
        logger.info("--done");
        logger.info("finished");

        parseMatureInhairpin(matureInHairpin);
        
    }
    
    
    private void writeOutArrayList(ArrayList<String> mirAmbiguousList, String outputFilename) throws IOException {
        BufferedWriter outputBW = new BufferedWriter(new FileWriter(new File(outputFilename)));
        
        for (String istr : mirAmbiguousList) {
            outputBW.write(istr + "\n");
        }
        outputBW.close();
    }

    private void parseMatureInhairpin(ArrayList<String> matureInHairpin) throws IOException {
        ArrayList<String> uniqueMatureArray = new ArrayList<>();
        ArrayList<String> duplicateMatureArray = new ArrayList<>();
        
        for(String ilineStr: matureInHairpin){
            ilineStr = ilineStr.trim();
            if (ilineStr.contains(";")) {
                for(String imatureStr :ilineStr.split("\t")[1].split(";") ){
                    String imaturename = imatureStr.split(":")[0];
                    if (uniqueMatureArray.contains(imaturename)) {
                        if (!duplicateMatureArray.contains(imaturename)){
                            duplicateMatureArray.add(imaturename);
                        }
                    }
                    else{
                        uniqueMatureArray.add(imaturename);
                    }
                }
                checkCloseLocation(ilineStr.split("\t")[1]);
            }
            else{
                String imaturename = ilineStr.split("\t")[1].split(":")[0];
                if (uniqueMatureArray.contains(imaturename)) {
                    if (!duplicateMatureArray.contains(imaturename)){
                        duplicateMatureArray.add(imaturename);
                    }
                }
                else{
                    uniqueMatureArray.add(imaturename);
                }
            }
        }
        
        logger.info(matureInHairpin.size());
        logger.info(uniqueMatureArray.size());
        logger.info(duplicateMatureArray.size());
        
        writeOutArrayList(duplicateMatureArray, (OUTPUTFolder+REF_SOURCE+"."+HOST+".duplicateMature.tsv"));
    }

    private void checkCloseLocation(String nameClusterStr) {
        String[] splitedNameCluster = nameClusterStr.split(";");
        
        for(int i=0;i < (splitedNameCluster.length -1); i ++){
            int diffStart = Math.abs(Integer.valueOf(splitedNameCluster[i].split(":")[2]) - Integer.valueOf(splitedNameCluster[i+1].split(":")[2]));
        
            int diffEnd = Math.abs(Integer.valueOf(splitedNameCluster[i].split(":")[3]) - Integer.valueOf(splitedNameCluster[i+1].split(":")[3]));
            
            if(diffStart <=2 | diffEnd <= 2){
//                logger.info(nameClusterStr);
//                logger.info(splitedNameCluster[i].split(":")[0] +"\t\t\t"+ splitedNameCluster[i+1].split(":")[0]);
                
            }
        }
    }

    private void buildBowtieIndex(String outputFilename, String indexstring) throws IOException, InterruptedException {
        logger.info("buildBowtieIndex");
        String indexCmd = BOWTIE_BUILD+" " + outputFilename +"   "+indexstring ;
        Runtime buildIndex = Runtime.getRuntime();
        Process buildIndexProcess = buildIndex.exec(indexCmd);
        logger.info("buildBowtieIndex");
        logger.info("cmd is " + indexCmd);
        BufferedReader brStdin  = new BufferedReader(new InputStreamReader(buildIndexProcess.getInputStream()));
        BufferedReader brStdErr = new BufferedReader(new InputStreamReader(buildIndexProcess.getErrorStream()));
            String line = null;
            logger.info("<OUTPUT>");
            while ( (line = brStdin.readLine()) != null)
                logger.info(line);
            logger.info("</OUTPUT>");
            logger.info("<ERROR>");
            while ( (line = brStdErr.readLine()) != null)
                logger.info(line);
            logger.info("</ERROR>");
            int exitVal = buildIndexProcess.waitFor();            
            logger.info("Process exitValue: " + exitVal);            
        brStdin.close();
        brStdErr.close();
        logger.info("finished");
        // buildIndexProcess.waitFor();
    }
    
    /*
    this function is to extract accession ID (Alias) from gff file downloaded from MirGeneDB
    gff-version 3
    MirGeneDB   v2.0
    
    INPUT: path to gff file as string
    OUTPUT: two accession number ArrayList, for precursor and miRNA, respectively
    */
    private void ExtractingAccessionIDfromGff(String gffFilePath) throws FileNotFoundException, IOException {
        mirGeneDB_precursor_ACCs = new ArrayList<>();
        mirGeneDB_microrna_ACCs = new ArrayList<>();
        File gffFile = new File(gffFilePath);
        if(gffFile.exists()){
            try {
                BufferedReader gffBR = new BufferedReader(new FileReader(gffFile));
                String gffLine = null;
                while ((gffLine = gffBR.readLine()) != null) {
                    // skip head lines
                    if (!gffLine.startsWith("#")) {
                        /*
                        skip entries without Alias
                        those are MirGeneDB specific entries
                        */
                        if(gffLine.contains("Alias")){
                            // pre_miRNA or miRNA
                            String molType = gffLine.split("\t")[2]; 
                            //get alias string
                            String alias = gffLine.split("\t")[8].split("=")[2].trim();

                            if(molType.equals("pre_miRNA")){
                                /*
                                in case MirGeneDB has duplicate entries for pre_miRNAs
                                */
                                if(mirGeneDB_precursor_ACCs.contains(alias)){
                                    logger.warn(alias + " is duplicated in MirGeneDB. Accession only added once.");
                                }
                                else{
                                    mirGeneDB_precursor_ACCs.add(alias);
                                }
                            }
                            else{
                                /*
                                in case MirGeneDB has duplicate entries for mature miRNAs
                                */
                                if (mirGeneDB_microrna_ACCs.contains(alias)) {
                                    logger.warn(alias + " is duplicated in MirGeneDB. Accession only added once.");
                                }
                                else{
                                    mirGeneDB_microrna_ACCs.add(alias);
                                }
                            }
                        }
                    }
                }
                gffBR.close();
            } catch (FileNotFoundException fileEx) {
                logger.error("File Not Found Exception <" + fileEx + ">\n" + fileEx.toString());
                throw new FileNotFoundException("File Not Found Exception");
            } catch (IOException ioEx) {
                logger.error("IO Exception when reading :" + ioEx.toString());
                throw new IOException("IO Exception");
            }
        }
        else{
            logger.error("Your defined gff file count not found! " + gffFilePath);
            System.exit(1);
        }
    }

    
}
