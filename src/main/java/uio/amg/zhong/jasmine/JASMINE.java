/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sr
 */
public class JASMINE {

    static Logger logger = LogManager.getRootLogger();
    static Options options = new Options();

    public static void main(String[] args) throws IOException, FileNotFoundException, InterruptedException {
        String xmlFile = parseArguments(args);
        String configXMLfile = findXMLfile(xmlFile);
        if (configXMLfile != null) {
            readXML(configXMLfile);
        } 
        else {
            logger.info("\n\n\n"
                    + "    #############################################\n"
                    + "    |  jasmine could not read configuration XML file.                                              |\n"
                    + "    #############################################\n\n\n");

            logger.info("*** Report bugs to Joey \n");
        }
        /**
         * step options 1 CheckingReference 2 BuildMappingReference 3
         * AdapterTrimming 4 CollapseReads 3 HairpinMapping 4 SAMParsing 5
         * PostAnalysis
         */
        logger.info("finishing");
    }

    /**
     * print command line usage
     *
     */
    public static void printHelp() {
        printBanner();
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("command line options", options);
        System.exit(0);
    }

    /**
     * print program info
     *
     */
    public static void printBanner() {
        logger.info("\n\n\n"
                + "    =======================================================================\n"
                + "    |  JASMINE:                                                           |\n"
                + "    |    A software tool for investigating isomiR populations in NGS data |\n"
                + "    |                                                                     |\n"
                + "    |    Usage: java -jar JASMINE.jar <configuration_file>                |\n"
                + "    |      where the configuration_file is specified in XML format        |\n"
                + "    |      an example file is available on the bitbucket repository       |\n"
                + "    |      (https://bitbucket.org/bipous/jasmine/src/master/)             |\n"
                + "    |                                                                     |\n"
                + "    |                                                                     |\n"
                + "    =======================================================================\n\n\n");

        logger.info("*** report issues to <simon.rayner@medisin.uio.no>\n");
    }

    /**
     * provide the user with basic information on how to run the program
     *
     * @param args
     */
    public static String parseArguments(String args[]) {
        logger.info("parse arguments");
        options.addOption("h", "help", false, "view help");

        CommandLineParser clParser = new BasicParser();
        CommandLine cmd = null;
        try {
            cmd = clParser.parse(options, args);

            if (cmd.hasOption("h")) {
                printHelp();
            } else {
                if (cmd.getArgs().length == 1) {
                    // it looks like the user specified an XML file
                    // let's check it exists
                    return cmd.getArgs()[0];
                } else {
                    return null;
                }
            }
            return "finished";
        } catch (ParseException exPa) {
            logger.info("error parsing parameters:\n" + exPa);
            logger.error("error parsing parameters::\n" + exPa);
        }
        return null;
    }

    /**
     * load the XML file containing all the parameters needed to run Jasmine
     * (NOTE: Currently, there isn't a specification anywhere about what is
     * allowed in the XML)
     *
     * @param xmlFilePath
     */
    private static void readXML(String xmlFilePath) {
        HashMap<String, String> configHashMap = new HashMap<String, String>() {
            {
                put("HOST", "NA");
                put("MIRBASE_PATH", "NA");
                put("PROJECT_ID", "NA");
                put("PROJECT_FOLDER", "NA");
                put("MISMATCH", "NA");
                put("DATASETLIST", "NA");
                put("DATASETLIST_FILE", "NA");
                put("THREADS", "NA");
                put("INPUTFOLDER", "NA");
                put("FASTQ2FASTA", "NA");
                put("ADAPTER", "NA");
                put("RSCRIPT", "NA");
                put("FASTQC", "NA");
                put("MULTIQC", "NA");
                put("PIPELINE_STEPS", "NA");
                put("TRIMMER_NAME", "NA");
                put("TRIMMER_PATH", "NA");
                put("TRIMMER_CONF", "NA");
                put("COLLAPSER", "NA");
                put("READ_FILTERING_COUNT", "NA");
                put("READ_FILTERING_LENGTH", "NA");
                put("MAPPER_NAME", "NA");
                put("MAPPER_PATH", "NA");
                put("MAPPER_CONF", "NA");
                put("BOWTIE_BUILD_PATH", "NA");
                put("DATASET_ARRAY_STR", "NA");
                put("CUSTOMIZED_FASTA", "NA");
                put("NT_SHIFTING", "NA");
                put("SRA_LIST", "NA");
                put("MIRGENEDB_PATH", "NA");
            }
        };
        File inputFile;
        NodeList projectList;

        try {
            inputFile = new File(xmlFilePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            projectList = doc.getElementsByTagName("project");
            logger.info("----------------------------");

            for (int temp = 0; temp < projectList.getLength(); temp++) {
                Node iProject = projectList.item(temp);
                if (iProject.getNodeType() == Node.ELEMENT_NODE) {
                    Element iProjectElement = (Element) iProject;
                    String projectName = iProjectElement.getAttribute("name");
                    boolean isExecute = false;
                    String isExecuteStr = iProjectElement.getAttribute("execute");
                    if (isExecuteStr.equals("YES")) {
                        isExecute = true;
                        logger.info("Starting working on " + projectName + "\n");
                        /*
                        build configuration hash map
                         */
                        if (iProjectElement.getElementsByTagName("HOST").getLength() != 0) {
                            configHashMap.replace("HOST", iProjectElement.getElementsByTagName("HOST").item(0).getTextContent());
                            logger.info("set <HOST> to <" + iProjectElement.getElementsByTagName("HOST").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "HOST" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("MIRBASE_PATH").getLength() != 0) {
                            configHashMap.replace("MIRBASE_PATH", iProjectElement.getElementsByTagName("MIRBASE_PATH").item(0).getTextContent());
                            logger.info("set <MIRBASE_PATH> to <" + iProjectElement.getElementsByTagName("MIRBASE_PATH").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "MIRBASE_PATH" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("MIRGENEDB_PATH").getLength() != 0) {
                            configHashMap.replace("MIRGENEDB_PATH", iProjectElement.getElementsByTagName("MIRGENEDB_PATH").item(0).getTextContent());
                            logger.info("set <MIRGENEDB_PATH> to <" + iProjectElement.getElementsByTagName("MIRGENEDB_PATH").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "MIRGENEDB_PATH" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("PROJECT_ID").getLength() != 0) {
                            configHashMap.replace("PROJECT_ID", iProjectElement.getElementsByTagName("PROJECT_ID").item(0).getTextContent());
                            logger.info("set <PROJECT_ID> to <" + iProjectElement.getElementsByTagName("PROJECT_ID").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "PROJECT_ID" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("PROJECT_FOLDER").getLength() != 0) {
                            configHashMap.replace("PROJECT_FOLDER", iProjectElement.getElementsByTagName("PROJECT_FOLDER").item(0).getTextContent());
                            logger.info("set <PROJECT_FOLDER> to <" + iProjectElement.getElementsByTagName("PROJECT_FOLDER").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "PROJECT_FOLDER" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("MISMATCH").getLength() != 0) {
                            configHashMap.replace("MISMATCH", iProjectElement.getElementsByTagName("MISMATCH").item(0).getTextContent());
                            logger.info("set <MISMATCH> to <" + iProjectElement.getElementsByTagName("MISMATCH").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "MISMATCH" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("INPUTFOLDER").getLength() != 0) {
                            configHashMap.replace("INPUTFOLDER", iProjectElement.getElementsByTagName("INPUTFOLDER").item(0).getTextContent());
                            logger.info("set <INPUTFOLDER> to <" + iProjectElement.getElementsByTagName("INPUTFOLDER").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "INPUTFOLDER" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("DATASETLIST").getLength() != 0) {
                            configHashMap.replace("DATASETLIST", iProjectElement.getElementsByTagName("DATASETLIST").item(0).getTextContent());
                            logger.info("set <DATASETLIST> to <" + iProjectElement.getElementsByTagName("DATASETLIST").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "DATASETLIST" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("THREADS").getLength() != 0) {
                            configHashMap.replace("THREADS", iProjectElement.getElementsByTagName("THREADS").item(0).getTextContent());
                            logger.info("set <THREADS> to <" + iProjectElement.getElementsByTagName("THREADS").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "THREADS" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("FASTQ2FASTA").getLength() != 0) {
                            configHashMap.replace("FASTQ2FASTA", iProjectElement.getElementsByTagName("FASTQ2FASTA").item(0).getTextContent());
                            logger.info("set <FASTQ2FASTA> to <" + iProjectElement.getElementsByTagName("FASTQ2FASTA").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "FASTQ2FASTA" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("ADAPTER").getLength() != 0) {
                            configHashMap.replace("ADAPTER", iProjectElement.getElementsByTagName("ADAPTER").item(0).getTextContent());
                            logger.info("set <ADAPTER> to <" + iProjectElement.getElementsByTagName("ADAPTER").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "ADAPTER" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("RSCRIPT").getLength() != 0) {
                            configHashMap.replace("RSCRIPT", iProjectElement.getElementsByTagName("RSCRIPT").item(0).getTextContent());
                            logger.info("set <RSCRIPT> to <" + iProjectElement.getElementsByTagName("RSCRIPT").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "RSCRIPT" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("FASTQC").getLength() != 0) {
                            configHashMap.replace("FASTQC", iProjectElement.getElementsByTagName("FASTQC").item(0).getTextContent());
                            logger.info("set <FASTQC> to <" + iProjectElement.getElementsByTagName("FASTQC").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "FASTQC" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("MULTIQC").getLength() != 0) {
                            configHashMap.replace("MULTIQC", iProjectElement.getElementsByTagName("MULTIQC").item(0).getTextContent());
                            logger.info("set <MULTIQC> to <" + iProjectElement.getElementsByTagName("MULTIQC").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "MULTIQC" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("BOWTIE_BUILD_PATH").getLength() != 0) {
                            configHashMap.replace("BOWTIE_BUILD_PATH", iProjectElement.getElementsByTagName("BOWTIE_BUILD_PATH").item(0).getTextContent());
                            logger.info("set <BOWTIE_BUILD_PATH> to <" + iProjectElement.getElementsByTagName("BOWTIE_BUILD_PATH").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "BOWTIE_BUILD_PATH" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        if (iProjectElement.getElementsByTagName("COLLAPSER").getLength() != 0) {
                            configHashMap.replace("COLLAPSER", iProjectElement.getElementsByTagName("COLLAPSER").item(0).getTextContent());
                            logger.info("set <COLLAPSER> to <" + iProjectElement.getElementsByTagName("COLLAPSER").item(0).getTextContent() + ">");
                        } else {
                            logger.warn("Element tag <" + "COLLAPSER" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        NodeList filterNodes = iProjectElement.getElementsByTagName("READ_FILTERING");
                        if (filterNodes.getLength() != 0) {
                            logger.info("found <READ_FILTERING> tag ");
                            for (int tempi = 0; tempi < filterNodes.getLength(); tempi++) {
                                Node iFilterNode = filterNodes.item(tempi);
                                if (iFilterNode.getNodeType() == Node.ELEMENT_NODE) {
                                    Element filterElement = (Element) iFilterNode;
                                    if (filterElement.getElementsByTagName("READ_FILTERING_COUNT").getLength() != 0) {
                                        configHashMap.replace("READ_FILTERING_COUNT", filterElement.getElementsByTagName("READ_FILTERING_COUNT").item(0).getTextContent());
                                        logger.info("set <READ_FILTERING_COUNT> to <" + iProjectElement.getElementsByTagName("READ_FILTERING_COUNT").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "READ_FILTERING_COUNT" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                    if (filterElement.getElementsByTagName("READ_FILTERING_LENGTH").getLength() != 0) {
                                        configHashMap.replace("READ_FILTERING_LENGTH", filterElement.getElementsByTagName("READ_FILTERING_LENGTH").item(0).getTextContent());
                                        logger.info("set <READ_FILTERING_LENGTH> to <" + iProjectElement.getElementsByTagName("READ_FILTERING_LENGTH").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "READ_FILTERING_LENGTH" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                }
                            }
                        } else {
                            logger.warn("Element tag <" + "READ_FILTERING" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        NodeList trimmerNodes = iProjectElement.getElementsByTagName("TRIMMER");
                        if (trimmerNodes.getLength() != 0) {
                            logger.info("found <TRIMMER> tag ");
                            for (int tempi = 0; tempi < trimmerNodes.getLength(); tempi++) {
                                Node iTrimmerNode = trimmerNodes.item(tempi);
                                if (iTrimmerNode.getNodeType() == Node.ELEMENT_NODE) {
                                    Element trimmerElement = (Element) iTrimmerNode;
                                    if (trimmerElement.getElementsByTagName("TRIMMER_NAME").getLength() != 0) {
                                        configHashMap.replace("TRIMMER_NAME", trimmerElement.getElementsByTagName("TRIMMER_NAME").item(0).getTextContent());
                                        logger.info("set <TRIMMER_NAME> to <" + iProjectElement.getElementsByTagName("TRIMMER_NAME").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "TRIMMER_NAME" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                    if (trimmerElement.getElementsByTagName("TRIMMER_PATH").getLength() != 0) {
                                        configHashMap.replace("TRIMMER_PATH", trimmerElement.getElementsByTagName("TRIMMER_PATH").item(0).getTextContent());
                                        logger.info("set <TRIMMER_PATH> to <" + iProjectElement.getElementsByTagName("TRIMMER_PATH").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "TRIMMER_PATH" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                    if (trimmerElement.getElementsByTagName("TRIMMER_CONF").getLength() != 0) {
                                        configHashMap.replace("TRIMMER_CONF", trimmerElement.getElementsByTagName("TRIMMER_CONF").item(0).getTextContent());
                                        logger.info("set <TRIMMER_CONF> to <" + iProjectElement.getElementsByTagName("TRIMMER_CONF").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "TRIMMER_CONF" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                }
                            }
                        } else {
                            logger.warn("Element tag <" + "TRIMMER" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        NodeList mapperNodes = iProjectElement.getElementsByTagName("MAPPER");
                        if (mapperNodes.getLength() != 0) {
                            logger.info("found <MAPPER> tag ");
//                            logger.info("set <HOST> to <" + iProjectElement.getElementsByTagName("HOST").item(0).getTextContent() + ">");
                            for (int tempi = 0; tempi < mapperNodes.getLength(); tempi++) {
                                Node iMapperNode = mapperNodes.item(tempi);
                                if (iMapperNode.getNodeType() == Node.ELEMENT_NODE) {
                                    Element mapperElement = (Element) iMapperNode;
                                    if (mapperElement.getElementsByTagName("MAPPER_NAME").getLength() != 0) {
                                        configHashMap.replace("MAPPER_NAME", mapperElement.getElementsByTagName("MAPPER_NAME").item(0).getTextContent());
                                        logger.info("set <MAPPER_NAME> to <" + iProjectElement.getElementsByTagName("MAPPER_NAME").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "MAPPER_NAME" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                    if (mapperElement.getElementsByTagName("MAPPER_PATH").getLength() != 0) {
                                        configHashMap.replace("MAPPER_PATH", mapperElement.getElementsByTagName("MAPPER_PATH").item(0).getTextContent());
                                        logger.info("set <MAPPER_PATH> to <" + iProjectElement.getElementsByTagName("MAPPER_PATH").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "MAPPER_PATH" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                    if (mapperElement.getElementsByTagName("MAPPER_CONF").getLength() != 0) {
                                        configHashMap.replace("MAPPER_CONF", mapperElement.getElementsByTagName("MAPPER_CONF").item(0).getTextContent());
                                        logger.info("set <HOST> to <" + iProjectElement.getElementsByTagName("HOST").item(0).getTextContent() + ">");
                                    } else {
                                        logger.warn("Element tag <" + "MAPPER_CONF" + "> was not found in the configuration file\n"
                                                + "this may cause problems in the subsequent analysis");
                                    }
                                }
                            }
                        } else {
                            logger.warn("Element tag <" + "MAPPER" + "> was not found in the configuration file\n"
                                    + "this may cause problems in the subsequent analysis");
                        }

                        ArrayList<String> steps2Run = new ArrayList<>();
                        if (iProjectElement.getElementsByTagName("PIPELINE_STEPS").getLength() != 0) {
                            logger.info("found <PIPELINE_STEPS> tag ");
                            Node childStep = (iProjectElement.getElementsByTagName("PIPELINE_STEPS").item(0)).getFirstChild();
                            while (childStep.getNextSibling() != null) {
                                childStep = childStep.getNextSibling();
                                if (childStep.getNodeType() == Node.ELEMENT_NODE) {
                                    Element childElement = (Element) childStep;
                                    String stepName = childElement.getAttribute("name");
                                    String stepExecute = childElement.getAttribute("execute");

                                    if (stepName.equals("SAMParser")) {
                                        logger.info("found <SAMParser> tag ");
                                        Node subStepChildNode = childStep.getFirstChild();
                                        while (subStepChildNode.getNextSibling() != null) {
                                            subStepChildNode = subStepChildNode.getNextSibling();
                                            if (subStepChildNode.getNodeType() == Node.ELEMENT_NODE) {
                                                Element subStepChildNodeElement = (Element) subStepChildNode;
                                                if (subStepChildNodeElement.getNodeName().equals("CUSTOMIZED_FASTA")) {
                                                    configHashMap.replace("CUSTOMIZED_FASTA", subStepChildNodeElement.getTextContent());
                                                    logger.info("set <HOCUSTOMIZED_FASTAST> to <" + iProjectElement.getElementsByTagName("CUSTOMIZED_FASTA").item(0).getTextContent() + ">");
                                                }
                                                if (subStepChildNodeElement.getNodeName().equals("NT_SHIFTING")) {
                                                    configHashMap.replace("NT_SHIFTING", subStepChildNodeElement.getTextContent());
                                                    logger.info("set <NT_SHIFTING> to <" + iProjectElement.getElementsByTagName("NT_SHIFTING").item(0).getTextContent() + ">");
                                                }
                                            }
                                        }
                                    }

                                    if (stepExecute.equals("YES")) {
                                        steps2Run.add(stepName);
                                    }
                                }
                            }
                            configHashMap.replace("PIPELINE_STEPS", StringUtils.join(steps2Run, ";"));
                            /*
                            Parsing complete. 
                            Now build a pipeline for this project to run
                             */
                            logger.info("Parsing complete, executing steps...");
                            JisomiRPipeline thisProjectPipeline = new JisomiRPipeline();
                            thisProjectPipeline.setConfigurations(configHashMap);
                            thisProjectPipeline.setSteps(steps2Run);
                            thisProjectPipeline.execute();

                            logger.info("Successfully executed Project <" + projectName + ">\n");
                        } else {
                            logger.warn("Element tag <" + "PIPELINE_STEPS" + "> was not found in the configuration file\n"
                                    + "Jasmine will not perform any analysis.");
                        }
                    } // end isExecuteStr.equals("YES")
                    else {
                        logger.info("execute set to FALSE for <" + projectName + ">, skipping... \n");
                    }
                } // end  if (iProject.getNodeType() == Node.ELEMENT_NODE)
            } // End project loop
        } catch (Exception exXML) {
            logger.error("Exception was thrown during execution <" + xmlFilePath + ">");
            logger.error("message is <" + exXML.toString() + ">");
        }
    }

    /**
     * looking for xml file to execute
     *
     * @param argsStr
     * @return
     */
    private static String findXMLfile(String confXML) {
//        String confXML = "";
        boolean notFind = true;
        int argsIndex = 0;
        while (notFind) {
            String iPotentialFile = confXML;
            String fileSuffix = iPotentialFile.substring(iPotentialFile.length() - 3);
            if (fileSuffix.toUpperCase().equals("XML")) {
                confXML = iPotentialFile;
                notFind = false;
            }
        }

        if (confXML == null) {
            String currentWorkDir = System.getProperty("user.dir");
            File currentfolder = new File(currentWorkDir);
            File[] listAllFiles = currentfolder.listFiles();
            for (File ifile : listAllFiles) {
                String ifileName = ifile.getName();
                String fileSuffix = ifileName.substring(ifileName.length() - 3);
                if (fileSuffix.toUpperCase().equals("XML")) {
                    confXML = ifileName;

                }
            }
        }

        return confXML;
    }

}
