/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
abstract public class JASMINEstep {
    Logger logger = LogManager.getRootLogger();
    
    private String TASK_FOLDER = "IsomiRsReports";
    private String  HOST;
    private String OUTPUTFOLDER;
    private String projectID;
    private String mirBasePath;
    private String mirBaseVersion;
    private String bowtiepath;
    private int mismatch;
    private String inputFolder ;
    private String datasetlstFile;
    private int threads ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    private ArrayList<String> datasetArray;
    private String projectFolder ;
    private String DOfilter ;
    private int thisDatasetThreshold;
    
    public void setConfigurations(HashMap<String, String> CONFIG_HASH) throws IOException{
        this.HOST = CONFIG_HASH.get("HOST");
        
        this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH");
        this.mirBaseVersion = CONFIG_HASH.get("MIRBASE_VERSION");
        this.bowtiepath = CONFIG_HASH.get("BOWTIE_PATH");
        this.mismatch = Integer.valueOf(CONFIG_HASH.get("MISMATCH"));
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.threads = Integer.valueOf(CONFIG_HASH.get("THREADS"));
        this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        this.inputFolder = CONFIG_HASH.get("PROJECT_FOLDER")+"MappedReads/";
        this.DOfilter = CONFIG_HASH.get("DOFILTER");
    }
    
}
