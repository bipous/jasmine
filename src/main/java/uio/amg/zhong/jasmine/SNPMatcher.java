/*
 * Copyright (C) 2019 xiangfuz
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class SNPMatcher {
    Logger logger = LogManager.getRootLogger();
    private HashMap<String, String> snpProfileHashMap = new HashMap<String, String>(){{
        /*
        temporary using http://bioinfo.life.hust.edu.cn/miRNASNP2/download/snp_in_human_premir.txt
        reordr column to format them
        */
            put("hsa", "hsa-SNP_in_pre-miRNAs.txt"); 
            put("bta", "bta-SNP_in_pre-miRNAs.txt");
            put("cfa", "cfa-SNP_in_pre-miRNAs.txt");
            put("mmu", "mmu-SNP_in_pre-miRNAs.txt");
            put("dre", "dre-SNP_in_pre-miRNAs.txt");
            put("ptr", "ptr-SNP_in_pre-miRNAs.txt");
            put("gga", "gga-SNP_in_pre-miRNAs.txt");
            put("rno", "rno-SNP_in_pre-miRNAs.txt");
        }};
    private  HashMap<String, String> pre2snpHashMap = new HashMap<String, String>();  
    private boolean available = false;

    SNPMatcher(String querySpecies) {
        snpMatcher(querySpecies);
    }
    
    SNPMatcher() {
        snpMatcher("hsa");
    }
    
    private void snpMatcher(String querySpecies){
        if(snpProfileHashMap.containsKey(querySpecies)){
            logger.info("SNPMatcher is active for: " + querySpecies);
////            String snpfilepath="NULLPATH for included SNP text";
            try {
//                snpfilepath = this.getClass().getResource("/resources/"+snpProfileHashMap.get(querySpecies)).getFile();
//                File snpfile = new File(snpfilepath);
                BufferedReader txtReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/resources/"+snpProfileHashMap.get(querySpecies))));

                buildHash4SNP(txtReader);
                available = true;
            } catch (Exception e) {
//                logger.error("error in read in snp profile from "+snpfilepath);
                logger.error("Due to "+e.toString());
            }
        }
        else{
            logger.error(String.format("The defined species is not supported for SNP matching yet! --- "+querySpecies));
        }
    }

    String matching(String queryMirStr) {
        String queryResultStr = "NA";
        if (pre2snpHashMap != null) {
            if(pre2snpHashMap.containsKey(queryMirStr)){
                queryResultStr = pre2snpHashMap.get(queryMirStr);
            }
        }
        return queryResultStr;
    }

    private void buildHash4SNP(File snpfile) throws FileNotFoundException, IOException {
        BufferedReader txtReader = new BufferedReader(new FileReader(snpfile));
        String txtline = null;
        while ((txtline = txtReader.readLine()) != null) {
            if(txtline.startsWith("id")){
                String headerInfo = txtline;
            }
            else{
                String[] txtlineSplitted = txtline.split("\t");
                /*
                txtlineSplitted[1] --- pre-miRNA name
                txtlineSplitted[2] --- the SNP position on pre-miRNA
                txtlineSplitted[0] --- snp ID
                txtlineSplitted[3] --- allele info 
                */
                pre2snpHashMap.put((txtlineSplitted[1]+"_"+txtlineSplitted[2]), (txtlineSplitted[0]+":"+txtlineSplitted[3]));
            }
        }
    }

    boolean isAvailable() {
        return available; 
    }

    private void buildHash4SNP(BufferedReader txtReader) throws IOException {
        for (String txtline; (txtline = txtReader.readLine()) != null;) {
            if (txtline.startsWith("id")) {
                String headerInfo = txtline;
            } else {
                String[] txtlineSplitted = txtline.split("\t");
                /*
                txtlineSplitted[1] --- pre-miRNA name
                txtlineSplitted[2] --- the SNP position on pre-miRNA
                txtlineSplitted[0] --- snp ID
                txtlineSplitted[3] --- allele info 
                 */
                pre2snpHashMap.put((txtlineSplitted[1] + "_" + txtlineSplitted[2]), (txtlineSplitted[0] + ":" + txtlineSplitted[3]));
            }
        }
    }

}
