/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uio.amg.zhong.jasmine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import uio.amg.zhong.utility.ReadStdIOStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author xiangfuz
 */
public class MiRBaseHairpinMapping {
    Logger logger = LogManager.getRootLogger();
    private String TASK_FOLDER = "MappedReads";
    private String  HOST;
    private String OUTPUTFOLDER;
    private String projectFolder;
    private String projectID;
    private String mirBasePath;
    private String mirBaseVersion;
    private String bowtiepath;
    private int mismatch;
    private String inputFolder ;
    private String datasetlstFile;
    private int threads ;
    private ArrayList<String> datasetArray;
    private ArrayList<String> mappingSummaryStrArray;
    
    private String bowtieIndex ;
    protected  final  String  FILESEPARATOR   = System.getProperty("file.separator");
    
    
    private   String INFILE_EXTENSION = ".trim.clp.fasta";
    private   String RAW_INPUT_EXTENSION = ".fastq.gz";
    
    private String fastqTrimmedInputFile = "";
    private String fastqAbundantAln = "";
    private String fastqAbundantUnAln = "";
    private String fastqGenomeAln = "";
    private String fastqGenomeUnAln = "";
    private String samGenomeAln = "";
    private String samAbundantAln = "";
    
//    private static final String FASTQ_ABUNALN_EXTENSION = ".trim.clp.abun.fasta";
//    private static final String FASTQ_ABUNUNALN_EXTENSION = ".trim.clp.notabun.fasta";
//    private static final String SAM_ABUNALN_EXTENSION = ".trim.clp.abun.sam";
    private static final String FASTQ_GENALN_EXTENSION = ".trim.clp.gen.fasta";
    private static final String FASTQ_UNALN_EXTENSION = ".trim.clp.unmap.fasta";
    private static final String SAM_GENALN_EXTENSION = ".trim.clp.gen.sam";
    private static final String MAPPING_SUMMARY_EXTENSION = ".trim.clp.gen.mapping.txt";
    
    

//    private static final String SAM_GENALN_MN2_EXTENSION = ".trim.clp.gen_MN2.sam";
//    private static final String MIRCOUNTS_MN2_EXTENSION  = ".trim.clp.gen.mircounts-MN2.tsv";
//    private static final String FASTQ_GENALN_MN2_EXTENSION = ".trim.clp.gen_MN2.fasta";
//    private static final String FASTQ_UNALN_MN2_EXTENSION = ".trim.clp.unmap_MN2.fasta";
    private ArrayList<String> mapGenStdErr;
    
    private String mapper_PATH;
    private String mapper_NAME;
    private String mapper_CONFIG;
    private HashMap<String, String> CONFIG_HASH;
    
    MiRBaseHairpinMapping(HashMap<String, String> CONFIGHASH) throws IOException {
        CONFIG_HASH = CONFIGHASH;
        setConfigurations(CONFIG_HASH);
        startMapping();
        logger.info(projectID);
    }

    private void setConfigurations(HashMap<String, String> CONFIG_HASH) throws IOException {
        this.HOST = CONFIG_HASH.get("HOST");
        this.mapper_NAME = CONFIG_HASH.get("MAPPER_NAME");
        this.mapper_PATH = CONFIG_HASH.get("MAPPER_PATH");
        this.mapper_CONFIG = CONFIG_HASH.get("MAPPER_CONF");
        
//        this.bowtiepath = CONFIG_HASH.get("BOWTIE_PATH");
        this.mismatch = Integer.valueOf(CONFIG_HASH.get("MISMATCH"));
        this.projectID = CONFIG_HASH.get("PROJECT_ID");
        this.threads = Integer.valueOf(CONFIG_HASH.get("THREADS"));
        
        if(CONFIG_HASH.get("PROJECT_FOLDER").endsWith(FILESEPARATOR)){
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER");
        }
        else{
            this.projectFolder = CONFIG_HASH.get("PROJECT_FOLDER") +FILESEPARATOR;
        }
        this.bowtieIndex = CONFIG_HASH.get("PROJECT_FOLDER")+"Reference"+FILESEPARATOR+"miRBase"+"."+HOST+".hairpin";
        this.OUTPUTFOLDER = CONFIG_HASH.get("PROJECT_FOLDER")+TASK_FOLDER+FILESEPARATOR;
        
        if(CONFIG_HASH.get("MIRBASE_PATH").endsWith(FILESEPARATOR)){
            this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH");
        }
        else{
            this.mirBasePath = CONFIG_HASH.get("MIRBASE_PATH")+FILESEPARATOR;
        }
                
        if(CONFIG_HASH.containsKey("DATASETLIST")){
            this.datasetlstFile = CONFIG_HASH.get("DATASETLIST");
        }
        else{
            this.datasetlstFile = projectFolder +"configs"+FILESEPARATOR+"miRNASeqData.tsv";
        }
        if (new File(CONFIG_HASH.get("INPUTFOLDER")).isDirectory()) {
            this.inputFolder = CONFIG_HASH.get("INPUTFOLDER");
        }
        else{
            this.inputFolder = projectFolder + CONFIG_HASH.get("INPUTFOLDER") +FILESEPARATOR;
        }
        this.OUTPUTFOLDER = projectFolder +TASK_FOLDER +FILESEPARATOR;

        JASMINEutility misoUtility = new JASMINEutility();
        this.datasetArray = misoUtility.getDatasetList(CONFIG_HASH.get("DATASETLIST"));

        String[] namesplitted = datasetArray.get(0).split("\\.");
        if(namesplitted[namesplitted.length -1].equals("gz")){
            this.RAW_INPUT_EXTENSION =  "."+namesplitted[namesplitted.length -2]+"."+namesplitted[namesplitted.length -1];
        }
        else{
            this.RAW_INPUT_EXTENSION = "."+namesplitted[namesplitted.length -1];
        }
    }

    private void startMapping() throws IOException {
        logger.info("start mapping ");
        /*
        verify the dataset list file
        */
//        if(new File(datasetlstFile).exists() == false){
//            logger.error("abundant sequence bowtie index < " + datasetlstFile +"> not found");
//            throw new IOException("abundant sequence bowtie index  < " + datasetlstFile +"> not found");
//        }
//
//        this.datasetArray = getDatasetList(datasetlstFile);
        
        Boolean fA = new File(OUTPUTFOLDER).mkdir();
        if (fA) {
            logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
        }
        
        String mappingCmd = bowtiepath;
        logger.info("-->Mapping software is " + mappingCmd);
        
        mappingSummaryStrArray = new ArrayList<>();
                
        for (String idatasetFastaGz : datasetArray) {
            String idataset = idatasetFastaGz;
            idataset = idatasetFastaGz.replace(RAW_INPUT_EXTENSION, "");
            idataset = idatasetFastaGz.replace(".gz", "");
            idataset = idatasetFastaGz.replace(".fastq", "");
            idataset = idatasetFastaGz.replace(".fasta", "");
                        
            ArrayList<String> cmd = new ArrayList<>();
            try {
                mapRead2Hairpin(idatasetFastaGz);
                /*
                    write out mapping summary
                 */
                fastqTrimmedInputFile = inputFolder+idatasetFastaGz.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION);
                int inputCount = countReadsInFasta(fastqTrimmedInputFile);
                int mappedCount = countReadsInFasta(fastqGenomeAln);
                int unmappedCount = countReadsInFasta(fastqGenomeUnAln);
                mappingSummaryStrArray.add(idatasetFastaGz.replace(RAW_INPUT_EXTENSION, "") +"\t"+ String.valueOf(inputCount) +"\t"+ String.valueOf(mappedCount)+"\t"+ String.valueOf(unmappedCount));

            } catch (IOException ex) {
                logger.error("error executing Bowtie Mapping command\n");
                logger.error(cmd);
                logger.error(ex.toString());
            }
        }
        
        String mappingSummaryFile  = OUTPUTFOLDER +projectID+ ".mappingSummary.tsv";
        BufferedWriter mappingSummaryBW  = new BufferedWriter(new FileWriter(new File(mappingSummaryFile)));
        for(String mappingSummaryLine: mappingSummaryStrArray){
            mappingSummaryBW.write(mappingSummaryLine +"\n");   
        }
        mappingSummaryBW.close();
        
    }
    
    private  ArrayList<String> getDatasetList(String dataLayoutFilename) throws IOException {
        ArrayList datasetArrayList = new ArrayList();
        try{
            logger.info("reading data layout file <" +  dataLayoutFilename + ">");
            BufferedReader brFA = new BufferedReader(new FileReader(new File(dataLayoutFilename)));
            String lineFA = null;
            while ((lineFA = brFA.readLine())!=null){
                if(lineFA.startsWith("File")) continue;
                String datasetString = lineFA.trim().split("\t")[0];
                datasetArrayList.add(datasetString);
            }
            brFA.close();
            logger.info("read " + datasetArrayList.size() + " entries");
            logger.info("done");
        }
        catch(IOException ex){
            logger.error("error reading data layout file <" +  dataLayoutFilename + ">\n" + ex.toString());
            throw new IOException("error reading data layout file <" +  dataLayoutFilename + ">");
        }
        return datasetArrayList;
    }

    private void mapRead2Hairpin(String idatasetFasta) throws IOException {
        logger.info(mapper_NAME+" is chosed for Mapping");
        String cmdBowtieMapmiRBaseReads = "";
        try {
            ArrayList cmd = new ArrayList<>();
            if (mapper_NAME.toLowerCase().equals("bowtie")) {
                cmd.add(mapper_PATH);
                cmd.add(bowtieIndex);

                cmd.add("-f");
                fastqTrimmedInputFile = inputFolder + idatasetFasta.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION);
                cmd.add(fastqTrimmedInputFile);
                /*
                    -n max mismatches in seed (can be 0-3, default: -n 2)
                    -v report end-to-end hits w/ <=v mismatches; ignore qualities
                 */
                cmd.add("-v --best  --norc"); // set to use 2 mismatch and not allign to reverse-complement reference strand
                cmd.add("-n "); 
                cmd.add(mismatch); 
                fastqGenomeAln = this.cleanPath(OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, FASTQ_GENALN_EXTENSION));
                fastqGenomeUnAln = this.cleanPath(OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, FASTQ_UNALN_EXTENSION));
                samGenomeAln = this.cleanPath(OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, SAM_GENALN_EXTENSION));
                cmd.add("--al " + fastqGenomeAln);
                cmd.add("--un " + fastqGenomeUnAln);
                cmd.add("-p " + threads);
                cmd.add("--sam " + samGenomeAln);

                cmdBowtieMapmiRBaseReads = this.cleanPath(StringUtils.join(cmd, " "));
                logger.info("Bowtie Map Genome Reads command:\t" + cmdBowtieMapmiRBaseReads);

                Runtime rtGenMap = Runtime.getRuntime();
                Process procGenMap = rtGenMap.exec(cmdBowtieMapmiRBaseReads);
                ReadStdIOStream s1 = new ReadStdIOStream("Stdin", procGenMap.getInputStream(), "test");
                ReadStdIOStream s2 = new ReadStdIOStream("StdErr", procGenMap.getErrorStream(), "test");
                s1.start();
                s2.start();
                procGenMap.waitFor();

                mapGenStdErr = s2.getStreamData();

                int gExitVal = procGenMap.waitFor();
                logger.info("Process exitValue: " + gExitVal);
            }
            else if (mapper_NAME.toLowerCase().equals("soap2") | mapper_NAME.toLowerCase().equals("soap")){
                cmd.add(mapper_PATH);
                fastqTrimmedInputFile = inputFolder + idatasetFasta.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION);
                cmd.add(fastqTrimmedInputFile);
                cmd.add(" -a " + fastqTrimmedInputFile);
                cmd.add(" -M " + mismatch);
                cmd.add(" -p " + threads);
                fastqGenomeAln = this.cleanPath(OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, FASTQ_GENALN_EXTENSION));
                fastqGenomeUnAln = this.cleanPath(OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, FASTQ_UNALN_EXTENSION));
                samGenomeAln = this.cleanPath(OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, SAM_GENALN_EXTENSION));
                cmd.add(" -o " + samGenomeAln);
            }
            else{
                String customizedMapperCmd = mapper_CONFIG;
                customizedMapperCmd = customizedMapperCmd.replace("INPUT", inputFolder + idatasetFasta.replace(RAW_INPUT_EXTENSION, INFILE_EXTENSION));
                Boolean f = new File(OUTPUTFOLDER).mkdir();
                if (f) {
                    logger.info("created output folder <" + OUTPUTFOLDER + "> for results");
                }
                customizedMapperCmd = customizedMapperCmd.replace("OUTPUT", OUTPUTFOLDER + idatasetFasta.replace(RAW_INPUT_EXTENSION, SAM_GENALN_EXTENSION));
                Runtime rt = Runtime.getRuntime();
                Process proc = rt.exec(customizedMapperCmd);
            }

        } catch (Exception ex) {
            logger.error("error Bowtie Mapping genome reads\n");
            logger.error(cmdBowtieMapmiRBaseReads);
            logger.error(ex.toString());
            throw new IOException(": \"error Bowtie Mapping genome reads " + cmdBowtieMapmiRBaseReads);
        }
    }

    private int countReadsInFasta(String fastqTrimmedInputFile) throws FileNotFoundException, IOException {
        int readsCount = 0;
        String faLine = "";
        BufferedReader brIR = new BufferedReader(new FileReader(new File(fastqTrimmedInputFile)));
        while ((faLine = brIR.readLine()) != null) {
            if(faLine.contains("-")){
                readsCount = readsCount + Integer.parseInt(faLine.substring(1).split("-")[1]);
                brIR.readLine();
            }
        }
        brIR.close(); 
        return readsCount;
    }

    private String cleanPath(String pathstring) {
        return pathstring.replace(FILESEPARATOR + FILESEPARATOR, FILESEPARATOR);
    }

    public HashMap<String, String> updateConfig() {
        /*
        updating configuration hash, passing it to next step
        */
        CONFIG_HASH.replace("INPUTFOLDER", TASK_FOLDER);
        CONFIG_HASH.replace("DATASET_ARRAY_STR", StringUtils.join(datasetArray, "#"));
        return CONFIG_HASH;
    }
}
