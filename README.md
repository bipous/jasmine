# Jasmine

Jasmine: a Java pipeline for isomiR characterization in miRNA-Seq Data

## Motivation 
The existence of complex subpopulations of miRNA isoforms, or isomiRs, is well established. While many tools exist for investigating isomiR populations, they differ in how they characterize an isomiR, making it difficult to compare results across different tools. Thus, there is a need for a more comprehensive and systematic standard for defining isomiRs. Such a standard would allow investigation of isomiR population structure in progressively more refined sub-populations, permitting the identification of more subtle changes between conditions and leading to an improved understanding of the processes that generate these differences.

## Results 
We developed Jasmine, a software tool that incorporates a hierarchal framework for characterizing isomiR populations. Jasmine is a Java application that can process raw read data in fastq/fasta format, or mapped reads in SAM format to produce a detailed characterization of isomiR populations. Thus, Jasmine can reveal structure not apparent in a standard miRNA-Seq analysis pipeline.

## Usage of Jasmine 
Run in command line: `java -jar Jasmine.jar jasmine_config.xml`. Currently, Jasmine released 1.1 version, so the command will be specifically `java -jar Jasmine-1.1.jar jasmine_config.xml`.

## Installation suggestion for dependencies
For the tools used in Jasmine pipeline, users can install them via `conda install -c bioconda fastqc fastx_toolkit multiqc bowtie trimmomatic`.

For the R package dependencies we deliberately haven’t included these dependencies as this can be cause problems with different versions of R if packages are automatically updated, for example due to functions being removed from an updated version of a package, breaking a user’s local R configuration. This was also why the Plotting step outputs R scripts rather than executing them within Jasmine. 

### Source Code ###
The source code of Jasmine is deposited at [bitbucket](https://bitbucket.org/bipous/jasmine/src/master/).
User can compile from source or use the [compiled jar file](https://bitbucket.org/bipous/jasmine/src/master/compiled_jar/)

### Case Study ###
[Case Study](https://bitbucket.org/bipous/jasmine_casestudy/src/master/) repository for Jasmine.
